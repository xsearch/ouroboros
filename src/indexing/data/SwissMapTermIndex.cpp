#include "indexing/data/SwissMapTermIndex.hpp"

#ifdef GOOGLE_SWISS_LIB

namespace ouroboros
{
    long SwissMapTermIndex::insert(const char *term)
    {
        char* storeTerm;
        long idx = -1;

        auto search = map.find(term);
        if (search == map.end()) {
            storeTerm = store->store(term);
            terms.push_back(storeTerm);
            idx = numTerms++;
            map.insert({storeTerm, idx});
        } else {
            idx = search->second;
        }

        return idx;
    }

    void SwissMapTermIndex::remove(const char *term)
    {

    }

    long SwissMapTermIndex::lookup(const char *term)
    {
        long idx = -1;

        auto search = map.find(term);
        if (search != map.end()) {
            idx = search->second;
        }

        return idx;
    }

    const char* SwissMapTermIndex::reverseLookup(long idx)
    {
        if (idx >= numTerms) {
            return NULL;
        }

        return terms.at(idx);
    }

    long SwissMapTermIndex::getNumTerms()
    {
        return numTerms;
    }

}

#endif