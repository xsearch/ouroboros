#include "indexing/data/StdMapTermIndex.hpp"

namespace ouroboros
{
    long StdMapTermIndex::insert(const char *term)
    {
        char* storeTerm;
        long idx = -1;

        auto search = map.find(term);
        if (search == map.end()) {
            storeTerm = store->store(term);
            terms.push_back(storeTerm);
            idx = numTerms++;
            map.insert({storeTerm, idx});
        } else {
            idx = search->second;
        }

        return idx;
    }

    void StdMapTermIndex::remove(const char *term)
    {

    }

    long StdMapTermIndex::lookup(const char *term)
    {
        long idx = -1;

        auto search = map.find(term);
        if (search != map.end()) {
            idx = search->second;
        }

        return idx;
    }

    const char* StdMapTermIndex::reverseLookup(long idx)
    {
        if (idx >= numTerms) {
            return NULL;
        }

        return terms.at(idx);
    }

    long StdMapTermIndex::getNumTerms()
    {
        return numTerms;
    }

}