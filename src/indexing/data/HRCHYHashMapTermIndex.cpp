#include "indexing/data/HRCHYHashMapTermIndex.hpp"
#include "indexing/indexing.hpp"

namespace ouroboros
{
    HRCHYVector::HRCHYVector(int vectorWidth)
    {
        this->vectorWidth = vectorWidth;
        index = new std::vector<std::vector<std::string*>*>();
        index->push_back(new std::vector<std::string*>(vectorWidth, nullptr));
        capacity = vectorWidth;
        pos.store(0, std::memory_order_seq_cst);
    }

    HRCHYVector::~HRCHYVector()
    {
        for (auto i = 0; i < (capacity / vectorWidth); ++i) {
            for  (auto term : *(index->at(i))) {
                delete term;
            }
            delete index->at(i);
        }
        delete index;
    }

    long HRCHYVector::push_back(std::string *term)
    {
        long idx = pos.fetch_add(1, std::memory_order_seq_cst);
        long i = idx / vectorWidth;
        long j = idx % vectorWidth;

        if (idx >= capacity) {
            std::lock_guard<std::mutex> lock(mtx);
            if (idx >= capacity) {
                index->push_back(new std::vector<std::string*>(vectorWidth, nullptr));
                capacity += vectorWidth;
            }
        }

        index->at(i)->at(j) = term;

        return idx;
    }

    std::string* HRCHYVector::at(long idx)
    {
        long i = idx / vectorWidth;
        long j = idx % vectorWidth;

        if (idx < capacity) {
            return index->at(i)->at(j);
        } else {
            return nullptr;
        }
    }

    void HRCHYVector::toombstone(long idx)
    {
        long i = idx / vectorWidth;
        long j = idx % vectorWidth;

        if (idx < capacity) {
            index->at(i)->at(j) = nullptr;
        }
    }
    
    HRCHYHashMapTermIndex::HRCHYHashMapTermIndex(int mapWidth, int vectorWidth)
    {
        this->mapWidth = mapWidth;
        maps = new std::vector<std::unordered_map<std::string*, long, strptr_hash, strptr_equal>*>();
        for (auto i = 0; i < mapWidth; ++i) {
            maps->push_back(new std::unordered_map<std::string*, long, strptr_hash, strptr_equal>());
        }
        mtxs = new std::vector<std::mutex*>();
        for (auto i = 0; i < mapWidth; ++i) {
            mtxs->push_back(new std::mutex());
        }
        index = new HRCHYVector(vectorWidth);
        numTerms.store(0, std::memory_order_seq_cst);
    }

    HRCHYHashMapTermIndex::~HRCHYHashMapTermIndex()
    {
        delete index;
        for (auto map : *maps) {
            delete map;
        }
        delete maps;
        for (auto mtx : *mtxs) {
            delete mtx;
        }
        delete mtxs;
    }

    inline unsigned long HRCHYHashMapTermIndex::djb2_hash(const unsigned char *str)
    {
        long hash = 5381;
        int c;

        while ((c = *str++)) {
            hash = ((hash << 5) + hash) + c;
        }

        return hash;
    }

    inline int HRCHYHashMapTermIndex::hash(const char *str)
    {
        unsigned long hash_value = djb2_hash((const unsigned char*) str);

        return (int) (hash_value % (unsigned long) mapWidth);
    }

    long HRCHYHashMapTermIndex::insert(const char *term)
    {
        int hash_value = hash(term);
        std::lock_guard<std::mutex> lock(*(mtxs->at(hash_value)));
        std::string *token = new std::string(term);
        long idx;

        auto search = maps->at(hash_value)->find(token);
        if (search == maps->at(hash_value)->end()) {
            idx = index->push_back(token);
            maps->at(hash_value)->insert(std::make_pair(token, idx));
            numTerms++;
        } else {
            idx = search->second;
            delete token;
        }

        return idx;
    }

    void HRCHYHashMapTermIndex::remove(const char *term)
    {
        int hash_value = hash(term);
        std::lock_guard<std::mutex> lock(*(*mtxs)[hash_value]);
        std::string *token;
        long idx = lookup(term);

        if (idx != INDEX_NEXISTS) {
            token = index->at(idx);
            index->toombstone(idx);
            (*maps)[hash_value]->at(token) = TOMBSTONE;
            delete token;
            numTerms--;
        }
    }

    long HRCHYHashMapTermIndex::lookup(const char *term)
    {
        int hash_value = hash(term);
        std::string *token = new std::string(term);
        long idx;

        auto search = (*maps)[hash_value]->find(token);
        if (search != (*maps)[hash_value]->end()) {
            idx = search->second;
        } else {
            idx = INDEX_NEXISTS;
        }

        delete token;

        return idx;
    }

    const char* HRCHYHashMapTermIndex::reverseLookup(long idx)
    {
        std::string *token;

        token = index->at(idx);
        if (token != nullptr) {
            return token->c_str();
        }

        return NULL;
    }

    long HRCHYHashMapTermIndex::getNumTerms()
    {
        return numTerms.load();
    }
}
