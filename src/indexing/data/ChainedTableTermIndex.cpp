#include "indexing/data/ChainedTableTermIndex.hpp"

namespace ouroboros
{
    long ChainedTableTermIndex::insert(const char *term)
    {
        char* storeTerm;
        long idx = -1;

        if (!map.lookup(term, idx)) {
            storeTerm = store->store(term);
            terms.push_back(storeTerm);
            idx = numTerms++;
            map.insert(storeTerm, idx);
        }

        return idx;
    }

    void ChainedTableTermIndex::remove(const char *term)
    {

    }

    long ChainedTableTermIndex::lookup(const char *term)
    {
        long idx = -1;

        map.lookup(term, idx);

        return idx;
    }

    const char* ChainedTableTermIndex::reverseLookup(long idx)
    {
        if (idx >= numTerms) {
            return NULL;
        }

        return terms.at(idx);
    }

    long ChainedTableTermIndex::getNumTerms()
    {
        return numTerms;
    }

}