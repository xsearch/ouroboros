#include "indexing/data/StdHashMapTermIndex.hpp"
#include "indexing/indexing.hpp"

namespace ouroboros
{
    StdHashMapTermIndex::StdHashMapTermIndex()
    {
        map = new std::unordered_map<std::string*, long, strptr_hash, strptr_equal>();
        index = new std::vector<std::string*>();
        numTerms = 0;
    }
    
    StdHashMapTermIndex::~StdHashMapTermIndex()
    {
        for (auto term : *index) {
            if (term != nullptr) {
                delete term;
            }
        }
        delete index;
        delete map;
        numTerms = 0;
    }

    long StdHashMapTermIndex::insert(const char *term)
    {
        std::lock_guard<std::mutex> lock(mtx);
        std::string *token = new std::string(term);
        long idx = lookup(term);

        if (idx == INDEX_NEXISTS) {
            index->push_back(token);
            idx = index->size() - 1;
            map->insert(std::make_pair(token, idx));
            numTerms++;
        } else {
            delete token;
        }

        return idx;
    }

    void StdHashMapTermIndex::remove(const char *term)
    {
        std::lock_guard<std::mutex> lock(mtx);
        std::string *token;
        long idx = lookup(term);

        if (idx != INDEX_NEXISTS) {
            token = index->at(idx);
            index->at(idx) = nullptr;
            map->at(token) = TOMBSTONE;
            delete token;
            numTerms--;
        }
    }
    
    long StdHashMapTermIndex::lookup(const char *term)
    {
        std::string* token = new std::string(term);
        long idx;

        auto search = map->find(token);
        if (search != map->end()) {
            idx = search->second; 
        } else {
            idx = INDEX_NEXISTS;
        }

        delete token;

        return idx;
    }

    const char* StdHashMapTermIndex::reverseLookup(long idx)
    {
        std::string* token;

        if (idx < (long) index->size()) {
            token = index->at(idx);
            if (token != nullptr) {
                return token->c_str();
            }
        }

        return NULL;
    }
    
    long StdHashMapTermIndex::getNumTerms()
    {
        return numTerms;
    }
}