#include "indexing/sources/StdHashMapFileIndex.hpp"
#include "indexing/indexing.hpp"

namespace ouroboros
{
    StdHashMapFileIndex::StdHashMapFileIndex()
    {
        map = new std::unordered_map<std::string*, long, strptr_hash, strptr_equal>();
        index = new std::vector<std::string*>();
        size = 0;
    }
    
    StdHashMapFileIndex::~StdHashMapFileIndex()
    {
        std::string *file;
        for (auto i = 0; i < (long) index->size(); i++) {
            if (index->at(i) != nullptr) {
                file = index->at(i);
                delete file;
            }
        }

        delete index;
        delete map;
        size = 0;
    }

    long StdHashMapFileIndex::insert(const char *path)
    {
        const std::lock_guard<std::mutex> lock(mtx);
        std::string *file = new std::string(path);
        long idx = lookup(path);

        if (idx == INDEX_NEXISTS) {
            index->push_back(file);
            idx = index->size() - 1;
            map->insert(std::make_pair(file, idx));
            size++;
        } else {
            delete file;
        }

        return idx;
    }

    void StdHashMapFileIndex::remove(const char *path)
    {
        const std::lock_guard<std::mutex> lock(mtx);
        std::string *file;
        long idx = lookup(path);

        if (idx != INDEX_NEXISTS) {
            file = index->at(idx);
            index->at(idx) = nullptr;
            map->at(file) = TOMBSTONE;
            delete file;
            size--;
        }
    }

    long StdHashMapFileIndex::lookup(const char *path)
    {
        std::string *file = new std::string(path);
        long idx;

        auto search = map->find(file);
        if (search != map->end()) {
            idx = search->second;
        } else {
            idx = INDEX_NEXISTS;
        }

        return idx;
    }
    
    const char* StdHashMapFileIndex::reverseLookup(long idx)
    {
        std::string *file;

        if (idx < (long) index->size()) {
            file = index->at(idx);
            if (file != nullptr) {
                return file->c_str();
            }
        }
        
        return NULL;
    }
    
    long StdHashMapFileIndex::getSize()
    {
        return size;
    }
}