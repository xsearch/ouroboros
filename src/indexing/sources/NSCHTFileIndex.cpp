#include "indexing/sources/NSCHTFileIndex.hpp"

namespace ouroboros
{
    NSCHTFileIndex::NSCHTFileIndex(long pageSize, long numBuckets) : BaseFileIndex()
    {
        store = new NSMemBytesStore(pageSize);
        map = new ChainedHashTable<char*, long>(numBuckets);
        files = new std::vector<char*>();
    }
    
    NSCHTFileIndex::~NSCHTFileIndex()
    {
        delete map;
        delete files;
        delete store;
    }

    long NSCHTFileIndex::insert(const char *path)
    {
        char* pathCopy;
        long idx = -1;

        if (map->lookup((char*) path, idx) == false) {
            pathCopy = store->copy(path);
            files->push_back(pathCopy);
            idx = files->size() - 1;
            map->insert(pathCopy, idx);
            store->next();
        }

        return idx;
    }
    
    long NSCHTFileIndex::lookup(const char *path)
    {
        long idx = -1;

        map->lookup((char*) path, idx);

        return idx;
    }
    
    const char* NSCHTFileIndex::reverseLookup(long idx)
    {
        return files->at(idx);
    }
}