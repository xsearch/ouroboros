#include "indexing/knowledge/SwissMapTermFileIndex.hpp"

#ifdef GOOGLE_SWISS_LIB

namespace ouroboros
{
    long SwissMapTermFileIndex::insert(const char *term, long fileIdx)
    {
        std::shared_ptr<TermFileIndexEntry> entry;
        long idx = -1;

        auto search = map.find(term);
        if (search == map.end()) {
            entry = std::shared_ptr<TermFileIndexEntry>(new TermFileIndexEntry(store->store(term)));
            index.push_back(entry);
            idx = numTerms++;
            map.insert({entry->term, idx});
            entry->files.push_back(fileIdx);
        } else {
            idx = search->second;
            entry = reverseLookup(idx);
            if (entry->files.back() != fileIdx) {
                entry->files.push_back(fileIdx);
            }
        }

        return idx;
    }

    void SwissMapTermFileIndex::remove(const char *term, long fileIdx)
    {

    }

    long SwissMapTermFileIndex::lookup(const char *term)
    {
        long idx = -1;

        auto search = map.find(term);
        if (search != map.end()) {
            idx = search->second;
        }

        return idx;
    }

    std::shared_ptr<TermFileIndexEntry> SwissMapTermFileIndex::reverseLookup(long idx)
    {
        std::shared_ptr<TermFileIndexEntry> entry;

        if (idx >= numTerms) {
            entry = std::shared_ptr<TermFileIndexEntry>(new TermFileIndexEntry(NULL));
        } else {
            entry = index.at(idx);
        }
        
        return entry;
    }

    long SwissMapTermFileIndex::getNumTerms()
    {
        return numTerms;
    }

}

#endif