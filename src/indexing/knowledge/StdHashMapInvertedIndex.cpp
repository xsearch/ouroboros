#include "indexing/knowledge/StdHashMapInvertedIndex.hpp"
#include "indexing/indexing.hpp"

namespace ouroboros
{
    StdHashMapInvertedIndex::StdHashMapInvertedIndex()
    {
        map = new std::unordered_map<long, std::unordered_map<long, long>*>();
        index = new std::vector<std::tuple<long, long>>();
        size = 0;
    }

    StdHashMapInvertedIndex::~StdHashMapInvertedIndex()
    {
        for (auto map_it = map->begin(); map_it != map->end(); ++map_it) {
            auto files_map = map_it->second;
            if (files_map != nullptr) {
                delete files_map;
            }
        }
        delete map;
        delete index;
        size = 0;
    }

    long StdHashMapInvertedIndex::insert(long termIdx, long fileIdx)
    {
        std::lock_guard<std::mutex> lock(mtx);
        long idx;

        auto search_term = map->find(termIdx);
        if (search_term != map->end()) {
            auto search_file = search_term->second->find(fileIdx);
            if (search_file != search_term->second->end()) {
                idx = map->at(termIdx)->at(fileIdx);
                if (idx == INDEX_NEXISTS) {
                    index->push_back(std::make_tuple(termIdx, fileIdx));
                    idx = index->size() - 1;
                    map->at(termIdx)->at(fileIdx) = idx;
                    size++;
                }
            } else {
                index->push_back(std::make_tuple(termIdx, fileIdx));
                idx = index->size() - 1;
                map->at(termIdx)->insert(std::make_pair(fileIdx, idx));
                size++;
            }
        } else {
            index->push_back(std::tuple<long, long>(termIdx, fileIdx));
            idx = index->size() - 1;
            map->insert(std::make_pair(termIdx, new std::unordered_map<long, long>()));
            map->at(termIdx)->insert(std::make_pair(fileIdx, idx));
            size++;
        }

        return idx;
    }
    
    void StdHashMapInvertedIndex::remove(long termIdx, long fileIdx)
    {
        std::lock_guard<std::mutex> lock(mtx);
        long idx;

        auto search_term = map->find(termIdx);
        if (search_term != map->end()) {
            auto search_file = search_term->second->find(fileIdx);
            if (search_file != search_term->second->end()) {
                idx = map->at(termIdx)->at(fileIdx);
                map->at(termIdx)->at(fileIdx) = TOMBSTONE;
                index->at(idx) = std::make_tuple(TOMBSTONE, TOMBSTONE);
                size--;
            }
        }
    }
    
    std::vector<std::tuple<long, long>>* StdHashMapInvertedIndex::lookup(long termIdx)
    {
        std::vector<std::tuple<long, long>> *files = new std::vector<std::tuple<long, long>>();

        auto search_term = map->find(termIdx);
        if (search_term != map->end()) {
            for (auto file_map = search_term->second->begin(); file_map != search_term->second->end(); ++file_map) {
                files->push_back(*file_map);
            }
        }

        return files;
    }
    
    void StdHashMapInvertedIndex::reverseRemove(long idx)
    {
        std::lock_guard<std::mutex> lock(mtx);
        long termIdx, fileIdx;

        if (idx < (long) index->size()) {
            if (index->at(idx) != std::make_tuple(TOMBSTONE, TOMBSTONE)) {
                termIdx = std::get<0>(index->at(idx));
                fileIdx = std::get<1>(index->at(idx));
                index->at(idx) = std::make_tuple(TOMBSTONE, TOMBSTONE);
                map->at(termIdx)->at(fileIdx) = TOMBSTONE;
                size--;
            }
        }
    }
    
    std::tuple<long, long> StdHashMapInvertedIndex::reverseLookup(long idx)
    {
        if (idx < (long) index->size()) {
            if (index->at(idx) != std::make_tuple(TOMBSTONE, TOMBSTONE)) {
                return index->at(idx);
            }
        }

        return std::tuple<long, long>(TOMBSTONE, TOMBSTONE);
    }
    
    long StdHashMapInvertedIndex::getSize()
    {
        return size;
    }
}