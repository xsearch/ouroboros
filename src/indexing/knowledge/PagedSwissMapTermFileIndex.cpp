#include "indexing/knowledge/PagedSwissMapTermFileIndex.hpp"

#ifdef GOOGLE_SWISS_LIB

namespace ouroboros
{
    long PagedSwissMapTermFileIndex::insert(const char *term, long fileIdx)
    {
        PagedVersatileIndex<char*, long>* entry;
        long idx = -1;

        auto search = map.find(term);
        if (search == map.end()) {
            entry = indexStore->getNew();
            entry->element = termStore->store(term);
            entry->tail = listStore->getNew();
            entry->tail->element = fileIdx;
            entry->tail->next = NULL;
            entry->head = entry->head;
            index.push_back(entry);
            idx = numTerms++;
            map.insert({entry->element, idx});
        } else {
            idx = search->second;
            entry = index.at(idx);
            if (entry->tail->element != fileIdx) {
                entry->tail->next = listStore->getNew();
                entry->tail->next->element = fileIdx;
                entry->tail->next->next = NULL;
                entry->tail = entry->tail->next;
            }
        }

        return idx;
    }

    void PagedSwissMapTermFileIndex::remove(const char *term, long fileIdx)
    {

    }

    long PagedSwissMapTermFileIndex::lookup(const char *term)
    {
        long idx = -1;

        auto search = map.find(term);
        if (search != map.end()) {
            idx = search->second;
        }

        return idx;
    }

    std::shared_ptr<TermFileIndexEntry> PagedSwissMapTermFileIndex::reverseLookup(long idx)
    {
        std::shared_ptr<TermFileIndexEntry> rcEntry;
        PagedVersatileIndex<char*, long>* entry;
        PagedLinkedElement<long>* p;

        if (idx >= numTerms) {
            rcEntry = std::shared_ptr<TermFileIndexEntry>(new TermFileIndexEntry(NULL));
        } else {
            entry = index.at(idx);
            rcEntry = std::shared_ptr<TermFileIndexEntry>(new TermFileIndexEntry(entry->element));
            p = entry->head;
            while (p != NULL) {
                rcEntry->files.push_back(p->element);
                p = p->next;
            }
        }
        
        return rcEntry;
    }

    long PagedSwissMapTermFileIndex::getNumTerms()
    {
        return numTerms;
    }

}

#endif