#include "indexing/information/StdHashMapFrequencyIndex.hpp"
#include "indexing/indexing.hpp"

namespace ouroboros
{
    StdHashMapFrequencyIndex::StdHashMapFrequencyIndex()
    {
        map = new std::unordered_map<long, long>();
        size = 0;
    }

    StdHashMapFrequencyIndex::~StdHashMapFrequencyIndex()
    {
        delete map;
    }

    void StdHashMapFrequencyIndex::insert(long idx)
    {
        std::lock_guard<std::mutex> lock(mtx);
        long freq = lookup(idx);

        if (freq == INDEX_NEXISTS) {
            map->insert(std::make_pair(idx, 0));
        }
    }

    void StdHashMapFrequencyIndex::remove(long idx)
    {
        std::lock_guard<std::mutex> lock(mtx);
        long freq = lookup(idx);

        if (freq != INDEX_NEXISTS) {
            map->at(idx) = TOMBSTONE;
        }
    }

    long StdHashMapFrequencyIndex::lookup(long idx)
    {
        long freq = INDEX_NEXISTS;

        auto search = map->find(idx);
        if (search != map->end()) {
            freq = search->second;
        }

        return freq;
    }

    inline void StdHashMapFrequencyIndex::initialize(long idx, long value)
    {
        std::lock_guard<std::mutex> lock(mtx);
        long freq = lookup(idx);

        if (freq == INDEX_NEXISTS) {
            map->insert(std::make_pair(idx, value));
        } else {
            map->at(idx) = value;
        }
    }

    inline void StdHashMapFrequencyIndex::increment(long idx, long value)
    {
        std::lock_guard<std::mutex> lock(mtx);
        long freq = lookup(idx);

        if (freq != INDEX_NEXISTS) {
            map->at(idx) += value;
        }
    }
    
    inline void StdHashMapFrequencyIndex::decrement(long idx, long value)
    {
        std::lock_guard<std::mutex> lock(mtx);
        long freq = lookup(idx);

        if (freq != INDEX_NEXISTS) {
            map->at(idx) -= value;
        }
    }
    
    long StdHashMapFrequencyIndex::getSize()
    {
        return size;
    }
}