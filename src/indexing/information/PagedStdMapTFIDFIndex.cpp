#include "indexing/information/PagedStdMapTFIDFIndex.hpp"

#include <iostream>

namespace ouroboros
{
    void PagedStdMapTFIDFIndex::insert(const char *term, long fileIdx)
    {
        PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>* entry;
        PagedLinkedElement<IDFIndexEntry>* node;
        char* storedBytes;
        long termLength;
        long entryLength;
        long nodeLength;

        auto search = map.find(term);
        if (search == map.end()) {
            // create a new index entry
            termLength = std::strlen(term) + 1;
            entryLength = sizeof(PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>);
            nodeLength = sizeof(PagedLinkedElement<IDFIndexEntry>);

            storedBytes = bytesStore->getBytes(termLength + entryLength + nodeLength);

            std::memcpy(storedBytes, term, termLength);
            entry = reinterpret_cast<PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>*>(&storedBytes[termLength]);
            node = reinterpret_cast<PagedLinkedElement<IDFIndexEntry>*>(&storedBytes[termLength + entryLength]);

            entry->element.term = storedBytes;
            entry->element.termFrequency = 1;
            entry->element.fileIdx = fileIdx;
            entry->element.fileFrequency = 1;
            node->next = NULL;

            // add sentinel
            entry->head = node;
            entry->tail = node;

            // actually insert the index
            map.insert({storedBytes, entry});
            numTerms++;
        } else {
            entry = search->second;
            if (entry->element.fileIdx != fileIdx) {
                // add a new node to the linked list
                nodeLength = sizeof(PagedLinkedElement<IDFIndexEntry>);

                storedBytes = bytesStore->getBytes(nodeLength);
                
                node = reinterpret_cast<PagedLinkedElement<IDFIndexEntry>*>(storedBytes);
                
                node->element.fileIdx = entry->element.fileIdx;
                node->element.fileFrequency = entry->element.fileFrequency;
                node->next = NULL;

                entry->tail->next = node;
                entry->tail = node;

                entry->element.termFrequency++;
                entry->element.fileIdx = fileIdx;
                entry->element.fileFrequency = 1;
            } else {
                entry->element.fileFrequency++;
            }
        }
    }

    std::shared_ptr<TFIndexResult> PagedStdMapTFIDFIndex::lookup(const char *term)
    {
        std::shared_ptr<TFIndexResult> rcEntry;
        PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>* entry;
        PagedLinkedElement<IDFIndexEntry>* p;

        rcEntry = std::shared_ptr<TFIndexResult>(new TFIndexResult());
        auto search = map.find(term);
        if (search != map.end()) {
            entry = search->second;
            rcEntry->term = entry->element.term;
            rcEntry->termFrequency = entry->element.termFrequency;
            p = entry->head->next;
            while (p != NULL) {
                rcEntry->files.push_back(p->element);
                p = p->next;
            }
            entry->head->element.fileIdx = entry->element.fileIdx;
            entry->head->element.fileFrequency = entry->element.fileFrequency;
            rcEntry->files.push_back(entry->head->element);
        }
        
        return rcEntry;
    }

    long PagedStdMapTFIDFIndex::getNumTerms()
    {
        return numTerms;
    }
}