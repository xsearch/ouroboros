#include "indexing/information/StdDirectTFIDFIndex.hpp"

namespace ouroboros
{
    StdDirectTFIDFIndex::StdDirectTFIDFIndex() : BaseInformationIndex(InformationIndexType::DIRECTTFIDF)
    {
        map = new std::unordered_map<std::string*, long, strptr_hash, strptr_equal>();
        terms = new std::vector<tfidf_entry_t*>();
    }

    StdDirectTFIDFIndex::~StdDirectTFIDFIndex()
    {
        for (tfidf_entry_t* elem : *terms) {
            delete elem->term;
            delete elem;
        }
        delete terms;
        delete map;
    }

    long StdDirectTFIDFIndex::insert(const char *term, long fileIdx)
    {
        std::string* key = new std::string(term);
        long termIdx = -1;
        tfidf_entry_t* elem;

        auto search = map->find(key);
        if (search != map->end()) {
            termIdx = search->second;
            elem = (*terms)[termIdx];
            elem->frequency++;
            delete key;
        } else {
            elem = new tfidf_entry_t{};
            elem->term = key;
            elem->fileIdx = fileIdx;
            elem->frequency = 1;
            terms->push_back(elem);
            termIdx = terms->size() - 1;
            (*map)[key] = termIdx;
        }

        return termIdx;
    }

    long StdDirectTFIDFIndex::lookup(const char *term)
    {
        std::string* key = new std::string(term);
        long termIdx = -1;

        auto search = map->find(key);
        if (search != map->end()) {
            termIdx = search->second;
        }

        delete key;

        return termIdx;
    }

    const std::tuple<const char*, long, long> StdDirectTFIDFIndex::reverseLookup(long idx)
    {
        tfidf_entry_t* elem = terms->at(idx);

        return std::make_tuple(elem->term->c_str(), elem->fileIdx, elem->frequency);
    }
}