#include "indexing/information/PagedStdMapTFIDFIndex.hpp"

#include <iostream>

namespace ouroboros
{
    long PagedStdMapTFIDFIndex::insert(const char *term, long fileIdx)
    {
        PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>* entry;
        PagedLinkedElement<IDFIndexEntry>* node;
        long idx = -1;

        auto search = map.find(term);
        if (search == map.end()) {
            // create a new index entry
            entry = indexStore->getNew();
            entry->element.term = termStore->store(term);
            entry->element.termFrequency = 1;
            entry->element.fileIdx = fileIdx;
            entry->element.fileFrequency = 1;

            // add a sentinel to the linked list
            node = listStore->getNew();
            node->element.fileIdx = -1;
            node->element.fileFrequency = -1;
            node->next = NULL;
            entry->head = node;
            entry->tail = node;

            // actually insert the index
            index.push_back(entry);
            idx = numTerms++;
            map.insert({entry->element.term, idx});
        } else {
            idx = search->second;
            entry = index.at(idx);
            if (entry->element.fileIdx != fileIdx) {
                // add a new node to the linked list
                node = listStore->getNew();
                node->element.fileIdx = entry->element.fileIdx;
                node->element.fileFrequency = entry->element.fileFrequency;
                node->next = NULL;
                entry->tail->next = node;
                entry->tail = node;

                entry->element.termFrequency++;
                entry->element.fileIdx = fileIdx;
                entry->element.fileFrequency = 1;
            } else {
                entry->element.fileFrequency++;
            }
        }

        return idx;
    }

    long PagedStdMapTFIDFIndex::lookup(const char *term)
    {
        long idx = -1;

        auto search = map.find(term);
        if (search != map.end()) {
            idx = search->second;
        }

        return idx;
    }

    std::shared_ptr<TFIndexResult> PagedStdMapTFIDFIndex::reverseLookup(long idx)
    {
        std::shared_ptr<TFIndexResult> rcEntry;
        PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>* entry;
        PagedLinkedElement<IDFIndexEntry>* p;

        rcEntry = std::shared_ptr<TFIndexResult>(new TFIndexResult());
        if (idx < numTerms) {
            entry = index.at(idx);
            // use the sentinel to store the cached index information of the last file
            entry->head->element.fileIdx = entry->element.fileIdx;
            entry->head->element.fileFrequency = entry->element.fileFrequency;

            rcEntry->term = entry->element.term;
            rcEntry->termFrequency = entry->element.termFrequency;
            p = entry->head->next;
            while (p != NULL) {
                rcEntry->files.push_back(p->element);
                p = p->next;
            }
            rcEntry->files.push_back(entry->head->element);
        }
        
        return rcEntry;
    }

    long PagedStdMapTFIDFIndex::getNumTerms()
    {
        return numTerms;
    }

}