#include "corestructures/OptimStringHashMap.hpp"

namespace ouroboros
{
    OptimStringHashMap::OptimStringHashMap(unsigned long numBuckets)
    {
        this->numBuckets = numBuckets;
        buckets = new head_bucket_t[numBuckets]{{OUROBOROS_OPTIMSTRINGHASHMAP_EMPTY_BUCKET}};
        keys = new SeqCapVector<char*>();
        pos = 0;
    }
    
    OptimStringHashMap::~OptimStringHashMap()
    {
        delete[] buckets;
        delete keys;
    }
    
    inline unsigned long OptimStringHashMap::hash(char* str)
    {
        return hashFn(str) % numBuckets;
    }

    long OptimStringHashMap::insert(char* key)
    {
        unsigned long bktid = hash(key);
        long idx;
        
        if (buckets[bktid].idx == OUROBOROS_OPTIMSTRINGHASHMAP_EMPTY_BUCKET) {
            idx = keys->push_back(key);
            buckets[bktid].idx = idx;
        } else {
            idx = keys->push_back(key);
            buckets[bktid].idx = idx;
        }

        return idx;
    }
    
    long OptimStringHashMap::lookup(char* key)
    {
        long bktid = hash(key);

        if (buckets[bktid].idx != OUROBOROS_OPTIMSTRINGHASHMAP_EMPTY_BUCKET) {
            if (std::strcmp(key, (*keys)[buckets[bktid].idx]) == 0) {
                return buckets[bktid].idx;
            }
        }

        return OUROBOROS_OPTIMSTRINGHASHMAP_NOT_FOUND;
    }
}
