#include "corestructures/DefaultPagedStringStore.hpp"

#include <cstring>

namespace ouroboros
{
    DefaultPagedStringStore::DefaultPagedStringStore(long pageSize) : 
        BasePagedStringStore(PagedStringStoreType::DEFAULT), pages(), pageSize(pageSize), pagePosition(0)
    {
        pageTail = new char[pageSize]{'x'};
        pages.push_back(pageTail);
    }

    DefaultPagedStringStore::~DefaultPagedStringStore()
    {
        while(pages.size() > 0) {
            pageTail = pages.back();
            pages.pop_back();
            delete[] pageTail;
        }
    }

    DefaultPagedStringStore::DefaultPagedStringStore(const DefaultPagedStringStore& other) : 
        BasePagedStringStore(PagedStringStoreType::DEFAULT), pages(), pageSize(other.pageSize), 
        pagePosition(other.pagePosition)
    {
        for (char* page: other.pages) {
            pageTail = new char[pageSize]();
            std::memcpy(pageTail, page, pageSize);
            pages.push_back(pageTail);
        }
    }

    DefaultPagedStringStore& DefaultPagedStringStore::operator=(const DefaultPagedStringStore& other)
    {
        if (this == &other) {
            return *this;
        }
        
        while(pages.size() > 0) {
            pageTail = pages.back();
            pages.pop_back();
            delete[] pageTail;
        }

        pageSize = other.pageSize;
        pagePosition = other.pagePosition;
        for (char* page: other.pages) {
            pageTail = new char[pageSize]();
            std::memcpy(pageTail, page, pageSize);
            pages.push_back(pageTail);
        }
        
        return *this;
    }

    DefaultPagedStringStore::DefaultPagedStringStore(DefaultPagedStringStore&& other) : 
        BasePagedStringStore(PagedStringStoreType::DEFAULT), pages(std::move(other.pages)), pageSize(other.pageSize),
        pagePosition(other.pagePosition)
    {
        pageTail = other.pageTail;

        other.pages = std::vector<char*>();
        other.pagePosition = 0;
        other.pageTail = NULL;
    }

    DefaultPagedStringStore& DefaultPagedStringStore::operator=(DefaultPagedStringStore&& other)
    {
        if (this == &other) {
            return *this;
        }
        
        while(pages.size() > 0) {
            pageTail = pages.back();
            pages.pop_back();
            delete[] pageTail;
        }

        pageSize = other.pageSize;
        pagePosition = other.pagePosition;
        pages = std::move(other.pages);
        pageTail = other.pageTail;

        other.pages = std::vector<char*>();
        other.pagePosition = 0;
        other.pageTail = NULL;
        
        return *this;
    }

    void DefaultPagedStringStore::expand()
    {
        pageTail = new char[pageSize]{'x'};
        pages.push_back(pageTail);
        pagePosition = 0;
    }

    char* DefaultPagedStringStore::store(const char* str)
    {
        int length = std::strlen(str) + 1;
        char* destination = nullptr;

        if ((pagePosition + length) > pageSize) {
            expand();
        }

        destination = &pageTail[pagePosition];
        std::memcpy(destination, str, length);
        pagePosition += length;

        return destination;
    }
}