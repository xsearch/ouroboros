#include "corestructures/SimpleHashMapV2.hpp"

namespace ouroboros
{
    SimpleHashMapV2::SimpleHashMapV2(unsigned long numBuckets)
    {
        long *bkts;
        this->numBuckets = numBuckets;
        bkts = new long[numBuckets]{-1};
        buckets = new long*[numBuckets]{NULL};
        for (unsigned long i = 0; i < numBuckets; i++) {
            buckets[i] = &bkts[i];
        }
    }

    SimpleHashMapV2::~SimpleHashMapV2()
    {
        delete[] buckets[0];
    }

    inline unsigned long SimpleHashMapV2::hash(char* str)
    {
        return hashFn(str) % numBuckets;
    }

    void SimpleHashMapV2::insert(char *key, long value)
    {
        unsigned long pos = hash(key);
        *(buckets[pos]) = value;
    }

    long SimpleHashMapV2::lookup(char *key)
    {
        unsigned long pos = hash(key);
        return *(buckets[pos]);
    }
}