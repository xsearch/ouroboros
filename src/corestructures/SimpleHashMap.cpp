#include "corestructures/SimpleHashMap.hpp"

namespace ouroboros
{
    SimpleHashMap::SimpleHashMap(unsigned long numBuckets)
    {
        this->numBuckets = numBuckets;
        buckets = new long[numBuckets]{-1};
    }

    SimpleHashMap::~SimpleHashMap()
    {
        delete[] buckets;
    }

    inline unsigned long SimpleHashMap::hash(char* str)
    {
        return hashFn(str) % numBuckets;
    }

    void SimpleHashMap::insert(char *key, long value)
    {
        unsigned long pos = hash(key);
        buckets[pos] = value;
    }

    long SimpleHashMap::lookup(char *key)
    {
        unsigned long pos = hash(key);
        return buckets[pos];
    }
}