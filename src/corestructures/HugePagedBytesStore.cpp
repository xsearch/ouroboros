#include "corestructures/HugePagedBytesStore.hpp"

#include <cstring>
#include <cstdlib>
#include <new>
#include <iostream>

extern "C"
{
    #include <sys/mman.h>
}

namespace ouroboros
{
    HugePagedBytesStore::HugePagedBytesStore(long pageSize) : pages(), pageSize(pageSize), pagePosition(0)
    {
        int rc;

        if (pageSize >= HUGE_PAGE_ALIGNMENT) {
            pageAlignment = HUGE_PAGE_ALIGNMENT;
        } else if (pageSize >= LARGE_PAGE_ALIGNMENT) {
            pageAlignment = LARGE_PAGE_ALIGNMENT;
        } else {
            pageAlignment = SMALL_PAGE_ALIGNMENT;
        }
        
        rc = posix_memalign((void**) &pageTail, pageAlignment, pageSize);
        if (rc != 0) {
            pagePosition = pageSize + 1;
            throw std::bad_alloc{};
        }

        rc = madvise(pageTail, pageSize, MADV_HUGEPAGE);
        if (rc != 0) {
            pagePosition = pageSize + 1;
            free(pageTail);
            throw std::bad_alloc{};
        }
        
        std::memset(pageTail, 'x', pageSize);
        pages.push_back(pageTail);
    }

    HugePagedBytesStore::~HugePagedBytesStore()
    {
        while(pages.size() > 0) {
            pageTail = pages.back();
            pages.pop_back();
            free(pageTail);
        }
    }

    HugePagedBytesStore::HugePagedBytesStore(const HugePagedBytesStore& other) : pages(), pageSize(other.pageSize), 
        pagePosition(other.pagePosition), pageAlignment(other.pageAlignment)
    {
        int rc;

        for (char* page: other.pages) {
            rc = posix_memalign((void**) &pageTail, pageAlignment, pageSize);
            if (rc != 0) {
                pagePosition = pageSize + 1;
                throw std::bad_alloc{};
            }

            rc = madvise(pageTail, pageSize, MADV_HUGEPAGE);
            if (rc != 0) {
                pagePosition = pageSize + 1;
                free(pageTail);
                throw std::bad_alloc{};
            }

            std::memcpy(pageTail, page, pageSize);
            pages.push_back(pageTail);
        }
    }

    HugePagedBytesStore& HugePagedBytesStore::operator=(const HugePagedBytesStore& other)
    {
        int rc;

        if (this == &other) {
            return *this;
        }
        
        while(pages.size() > 0) {
            pageTail = pages.back();
            pages.pop_back();
            free(pageTail);
        }

        pageSize = other.pageSize;
        pagePosition = other.pagePosition;
        pageAlignment = other.pageAlignment;
        for (char* page: other.pages) {
            rc = posix_memalign((void**) &pageTail, pageAlignment, pageSize);
            if (rc != 0) {
                pagePosition = pageSize + 1;
                throw std::bad_alloc{};
            }

            rc = madvise(pageTail, pageSize, MADV_HUGEPAGE);
            if (rc != 0) {
                pagePosition = pageSize + 1;
                free(pageTail);
                throw std::bad_alloc{};
            }

            std::memcpy(pageTail, page, pageSize);
            pages.push_back(pageTail);
        }
        
        return *this;
    }

    HugePagedBytesStore::HugePagedBytesStore(HugePagedBytesStore&& other) : pages(std::move(other.pages)), 
        pageSize(other.pageSize), pagePosition(other.pagePosition), pageAlignment(other.pageAlignment)
    {
        pageTail = other.pageTail;

        other.pages = std::deque<char*>();
        other.pagePosition = 0;
        other.pageTail = NULL;
    }

    HugePagedBytesStore& HugePagedBytesStore::operator=(HugePagedBytesStore&& other)
    {
        if (this == &other) {
            return *this;
        }
        
        while(pages.size() > 0) {
            pageTail = pages.back();
            pages.pop_back();
            delete[] pageTail;
        }

        pageSize = other.pageSize;
        pagePosition = other.pagePosition;
        pages = std::move(other.pages);
        pageTail = other.pageTail;

        other.pages = std::deque<char*>();
        other.pagePosition = 0;
        other.pageTail = NULL;
        
        return *this;
    }

    void HugePagedBytesStore::expand()
    {
        int rc;

        rc = posix_memalign((void**) &pageTail, pageAlignment, pageSize);
        if (rc != 0) {
            pagePosition = pageSize + 1;
            throw std::bad_alloc{};
        }

        rc = madvise(pageTail, pageSize, MADV_HUGEPAGE);
        if (rc != 0) {
            pagePosition = pageSize + 1;
            free(pageTail);
            throw std::bad_alloc{};
        }

        std::memset(pageTail, 'x', pageSize);
        pages.push_back(pageTail);
        pagePosition = 0;
    }

    char* HugePagedBytesStore::getBytes(long length)
    {
        char* destination = nullptr;

        if ((pagePosition + length) > pageSize) {
            expand();
        }

        destination = &pageTail[pagePosition];
        pagePosition += length;

        return destination;
    }
}