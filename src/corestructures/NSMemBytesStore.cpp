#include "corestructures/NSMemBytesStore.hpp"

#include <cstring>

namespace ouroboros
{
    NSMemBytesStore::NSMemBytesStore(long pageSize)
    {
        this->pageSize = pageSize;
        pages = new std::vector<char*>();
        pages->push_back(new char[pageSize]{});
        pageTail = pages->back();
        position = 0;
        lastLength = -1;
    }

    NSMemBytesStore::~NSMemBytesStore()
    {
        for (char* page : *pages) {
            delete[] page;
        }

        delete pages;
    }

    void NSMemBytesStore::createNewPage()
    {
        pages->push_back(new char[pageSize]);
        pageTail = pages->back();
        position = 0;
    }

    char* NSMemBytesStore::copy(const char* str)
    {
        lastLength = std::strlen(str);

        if ((lastLength + position) >= pageSize) {
            createNewPage();
        }

        std::memcpy(&pageTail[position], str, lastLength + 1);

        return &pageTail[position];
    }

    void NSMemBytesStore::next()
    {
        position += lastLength + 1;
        lastLength = -1;
    }
}