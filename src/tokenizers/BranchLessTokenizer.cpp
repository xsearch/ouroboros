#include "tokenizers/BranchLessTokenizer.hpp"

#include <cstring>

namespace ouroboros
{
    BranchLessTokenizer::BranchLessTokenizer(char *delim): BaseTokenizer(TokenizerType::BRANCHLESS) {
        this->delim = delim;
        
        for (auto i = 0; i < 256; i++) {
            charDict[i] = ~0;
        }

        for (auto i = 0; i < (int) std::strlen(delim); i++) {
            charDict[(int) delim[i]] = 0;
        }
    }
    
    inline void BranchLessTokenizer::getTokens(BaseDataBlock *dataBlock, BaseTokBlock *tokBlock) {
        getTokens((FileDataBlock*) dataBlock, (CTokBLock*) tokBlock);
    }

    void BranchLessTokenizer::getTokens(FileDataBlock *dataBlock, CTokBLock *tokBlock) {
        char *bufferData;
        char *bufferTok;
        char **tokens;
        char charPrev;
        char charNext;
        int length;
        int numTokens;
        int condition;

        bufferData = dataBlock->buffer;
        bufferTok = tokBlock->buffer;
        tokens = tokBlock->tokens;
        length = dataBlock->length;
        bufferTok[length] = '\0';

        numTokens = 0;
        
        charPrev = charDict[(int) bufferData[0]];
        bufferTok[0] = bufferData[0] & charPrev;
        
        condition = (charPrev == ~0);
        tokens[numTokens] = (condition) ? &bufferTok[0] : tokens[numTokens];
        numTokens = (condition) ? (numTokens + 1) : numTokens;

        for (auto i = 1; i < length; i++) {
            charNext = charDict[(int) bufferData[i]];
            bufferTok[i] = bufferData[i] & charNext;

            condition = (charPrev == 0) & (charNext == ~0);
            tokens[numTokens] = (condition) ? &bufferTok[i] : tokens[numTokens];
            numTokens = (condition) ? (numTokens + 1) : numTokens;

            charPrev = charNext;
        }

        tokBlock->numTokens = numTokens;
    }
}