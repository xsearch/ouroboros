#include "memorymanagers/InputBufferMemoryComponent.hpp"

#include <cstdlib>

namespace ouroboros
{
    InputBufferMemoryComponent::InputBufferMemoryComponent() : 
            BaseMemoryComponent(MemoryComponentType::INPUT_BUFFER)
    {
        bool success = true;
        try {
            this->blockVector = new std::vector<FileDataBlock*>();
        } catch(std::bad_alloc& e) {
            success = false;
        }
        if (!success) {
            this->blockVector = nullptr;
            allocated = false;
        } else {
            allocated = true;
        }
    }
    
    InputBufferMemoryComponent::~InputBufferMemoryComponent()
    {
        if (allocated) {
            for (auto ptr = this->blockVector->begin(); ptr != this->blockVector->end(); ptr++) {
                FileDataBlock* dataBlock = *ptr;
                delete dataBlock->buffer;
            }
            delete this->blockVector;
        }
    }

    std::vector<FileDataBlock*>* InputBufferMemoryComponent::getBlocks() {
        return this->blockVector;
    }

    bool InputBufferMemoryComponent::isAllocated() {
        return allocated;
    }
}
