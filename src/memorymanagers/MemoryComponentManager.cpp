#include "memorymanagers/MemoryComponentManager.hpp"

namespace ouroboros
{
    MemoryComponentManager::MemoryComponentManager()
    {
        dict = new std::unordered_map<MemoryComponentType, std::vector<BaseMemoryComponent*>*>();
        mtx = new std::mutex();
    }

    MemoryComponentManager::~MemoryComponentManager()
    {
        std::vector<BaseMemoryComponent*>* vect;
        BaseMemoryComponent* component;

        for (auto it = dict->begin(); it != dict->end(); it++) {
            vect = it->second;
            for (unsigned int i = 0; i < vect->size(); i++) {
                component = (*vect)[i];
                delete component;
            }
            delete vect;
        }
        delete dict;

        delete mtx;
    }

    void MemoryComponentManager::addMemoryComponent(MemoryComponentType type,
                                                    unsigned int id,
                                                    BaseMemoryComponent *component)
    {
        std::lock_guard<std::mutex> guard(*mtx);
        std::vector<BaseMemoryComponent*>* vect;
        unsigned int n;

        auto search = dict->find(type);
        if (search != dict->end()) {
            vect = (*dict)[type];
        } else {
            vect = new std::vector<BaseMemoryComponent*>();
            (*dict)[type] = vect;
        }

        if (vect->size() <= id) {
            n = id - vect->size() + 1;
            for (unsigned int i = 0 ; i < n; i++) {
                vect->push_back(nullptr);
            }
        }

        (*vect)[id] = component;
    }

    BaseMemoryComponent* MemoryComponentManager::getMemoryComponent(MemoryComponentType type, unsigned int id)
    {
        BaseMemoryComponent* component = nullptr;
        std::vector<BaseMemoryComponent*>* vect;

        auto search = dict->find(type);
        if (search != dict->end()) {
            vect = (*dict)[type];
            if (id < vect->size()) {
                component = (*vect)[id];
            }
        }

        return component;
    }
}
