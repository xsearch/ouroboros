#include "memorymanagers/TermIndexMemoryComponent.hpp"
#include "corestructures/HugePagedStringStore.hpp"
#include "indexing/data/StdMapTermIndex.hpp"
#include "indexing/data/DenseMapTermIndex.hpp"
#include "indexing/data/SwissMapTermIndex.hpp"
#include "indexing/data/ChainedTableTermIndex.hpp"
#include <iostream>

namespace ouroboros
{
    TermIndexMemoryComponent::TermIndexMemoryComponent(long pageSize,
                                                       TermIndexMemoryComponentType type,
                                                       unsigned long numBuckets) : 
        BaseMemoryComponent(MemoryComponentType::TERM_INDEX)
    {
        store = std::shared_ptr<BasePagedStringStore>(new HugePagedStringStore(pageSize));
        switch (type) {
            case TermIndexMemoryComponentType::STD:
                index = std::shared_ptr<BaseTermIndex>(new StdMapTermIndex(store));
                break;
#ifdef GOOGLE_DENSE_LIB
            case TermIndexMemoryComponentType::DENSE:
                index = std::shared_ptr<BaseTermIndex>(new DenseMapTermIndex(store));
                break;
#endif
#ifdef GOOGLE_SWISS_LIB
            case TermIndexMemoryComponentType::SWISS:
                index = std::shared_ptr<BaseTermIndex>(new SwissMapTermIndex(store));
                break;
#endif
            case TermIndexMemoryComponentType::CHAINED:
                index = std::shared_ptr<BaseTermIndex>(new ChainedTableTermIndex(store, numBuckets));
                break;
            default:
                std::cout << "WARN: picking default (STD) TermIndexMemoryComponent!" << std::endl;
                index = std::shared_ptr<BaseTermIndex>(new StdMapTermIndex(store));
        };
    }

    std::shared_ptr<BasePagedStringStore> TermIndexMemoryComponent::getPagedStringStore()
    {
        return store;
    }

    std::shared_ptr<BaseTermIndex> TermIndexMemoryComponent::getTermIndex()
    {
        return index;
    }
}