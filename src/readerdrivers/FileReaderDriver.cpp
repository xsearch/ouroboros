#include "readerdrivers/FileReaderDriver.hpp"

namespace ouroboros
{
    FileReaderDriver::FileReaderDriver(char *filepath, int blockSize) : BaseReaderDriver(ReaderDriverType::FILE) {
        this->filepath = filepath;
        this->filep = NULL;
        this->position = 0;
        this->blockSize = blockSize;
    }

    void FileReaderDriver::open() {
        filep = fopen(filepath, "rb");
        if (filep == NULL) {
            throw FileReaderException("ERROR: could not open " + std::string(filepath) + " !");
        }
    }

    void FileReaderDriver::close() {
        if (filep != NULL) {
            fclose(filep);
        }
    }
    
    inline void FileReaderDriver::readNextBlock(BaseDataBlock* dataBlock) {
        readNextBlock((FileDataBlock*) dataBlock);
    }

    void FileReaderDriver::readNextBlock(FileDataBlock* dataBlock) {
        int rc;
        
        rc = fread(dataBlock->buffer, sizeof(char), blockSize, filep);
        
        dataBlock->position = position;
        dataBlock->blockSize = blockSize;
        dataBlock->length = rc;
        dataBlock->filepath = filepath;
        
        if (rc != 0) {
            position += 1;
        }
    }
    
    void FileReaderDriver::readBlockAt(FileDataBlock* dataBlock, int position) {
        int rc;
        long offset;

        offset = (long) position * (long) blockSize;
        rc = fseek(filep, offset, SEEK_SET);
        
        if (rc == 0) {
            rc = fread(dataBlock->buffer, sizeof(char), blockSize, filep);
        } else {
            rc = 0;
        }
        
        dataBlock->position = position;
        dataBlock->blockSize = blockSize;
        dataBlock->length = rc;
        dataBlock->filepath = filepath;
    }
}