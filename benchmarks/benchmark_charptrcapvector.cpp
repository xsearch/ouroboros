#include <iostream>
#include <cstring>
#include <thread>
#include <atomic>
#include <cstdio>

extern "C"
{
    #include <sys/time.h>
}

#include "ouroboros.hpp"
#include <vector>
#include <tuple>

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_charptrcapvector <total size in bytes> \
<term size in bytes> <number of threads> <vector capacity step> <buffer capacity step>"

void work_index(long num_terms,
                vector<char*> *terms,
                CapacityVector<char*> *termIndex,
                CharPtrCapBuffer *buffer,
                int thread_id,
                int num_threads)
{
    char *token;

    for (long i = thread_id; i < num_terms; i += num_threads) {
        token = buffer->copyToPos(terms->at(i));
        termIndex->push_back(token);
        buffer->movePosNext();
    }
}

int benchmark(long num_terms, vector<char*> *terms, int num_threads, int vect_cap_step, long buff_cap_step)
{
    CapacityVector<char*> *termIndex;
    vector<thread*> threads;
    vector<CharPtrCapBuffer*> buffers;
    int rc = 0;

    termIndex = new CapacityVector<char*>(vect_cap_step);
    
    for (auto i = 0; i < num_threads; i++) {
        buffers.push_back(new CharPtrCapBuffer(buff_cap_step));
        threads.push_back(new thread(work_index, num_terms, terms, termIndex, buffers[i], i, num_threads));
    }

    for (auto i = 0; i < num_threads; i++) {
        threads[i]->join();
    }
    
    rc = std::strlen((*termIndex)[num_threads]);

    delete termIndex;
    for (auto i = 0; i < num_threads; i++) {
        delete buffers[i];
    }

    return rc;
}

int main(int argc, char **argv)
{
    long total_size;
    int term_size;
    int num_threads;
    int vect_cap_step;
    long buff_cap_step;
    long num_terms;
    
    char *buffer;
    vector<char*> *terms;
    tuple<std::vector<char*>*, char*> data;

    struct timeval start, end;
    double exec_time;
    int rc = 0;

    if (argc != 6) {
        cout << MSG << endl;
        return 1;
    }

    total_size = atol(argv[1]);
    term_size = atoi(argv[2]);
    num_threads = atoi(argv[3]);
    vect_cap_step = atoi(argv[4]);
    buff_cap_step = atol(argv[5]);
    num_terms = total_size / (long) term_size;

    data = generate_synthdata(num_terms, term_size - 1);
    buffer = get<1>(data);
    terms = get<0>(data);
    
    gettimeofday(&start, NULL);
    rc = benchmark(num_terms, terms, num_threads, vect_cap_step, buff_cap_step);
    gettimeofday(&end, NULL);
    exec_time = (double) (end.tv_sec - start.tv_sec) + (double) (end.tv_usec - start.tv_usec) / 1000000;

    delete terms;
    delete[] buffer;

    cout << "Finished inserting " << total_size << " bytes of data with " << num_threads \
        << " thread(s) and a term size of " << term_size << " bytes!" << endl;
    cout << "Total execution time: " << exec_time << " seconds" << endl;
    cout << "Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MB/sec" << endl;
    cout << "Throughput: " << ((double) num_terms / 1000) / exec_time << " kOPS" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}