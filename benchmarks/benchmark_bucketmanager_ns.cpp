#include <iostream>
#include <cstring>
#include <thread>
#include <atomic>
#include <cstdio>

extern "C"
{
    #include <sys/time.h>
}

#include "ouroboros.hpp"
#include <vector>
#include <tuple>
#include <functional>

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_bucketmanager_ns <total size in bytes> \
<term size in bytes> <number of threads> <bucket manager capacity step>"

void work_append(long num_terms,
                vector<char*> *terms,
                BucketManager<char*, long>* bucketMgr,
                int thread_id,
                int num_threads)
{
    long N = num_terms / num_threads;
    bucket_t<char*, long> *head;
    bucket_t<char*, long> *tail;
    long i;
    
    i = thread_id * N;
    head = bucketMgr->getNewBucket();
    head->key = terms->at(i);
    head->value = i;
    tail = head;

    for (i = thread_id * N + 1; i < (thread_id + 1) * N; i++) {
        tail->next = bucketMgr->getNewBucket();
        tail = tail->next;
        tail->key = terms->at(i);
        tail->value = i;
    }
}

tuple<double, int> benchmark(long num_terms, vector<char*> *terms, int num_threads, long capacity_step)
{
    vector<BucketManager<char*, long>*> bucketMgrs;
    vector<thread> threads;

    struct timeval start, end;
    double exec_time;
    int rc = 0;
    
    gettimeofday(&start, NULL);
    for (int i = 0; i < num_threads; i++) {
        bucketMgrs.push_back(new BucketManager<char*, long>(capacity_step));
    }

    for (auto i = 0; i < num_threads; i++) {
        threads.push_back(thread(work_append, num_terms, terms, bucketMgrs[i], i, num_threads));
    }

    for (auto i = 0; i < num_threads; i++) {
        threads[i].join();
    }
    gettimeofday(&end, NULL);
    exec_time = (double) (end.tv_sec - start.tv_sec) + (double) (end.tv_usec - start.tv_usec) / 1000000;

    for (auto i = 0; i < num_threads; i++) {
        delete bucketMgrs[i];
    }

    return make_tuple(exec_time, rc);
}

int main(int argc, char **argv)
{
    long total_size;
    int term_size;
    int num_threads;
    long capacity_step;
    long num_terms;
    
    tuple<std::vector<char*>*, char*> data;
    char *buffer;
    vector<char*> *terms;
    
    tuple<double, int> benchmark_rc;
    double exec_time;
    int rc;
    

    if (argc != 5) {
        cout << MSG << endl;
        return 1;
    }

    total_size = atol(argv[1]);
    term_size = atoi(argv[2]);
    num_threads = atoi(argv[3]);
    capacity_step = atol(argv[4]);
    num_terms = total_size / (long) term_size;

    data = generate_synthdata(num_terms, term_size - 1);
    buffer = get<1>(data);
    terms = get<0>(data);
    
    benchmark_rc = benchmark(num_terms, terms, num_threads, capacity_step);
    exec_time = get<0>(benchmark_rc);
    rc = get<1>(benchmark_rc);

    delete terms;
    delete[] buffer;

    cout << "Finished appending " << total_size << " bytes of data with " << num_threads << " thread(s), " \
         << "a term size of " << term_size << " bytes and a capacity step of " << capacity_step << "!" << endl;
    cout << "Total execution time: " << exec_time << " seconds" << endl;
    cout << "Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MB/sec" << endl;
    cout << "Throughput: " << ((double) num_terms / 1000) / exec_time << " kOPS" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}