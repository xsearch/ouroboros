#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>

#include "ouroboros.hpp"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_rd_wave <source file> <total size in bytes> <number of numa nodes> \
<number of reader threads> <block size in bytes>"

#define QUEUE_SIZE_RATIO 2
#define BLOCK_ADDON_SIZE 4096
#define DELIMITERS " \t\n"

void work_read(MemoryComponentManager* manager,
               atomic<int>* file_counter,
               atomic<long>* total_num_blocks_read,
               vector<char*>* filenames,
               unsigned int numa_id,
               unsigned int reader_id,
               int block_size)
{
    // self configure to run in numa_id NUMA node
    NUMAConfig config;
    config.set_numa_node(numa_id);

    {
        FileDualQueueMemoryComponent* component;
        DualQueue<FileDataBlock*> *queue;
        WaveFileReaderDriver *reader;
        FileDataBlock *dataBlock;
        long num_blocks_read(0);
        char delims[32] = DELIMITERS;
        int i;

        // get the queue component identified by numa_id from the manager
        component = (FileDualQueueMemoryComponent*) 
                    manager->getMemoryComponent(MemoryComponentType::DUALQUEUE, reader_id);
        // get the queue from the queue component
        queue = component->getDualQueue();

        // load balancing is performed through an atomic counter
        i = file_counter->fetch_add(1, std::memory_order_seq_cst);
        while (i < (int) filenames->size()) {
            // create a new reader driver for the current file
            reader = new WaveFileReaderDriver((*filenames)[i], block_size, BLOCK_ADDON_SIZE, delims);
            
            // try to open the file in the reader driver; if it errors out print message and skip the file
            try {
                reader->open();
            } catch (exception &e) {
                cout << "ERR: could not open file " << (*filenames)[i] << endl;
                delete reader;
                i = file_counter->fetch_add(1, std::memory_order_seq_cst);
                continue;
            }

            while (true) {
                // pop empty data block from the queue
                dataBlock = queue->pop_empty();
                
                // read a block of data from the file into data block buffer
                reader->readNextBlock(dataBlock);
                
                // push full data block to queue (in this case it pushed to the empty queue since there is no consumer)
                queue->push_empty(dataBlock);

                // if the reader driver reached the end of the file break from the while loop and read next file
                if (dataBlock->length == 0) {
                    break;
                } else {
                    num_blocks_read++;
                }
            }

            // close the reader and free memory
            reader->close();
            delete reader;
            
            // load balancing is performed through an atomic counter
            i = file_counter->fetch_add(1, std::memory_order_seq_cst);
        }

        (*total_num_blocks_read) += num_blocks_read;
    }
}

void work_init(MemoryComponentManager* manager,
               unsigned int numa_id,
               unsigned int reader_id,
               int queue_size,
               int block_size)
{
    // self configure to run in numa_id NUMA node
    NUMAConfig config;
    config.set_numa_node(numa_id);

    {
        FileDualQueueMemoryComponent* component;

        // create a new queue component; the component is responsible with intializing the queue and the queue elements
        component = new FileDualQueueMemoryComponent(queue_size, block_size + BLOCK_ADDON_SIZE);

        // add the queue component to the manager identified by the current numa_id
        manager->addMemoryComponent(MemoryComponentType::DUALQUEUE, reader_id, component);
    }
}

void read_filenames(char *file_path, vector<vector<char*>> &filenames, int num_numa_nodes)
{
    FILE *fp;
    char *buffer, *rc;
    int i;

    fp = fopen(file_path, "r");
    if (fp == NULL) {
        cout << "ERR: cannot open file " << file_path << endl;
        return;
    }

    i = 0;
    while (true) {
        buffer = new char[4096];
        
        rc = fgets(buffer, 4096, fp);
        if (rc == NULL) {
            delete buffer;
            break;
        }
        if (buffer[strlen(buffer) - 1] == '\n') {
            buffer[strlen(buffer) - 1] = '\0';
        }

        filenames[i % num_numa_nodes].push_back(buffer);
        i++;
    }

    fclose(fp);
}

tuple<double, long, int, int, int> benchmark(char *file_path, int set_num_numa_nodes, int num_readers, int block_size)
{
    // the manager is used to store and provide access to NUMA sensitive components (i.e. the queues)
    MemoryComponentManager* manager;
    FileDualQueueMemoryComponent* component;
    
    // list of names of input files that will be read by the program
    vector<vector<char*>> filenames;
    char* filename;

    // list of threads that initialize the NUMA sensitive components
    vector<thread> threads_init;

    // list of threads that will read the contents of the input files
    vector<thread> threads_read;

    // atomic counter used by the reader threads to go over the input files
    vector<atomic<int>*> file_counters;

    // atomic int storing the total number of blocks read
    atomic<long> num_blocks_read(0);
    
    int num_numa_nodes;
    int queue_size;
    NUMAConfig config;

    chrono::time_point<chrono::high_resolution_clock> start, end;
    chrono::duration<double> diff;
    double exec_time = 0.0;
    long rc = -1;
    
    // find out how many NUMA nodes exist in the system
    num_numa_nodes = config.get_num_numa_nodes();
    num_numa_nodes = (set_num_numa_nodes < num_numa_nodes) ? set_num_numa_nodes : num_numa_nodes;
    num_numa_nodes = (num_readers < num_numa_nodes) ? num_readers : num_numa_nodes;

    // read the input file names from the file passed as argument to the program
    for (int i = 0; i < num_numa_nodes; i++) {
        filenames.push_back(vector<char*>());
        file_counters.push_back(new atomic<int>(0));
    }
    read_filenames(file_path, filenames, num_numa_nodes);

    // create each queue in a specific NUMA node and add the queues to the manager
    manager = new MemoryComponentManager();
    queue_size = QUEUE_SIZE_RATIO;
    
    for (int i = 0; i < num_readers; i++) {
        threads_init.push_back(thread(work_init,
                                      manager,
                                      i % num_numa_nodes,
                                      i,
                                      queue_size,
                                      block_size));
    }

    for (int i = 0; i < num_readers; i++) {
        threads_init[i].join();
    }

    // start running the benchmark by creating the reader threads
    start = chrono::high_resolution_clock::now();
    for (auto i = 0; i < num_readers; i++) {
        threads_read.push_back(thread(work_read,
                                      manager,
                                      file_counters[i % num_numa_nodes],
                                      &num_blocks_read,
                                      &filenames[i % num_numa_nodes],
                                      i % num_numa_nodes,
                                      i,
                                      block_size));
    }
    
    for (auto i = 0; i < num_readers; i++) {
        threads_read[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    exec_time = diff.count();
    rc = num_blocks_read;

    // free all NUMA sensitive memory components
    delete manager;

    // free the buffers storing the input file names
    for (int i = 0; i < num_numa_nodes; i++) {
        while (filenames[i].size() > 0) {
            filename = filenames[i].back();
            filenames[i].pop_back();
            delete[] filename;
        }
        delete file_counters[i];
    }

    return make_tuple(exec_time, rc, num_numa_nodes, queue_size, num_readers);
}

int main(int argc, char **argv)
{
    char source_file_path[4096];
    long total_size;
    int num_numa_nodes;
    int num_readers;
    int block_size;
    int queue_size;
    int num_queues;

    tuple<double, long, int, int, int> benchmark_rc;
    double exec_time;
    long rc;

    if (argc != 6) {
        cout << MSG << endl;
        return 1;
    }
    
    strcpy(source_file_path, argv[1]);
    total_size = atol(argv[2]);
    num_numa_nodes = atoi(argv[3]);
    num_readers = atoi(argv[4]);
    block_size = atoi(argv[5]);

    benchmark_rc = benchmark(source_file_path, num_numa_nodes, num_readers, block_size);
    exec_time = get<0>(benchmark_rc);
    rc = get<1>(benchmark_rc);
    num_numa_nodes = get<2>(benchmark_rc);
    queue_size = get<3>(benchmark_rc);
    num_queues = get<4>(benchmark_rc);

    cout << "Finished reading " << total_size << " bytes of data:" << endl;
    cout << "- number of reader threads: " << num_readers << endl;
    cout << "- number of NUMA nodes: " << num_numa_nodes << endl;
    cout << "- number of queues: " << num_queues << endl;
    cout << "- queue size: " << queue_size << endl;
    cout << "- block size: " << block_size << " bytes" << endl;
    cout << "- total execution time: " << exec_time << " seconds" << endl;
    cout << "- throughput: " << (total_size / (1024 * 1024)) / exec_time << " MiB/sec" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}
