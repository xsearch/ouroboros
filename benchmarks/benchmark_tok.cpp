#include <iostream>
#include <cstring>
#include <thread>
#include <atomic>
#include <cstdio>

extern "C"
{
    #include <sys/time.h>
}

#include "ouroboros.hpp"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_tok <total size in bytes> <number of threads> \
<number of tokenizers> <block size in bytes>"

#define QUEUE_SIZE 1024
#define DELIMS " \t\n/_.,;:"

void work_tok(DualQueue<FileDataBlock*> *queue, atomic<long> *numTokens, int block_size)
{
    FileDataBlock *dataBlock;
    BranchLessTokenizer *tokenizer;
    CTokBLock *tokBlock;
    char *buffer;
    char **tokens;
    char delims[32] = DELIMS;
    long toks(0);
    int length;

    buffer = new char[block_size + 1];
    tokens = new char*[block_size / 2 + 1];
    tokBlock = new CTokBLock();
    tokBlock->buffer = buffer;
    tokBlock->tokens = tokens;
    tokenizer = new BranchLessTokenizer(delims);

    while (true) {
        dataBlock = queue->pop_full();
        
        length = dataBlock->length;
        if (length > 0) {
            tokenizer->getTokens(dataBlock, tokBlock);
            toks += tokBlock->numTokens;
        }
        
        queue->push_empty(dataBlock);

        if (length == -1) {
            break;
        }
    }

    *numTokens += toks;
}

void work_read(DualQueue<FileDataBlock*> *queue, atomic<int> *position, int maxBlocks, int block_size)
{
    FileDataBlock *dataBlock;
    int pos;

    pos = position->fetch_add(1, std::memory_order_seq_cst);
    while (pos < maxBlocks) {
        dataBlock = queue->pop_empty();
        
        memset(dataBlock->buffer, ' ', block_size);
        for (auto i = 0; i < block_size; i+=8) {
            auto c = 'A' + (char) (pos % 26);
            dataBlock->buffer[i] = (char) c;
            dataBlock->buffer[i + 1] = (char) c;
            dataBlock->buffer[i + 2] = (char) c;
            dataBlock->buffer[i + 3] = (char) c;
            dataBlock->buffer[i + 4] = (char) c;
            dataBlock->buffer[i + 5] = (char) c;
            dataBlock->buffer[i + 6] = (char) c;
        }
        
        dataBlock->blockSize = block_size;
        dataBlock->length = block_size;
        
        queue->push_full(dataBlock);

        pos = position->fetch_add(1, std::memory_order_seq_cst);
    }
}

long benchmark(long num_blocks, int num_threads, int num_tokenizers, int block_size)
{
    FileDataBlock *dataBlocks;
    FileDataBlock *finalBlock;
    char *buffers;
    DualQueue<FileDataBlock*> queue(QUEUE_SIZE);
    vector<thread*> threads_read;
    vector<thread*> threads_tok;
    atomic<long> numTokens(0);
    atomic<int> position(0);
    long rc;

    dataBlocks = new FileDataBlock[QUEUE_SIZE]();
    buffers = new char[QUEUE_SIZE * (long) block_size];

    for (auto i = 0; i < QUEUE_SIZE; i++) {
        dataBlocks[i].buffer = &buffers[i * block_size];
        queue.push_empty(&dataBlocks[i]);
    }
    
    for (auto i = 0; i < num_threads; i++) {
        threads_read.push_back(new thread(work_read, &queue, &position, num_blocks, block_size));
    }

    for (auto i = 0; i < num_tokenizers; i++) {
        threads_tok.push_back(new thread(work_tok, &queue, &numTokens, block_size));
    }

    for (auto i = 0; i < num_threads; i++) {
        threads_read[i]->join();
    }

    for (auto i = 0; i < num_tokenizers; i++) {
        finalBlock = queue.pop_empty();
        finalBlock->length = -1;
        queue.push_full(finalBlock);
    }
    
    for (auto i = 0; i < num_tokenizers; i++) {
        threads_tok[i]->join();
    }
    
    rc = numTokens;

    delete[] buffers;
    delete[] dataBlocks;

    return rc;
}

int main(int argc, char **argv)
{
    long total_size;
    int num_threads;
    int num_tokenizers;
    int block_size;
    int num_blocks;
    struct timeval start, end;
    double exec_time;
    long rc = 0;

    if (argc != 5) {
        cout << MSG << endl;
        return 1;
    }

    total_size = atol(argv[1]);
    num_threads = atoi(argv[2]);
    num_tokenizers = atoi(argv[3]);
    block_size = atoi(argv[4]);
    num_blocks = (int) (total_size / (long) block_size);

    gettimeofday(&start, NULL);
    rc = benchmark(num_blocks, num_threads, num_tokenizers, block_size);
    gettimeofday(&end, NULL);
    exec_time = (double) (end.tv_sec - start.tv_sec) + (double) (end.tv_usec - start.tv_usec) / 1000000;

    cout << "Finished tokenizing " << total_size << " bytes of data with " << num_threads << " thread(s), " \
        << num_tokenizers << " tokenizer(s) and a block size of " << block_size << " bytes!" << endl;
    cout << "Total execution time: " << exec_time << " seconds" << endl;
    cout << "Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MB/sec" << endl;
    cout << "Throughput: " << rc / exec_time << " tokens/sec" << endl;
    cout << "Return code: " << rc << endl;


    return 0;
}
