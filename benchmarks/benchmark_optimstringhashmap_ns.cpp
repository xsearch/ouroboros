#include <iostream>
#include <cstring>
#include <thread>
#include <atomic>
#include <cstdio>

extern "C"
{
    #include <sys/time.h>
}

#include "ouroboros.hpp"
#include <vector>
#include <tuple>
#include <functional>

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_optimstringhashmap_ns <total size in bytes> \
<term size in bytes> <number of threads> <per thread number of buckets>"

void work_index(long num_terms,
                vector<char*> *terms,
                OptimStringHashMap* termIndex,
                int thread_id,
                int num_threads)
{
    long N = num_terms / num_threads;

    for (long i = thread_id * N; i < (thread_id + 1) * N; i++) {
        termIndex->insert(terms->at(i));
    }
}

tuple<double, int> benchmark(long num_terms, vector<char*> *terms, int num_threads, long num_buckets)
{
    vector<OptimStringHashMap*> termIndexes;
    vector<thread> threads;
    
    std::tuple<long, bool> query_res;
    char query_term[10] = "000000000";
    bool found_term;

    struct timeval start, end;
    double exec_time;
    int rc = 0;
    
    gettimeofday(&start, NULL);
    for (int i = 0; i < num_threads; i++) {
        termIndexes.push_back(new OptimStringHashMap(num_buckets));
    }

    for (int i = 0; i < num_threads; i++) {
        threads.push_back(thread(work_index, num_terms, terms, termIndexes[i], i, num_threads));
    }

    for (int i = 0; i < num_threads; i++) {
        threads[i].join();
    }
    gettimeofday(&end, NULL);
    exec_time = (double) (end.tv_sec - start.tv_sec) + (double) (end.tv_usec - start.tv_usec) / 1000000;
    
    found_term = false;
    for (auto i = 0; i < num_threads; i++) {
        long search = termIndexes[i]->lookup((*terms)[num_threads]);
        if (search == num_threads) {
            found_term = true;
            break;
        }
    }
    if (!found_term) {
        cout << "WARN: term <" << (*terms)[num_threads] << "> was not found in the hashmap!" << endl;
    } else {
        rc = get<0>(query_res);
    }
    
    found_term = false;
    for (auto i = 0; i < num_threads; i++) {
        long search = termIndexes[i]->lookup(query_term);
        if (search != OUROBOROS_OPTIMSTRINGHASHMAP_NOT_FOUND) {
            found_term = true;
            break;
        }
    }
    if (found_term) {
        cout << "WARN: term <" << query_term << "> was found in the hashmap!" << endl;
    }

    for (auto i = 0; i < num_threads; i++) {
        delete termIndexes[i];
    }

    return make_tuple(exec_time, rc);
}

int main(int argc, char **argv)
{
    long total_size;
    int term_size;
    int num_threads;
    long num_buckets;
    long num_terms;
    
    tuple<std::vector<char*>*, char*> data;
    char *buffer;
    vector<char*> *terms;
    
    tuple<double, int> benchmark_rc;
    double exec_time;
    int rc;
    

    if (argc != 5) {
        cout << MSG << endl;
        return 1;
    }

    total_size = atol(argv[1]);
    term_size = atoi(argv[2]);
    num_threads = atoi(argv[3]);
    num_buckets = atol(argv[4]);
    num_terms = total_size / (long) term_size;

    data = generate_synthdata(num_terms, term_size - 1);
    buffer = get<1>(data);
    terms = get<0>(data);
    
    benchmark_rc = benchmark(num_terms, terms, num_threads, num_buckets);
    exec_time = get<0>(benchmark_rc);
    rc = get<1>(benchmark_rc);

    delete terms;
    delete[] buffer;

    cout << "Finished hashing/inserting " << total_size << " bytes of data with " << num_threads \
         << " thread(s) and a term size of " << term_size << " bytes!" << endl;
    cout << "Total execution time: " << exec_time << " seconds" << endl;
    cout << "Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MiB/sec" << endl;
    cout << "Throughput: " << ((double) num_terms / 1000) / exec_time << " kOPS" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}