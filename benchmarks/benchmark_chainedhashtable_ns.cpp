#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>
#include <functional>

#include "ouroboros.hpp"
#include <vector>
#include <tuple>

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_chainedhashtable_ns <total size in bytes> \
<term size in bytes> <number of threads> <per thread number of buckets>"

void work_index(long num_terms,
                vector<char*> *terms,
                ChainedHashTable<char*, long>*& termIndex,
                int thread_id,
                int num_threads,
                long num_buckets)
{
    NUMAConfig config;
    config.set_task_numa_node(thread_id);

    {
        long N = num_terms / num_threads;
        termIndex = new ChainedHashTable<char*, long>(num_buckets);

        for (long i = thread_id * N; i < (thread_id + 1) * N; i++) {
            termIndex->insert(terms->at(i), i);
        }
    }
}

tuple<double, int> benchmark(long num_terms, vector<char*> *terms, int num_threads, long num_buckets)
{
    vector<ChainedHashTable<char*, long>*> termIndexes;
    vector<thread> threads;

    char query_term[10] = "000000000";
    bool found_term;
    long value;

    chrono::time_point<chrono::high_resolution_clock> start, end;
    chrono::duration<double> diff;
    double exec_time;
    int rc = -1;
    
    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_threads; i++) {
        termIndexes.push_back(NULL);
    }

    for (int i = 0; i < num_threads; i++) {
        threads.push_back(thread(work_index, num_terms, terms, std::ref(termIndexes[i]), i, num_threads, num_buckets));
    }

    for (int i = 0; i < num_threads; i++) {
        threads[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    exec_time = diff.count();
    
    found_term = false;
    for (int i = 0; i < num_threads; i++) {
        bool search = termIndexes[i]->lookup((*terms)[num_threads], value);
        if (search == true) {
            found_term = true;
            break;
        }
    }
    if (!found_term) {
        cout << "WARN: term <" << (*terms)[num_threads] << "> was not found in the hashmap!" << endl;
    } else {
        rc = (int) value;
    }
    
    found_term = false;
    for (int i = 0; i < num_threads; i++) {
        bool search = termIndexes[i]->lookup(query_term, value);
        if (search == true) {
            found_term = true;
            break;
        }
    }
    if (found_term) {
        cout << "WARN: term <" << query_term << "> was found in the hashmap!" << endl;
    }

    for (auto i = 0; i < num_threads; i++) {
        delete termIndexes[i];
    }

    return make_tuple(exec_time, rc);
}

int main(int argc, char **argv)
{
    long total_size;
    int term_size;
    int num_threads;
    long num_buckets;
    long num_terms;
    
    tuple<std::vector<char*>*, char*> data;
    char *buffer;
    vector<char*> *terms;
    
    tuple<double, int> benchmark_rc;
    double exec_time;
    int rc;
    

    if (argc != 5) {
        cout << MSG << endl;
        return 1;
    }

    total_size = atol(argv[1]);
    term_size = atoi(argv[2]);
    num_threads = atoi(argv[3]);
    num_buckets = atol(argv[4]);
    num_terms = total_size / (long) term_size;

    data = generate_synthdata(num_terms, term_size - 1);
    buffer = get<1>(data);
    terms = get<0>(data);
    
    benchmark_rc = benchmark(num_terms, terms, num_threads, num_buckets);
    exec_time = get<0>(benchmark_rc);
    rc = get<1>(benchmark_rc);

    delete terms;
    delete[] buffer;

    cout << "Finished hashing/inserting " << total_size << " bytes of data with " << num_threads \
         << " thread(s) and a term size of " << term_size << " bytes!" << endl;
    cout << "Total execution time: " << exec_time << " seconds" << endl;
    cout << "Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MiB/sec" << endl;
    cout << "Throughput: " << ((double) num_terms / 1000) / exec_time << " kOPS" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}