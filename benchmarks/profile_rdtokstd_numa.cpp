#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>
#include <filesystem>

#include "ouroboros.hpp"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./profile_rdtokstd_numa <source file> <total size in bytes> \
<number of reader threads> <number of tokenizer threads> <block size in bytes>"

#define QUEUE_SIZE_RATIO 4
#define DELIMIMITERS " \t\n/_.,;:"

// vectors used to store the fine grained execution time
struct fine_grained_times {
    vector<double*> rd_pop_times;
    vector<double*> rd_read_times;
    vector<double*> rd_push_times;
    vector<int> rd_num_times;
    vector<double*> tok_pop_times;
    vector<double*> tok_tok_times;
    vector<double*> tok_push_times;
    vector<int> tok_num_times;
};

void work_tok(MemoryComponentManager* manager,
              atomic<long>* total_num_tokens,
              fine_grained_times* times,
              unsigned int numa_id,
              int block_size)
{
    NUMAConfig config;
    config.set_task_numa_node(numa_id);

    {
        FileDualQueueMemoryComponent* component;
        DualQueue<FileDataBlock*> *queue;
        FileDataBlock *dataBlock;
        StandardTokenizer *tokenizer;
        CTokBLock *tokBlock;
        char *buffer;
        char **tokens;
        char delims[32] = DELIMIMITERS;
        long num_tokens(0);
        int length;
        int i, j;

        // allocate the buffers, the list of tokens for the tokenizer data block and create the 
        buffer = new char[block_size + 1];
        tokens = new char*[block_size / 2 + 1];
        tokBlock = new CTokBLock();
        tokBlock->buffer = buffer;
        tokBlock->tokens = tokens;
        tokenizer = new StandardTokenizer(delims);

        // get the queue component identified by numa_id from the manager
        component = (FileDualQueueMemoryComponent*) manager->getMemoryComponent(MemoryComponentType::DUALQUEUE,
                                                                                numa_id);
        // get the queue from the queue component
        queue = component->getDualQueue();

        chrono::time_point<chrono::high_resolution_clock> start, end;
        chrono::duration<double> diff;
        double pop_time(0.0), tok_time(0.0), push_time(0.0);

        // load balancing is achieved through the queue
        while (true) {
            // pop full data block from the queue
            start = chrono::high_resolution_clock::now();
            dataBlock = queue->pop_full();
            end = chrono::high_resolution_clock::now();
            diff = end - start;
            pop_time = diff.count() * 1000000.0;

            i = dataBlock->fileIdx;
            j = dataBlock->position;
            
            // if the data in the block has a length greater than 0 then tokenzie, otherwise exit the while loop
            length = dataBlock->length;
            if (length > 0) {
                start = chrono::high_resolution_clock::now();
                tokenizer->getTokens(dataBlock, tokBlock);
                end = chrono::high_resolution_clock::now();
                diff = end - start;
                tok_time = diff.count() * 1000000.0;
                num_tokens += tokBlock->numTokens;
            }
            
            start = chrono::high_resolution_clock::now();
            queue->push_empty(dataBlock);
            end = chrono::high_resolution_clock::now();
            diff = end - start;
            push_time = diff.count() * 1000000.0;

            if (length == -1) {
                break;
            } else {
                if (length > 0) {
                    times->tok_pop_times[i][j] = pop_time;
                    times->tok_tok_times[i][j] = tok_time;
                    times->tok_push_times[i][j] = push_time;
                }
            }
        }

        delete tokenizer;
        delete tokBlock;
        delete[] tokens;
        delete[] buffer;

        *total_num_tokens += num_tokens;
    }
}

void work_read(MemoryComponentManager* manager,
               atomic<int>* file_counter,
               fine_grained_times* times,
               vector<char*>* filenames,
               unsigned int numa_id,
               int block_size)
{
    // self configure to run in numa_id NUMA node
    NUMAConfig config;
    config.set_task_numa_node(numa_id);

    {
        FileDualQueueMemoryComponent* component;
        DualQueue<FileDataBlock*> *queue;
        PFileReaderDriver *reader;
        FileDataBlock *dataBlock;
        int i, j;
        filesystem::path *file_path;

        // get the queue component identified by numa_id from the manager
        component = (FileDualQueueMemoryComponent*) manager->getMemoryComponent(MemoryComponentType::DUALQUEUE,
                                                                                numa_id);
        // get the queue from the queue component
        queue = component->getDualQueue();

        chrono::time_point<chrono::high_resolution_clock> start, end;
        chrono::duration<double> diff;
        double pop_time(0.0), read_time(0.0), push_time(0.0);

        // load balancing is performed through an atomic counter
        i = file_counter->fetch_add(1, std::memory_order_seq_cst);
        while (i < (int) filenames->size()) {
            // create a new reader driver for the current file
            reader = new PFileReaderDriver((*filenames)[i], block_size);
            
            // try to open the file in the reader driver; if it errors out print message and skip the file
            try {
                reader->open();
            } catch (exception &e) {
                cout << "ERR: could not open file " << (*filenames)[i] << endl;
                delete reader;
                i = file_counter->fetch_add(1, std::memory_order_seq_cst);
                continue;
            }

            file_path = new filesystem::path((*filenames)[i]);
            if (filesystem::file_size(*file_path) % block_size == 0) {
                times->rd_pop_times[i] = new double[filesystem::file_size(*file_path) / block_size];
                times->rd_read_times[i] = new double[filesystem::file_size(*file_path) / block_size];
                times->rd_push_times[i] = new double[filesystem::file_size(*file_path) / block_size];
                times->rd_num_times[i] = (filesystem::file_size(*file_path) / block_size);
                times->tok_pop_times[i] = new double[filesystem::file_size(*file_path) / block_size];
                times->tok_tok_times[i] = new double[filesystem::file_size(*file_path) / block_size];
                times->tok_push_times[i] = new double[filesystem::file_size(*file_path) / block_size];
                times->tok_num_times[i] = (filesystem::file_size(*file_path) / block_size);
            } else {
                times->rd_pop_times[i] = new double[filesystem::file_size(*file_path) / block_size + 1];
                times->rd_read_times[i] = new double[filesystem::file_size(*file_path) / block_size + 1];
                times->rd_push_times[i] = new double[filesystem::file_size(*file_path) / block_size + 1];
                times->rd_num_times[i] = (filesystem::file_size(*file_path) / block_size + 1);
                times->tok_pop_times[i] = new double[filesystem::file_size(*file_path) / block_size + 1];
                times->tok_tok_times[i] = new double[filesystem::file_size(*file_path) / block_size + 1];
                times->tok_push_times[i] = new double[filesystem::file_size(*file_path) / block_size + 1];
                times->tok_num_times[i] = (filesystem::file_size(*file_path) / block_size + 1);
            }
            delete file_path;

            while (true) {
                // pop empty data block from the queue
                start = chrono::high_resolution_clock::now();
                dataBlock = queue->pop_empty();
                end = chrono::high_resolution_clock::now();
                diff = end - start;
                pop_time = diff.count() * 1000000.0;
                
                // read a block of data from the file into data block buffer
                start = chrono::high_resolution_clock::now();
                reader->readNextBlock(dataBlock);
                end = chrono::high_resolution_clock::now();
                diff = end - start;
                read_time = diff.count() * 1000000.0;

                dataBlock->fileIdx = i;
                j = dataBlock->position;
                
                // push full data block to queue (in this case it pushed to the empty queue since there is no consumer)
                start = chrono::high_resolution_clock::now();
                queue->push_full(dataBlock);
                end = chrono::high_resolution_clock::now();
                diff = end - start;
                push_time = diff.count() * 1000000.0;

                // if the reader driver reached the end of the file break from the while loop and read next file
                if (dataBlock->length == 0) {
                    break;
                } else {
                    times->rd_pop_times[i][j] = pop_time;
                    times->rd_read_times[i][j] = read_time;
                    times->rd_push_times[i][j] = push_time;
                }
            }

            // close the reader and free memory
            reader->close();
            delete reader;
            
            // load balancing is performed through an atomic counter
            i = file_counter->fetch_add(1, std::memory_order_seq_cst);
        }
    }
}

void work_init(MemoryComponentManager* manager, unsigned int numa_id, int queue_size, int block_size)
{
    // self configure to run in numa_id NUMA node
    NUMAConfig config;
    config.set_task_numa_node(numa_id);

    {
        FileDualQueueMemoryComponent* component;

        // create a new queue component; the component is responsible with intializing the queue and the queue elements
        component = new FileDualQueueMemoryComponent(queue_size, block_size);

        // add the queue component to the manager identified by the current numa_id
        manager->addMemoryComponent(MemoryComponentType::DUALQUEUE, numa_id, component);
    }
}

void read_filenames(char *file_path, vector<char*> &filenames)
{
    FILE *fp;
    char *buffer, *rc;

    fp = fopen(file_path, "r");
    if (fp == NULL) {
        cout << "ERR: cannot open file " << file_path << endl;
        return;
    }

    while (true) {
        buffer = new char[4096];
        
        rc = fgets(buffer, 4096, fp);
        if (rc == NULL) {
            delete buffer;
            break;
        }
        if (buffer[strlen(buffer) - 1] == '\n') {
            buffer[strlen(buffer) - 1] = '\0';
        }

        filenames.push_back(buffer);
    }

    fclose(fp);
}

tuple<long, int, int> benchmark(char *file_path,
                                int num_readers,
                                int num_tokenizers,
                                int block_size,
                                fine_grained_times* times)
{
    // the manager is used to store and provide access to NUMA sensitive components (i.e. the queues)
    MemoryComponentManager* manager;
    FileDualQueueMemoryComponent* component;
    DualQueue<FileDataBlock*> *queue;
    FileDataBlock *finalBlock;
    
    // list of names of input files that will be read by the program
    vector<char*> filenames;
    char* filename;

    // list of threads that initialize the NUMA sensitive components
    vector<thread> threads_init;

    // list of threads that will read the contents of the input files
    vector<thread> threads_read;

    // list of threads that will tokenize the contents of the input files
    vector<thread> threads_tok;

    // atomic counter used by the reader threads to go over the input files
    atomic<int> file_counter(0);

    // atomic int storing the total number of blocks read
    atomic<long> total_num_tokens(0);
    
    int num_numa_nodes;
    int queue_size;
    NUMAConfig config;

    long rc = -1;
    
    // find out how many NUMA nodes exist in the system
    num_numa_nodes = config.get_num_numa_nodes();
    num_numa_nodes = (num_readers < num_numa_nodes) ? num_readers : num_numa_nodes;
    num_numa_nodes = (num_tokenizers < num_numa_nodes) ? num_tokenizers : num_numa_nodes;

    // read the input file names from the file passed as argument to the program
    read_filenames(file_path, filenames);
    for (int i  = 0; i < (int) filenames.size(); i++) {
        times->rd_pop_times.push_back(nullptr);
        times->rd_read_times.push_back(nullptr);
        times->rd_push_times.push_back(nullptr);
        times->rd_num_times.push_back(0);
        times->tok_pop_times.push_back(nullptr);
        times->tok_tok_times.push_back(nullptr);
        times->tok_push_times.push_back(nullptr);
        times->tok_num_times.push_back(0);
    }

    // create each queue in a specific NUMA node and add the queues to the manager
    manager = new MemoryComponentManager();
    queue_size = QUEUE_SIZE_RATIO * num_tokenizers / num_numa_nodes;
    
    for (int i = 0; i < num_numa_nodes; i++) {
        threads_init.push_back(thread(work_init, manager, i, queue_size, block_size));
    }

    for (int i = 0; i < num_numa_nodes; i++) {
        threads_init[i].join();
    }

    // check to see if the queues were allocated successfully
    for (int i = 0; i < num_numa_nodes; i++) {
        component = (FileDualQueueMemoryComponent*) manager->getMemoryComponent(MemoryComponentType::DUALQUEUE, i);
        if (!component->isAllocated()) {
            cout << "ERR: Could not allocate a buffer for the a queue!" << endl;
            
            // free all NUMA sensitive memory
            delete manager;

            // free the buffers storing the input file names
            while (filenames.size() > 0) {
                filename = filenames.back();
                filenames.pop_back();
                delete[] filename;
            }

            // exit the benchmark on error
            return make_tuple(rc, queue_size, num_numa_nodes);
        }
    }

    // start running the benchmark by creating the reader threads
    for (int i = 0; i < num_readers; i++) {
        threads_read.push_back(thread(work_read,
                                      manager,
                                      &file_counter,
                                      times,
                                      &filenames,
                                      i % num_numa_nodes,
                                      block_size));
    }

    for (int i = 0; i < num_tokenizers; i++) {
        threads_tok.push_back(thread(work_tok,
                                     manager,
                                     &total_num_tokens,
                                     times,
                                     i % num_numa_nodes,
                                     block_size));
    }
    
    for (int i = 0; i < num_readers; i++) {
        threads_read[i].join();
    }

    for (int i = 0; i < num_numa_nodes; i++) {
        for (int j = 0; j < num_tokenizers / num_numa_nodes + 1; j++) {
            component = (FileDualQueueMemoryComponent*) manager->getMemoryComponent(MemoryComponentType::DUALQUEUE, i);
            queue = component->getDualQueue();
            finalBlock = queue->pop_empty();
            finalBlock->length = -1;
            queue->push_full(finalBlock);
        }
    }

    for (int i = 0; i < num_tokenizers; i++) {
        threads_tok[i].join();
    }
    rc = total_num_tokens;

    // free all NUMA sensitive memory components
    delete manager;

    // free the buffers storing the input file names
    while (filenames.size() > 0) {
        filename = filenames.back();
        filenames.pop_back();
        delete[] filename;
    }

    return make_tuple(rc, queue_size, num_numa_nodes);
}

int main(int argc, char **argv)
{
    char source_file_path[4096];
    long total_size;
    int num_readers;
    int num_tokenizers;
    int block_size;
    int queue_size;
    int num_queues;

    tuple<long, int, int> benchmark_rc;
    fine_grained_times* times;
    double* elements;
    long rc;

    if (argc != 6) {
        cout << MSG << endl;
        return 1;
    }
    
    strcpy(source_file_path, argv[1]);
    total_size = atol(argv[2]);
    num_readers = atoi(argv[3]);
    num_tokenizers = atoi(argv[4]);
    block_size = atoi(argv[5]);

    times = new fine_grained_times{};
    benchmark_rc = benchmark(source_file_path, num_readers, num_tokenizers, block_size, times);
    rc = get<0>(benchmark_rc);
    queue_size = get<1>(benchmark_rc);
    num_queues = get<2>(benchmark_rc);

    cout << "Finished tokenizing " << total_size << " bytes of data:" << endl;
    cout << "- number of reader threads: " << num_readers << endl;
    cout << "- number of tokenizer threads: " << num_tokenizers << endl;
    cout << "- number of queues: " << num_queues << endl;
    cout << "- queue size: " << queue_size << endl;
    cout << "- block size: " << block_size << " bytes" << endl;
    cout << "- reader pop times (microseconds):";
    for (int i = 0; i < (int) times->rd_pop_times.size(); i++) {
        for (int j = 0; j < times->rd_num_times[i]; j++) {
            cout << " " << times->rd_pop_times[i][j];
        }
    }
    cout << endl;
    cout << "- reader read times (microseconds):";
    for (int i = 0; i < (int) times->rd_read_times.size(); i++) {
        for (int j = 0; j < times->rd_num_times[i]; j++) {
            cout << " " << times->rd_read_times[i][j];
        }
    }
    cout << endl;
    cout << "- reader push times (microseconds):";
    for (int i = 0; i < (int) times->rd_push_times.size(); i++) {
        for (int j = 0; j < times->rd_num_times[i]; j++) {
            cout << " " << times->rd_push_times[i][j];
        }
    }
    cout << endl;
    cout << "- tokenizer pop times (microseconds):";
    for (int i = 0; i < (int) times->tok_pop_times.size(); i++) {
        for (int j = 0; j < times->tok_num_times[i]; j++) {
            cout << " " << times->tok_pop_times[i][j];
        }
    }
    cout << endl;
    cout << "- tokenizer tokenize times (microseconds):";
    for (int i = 0; i < (int) times->tok_tok_times.size(); i++) {
        for (int j = 0; j < times->tok_num_times[i]; j++) {
            cout << " " << times->tok_tok_times[i][j];
        }
    }
    cout << endl;
    cout << "- tokenizer push times (microseconds):";
    for (int i = 0; i < (int) times->tok_push_times.size(); i++) {
        for (int j = 0; j < times->tok_num_times[i]; j++) {
            cout << " " << times->tok_push_times[i][j];
        }
    }
    cout << endl;
    cout << "Return code: " << rc << endl;

    for (int i = 0; i < (int) times->rd_num_times.size(); i++) {
        elements = times->rd_pop_times.back();
        times->rd_pop_times.pop_back();
        delete[] elements;
        elements = times->rd_read_times.back();
        times->rd_read_times.pop_back();
        delete[] elements;
        elements = times->rd_push_times.back();
        times->rd_push_times.pop_back();
        delete[] elements;
    }
    for (int i = 0; i < (int) times->tok_num_times.size(); i++) {
        elements = times->tok_pop_times.back();
        times->tok_pop_times.pop_back();
        delete[] elements;
        elements = times->tok_tok_times.back();
        times->tok_tok_times.pop_back();
        delete[] elements;
        elements = times->tok_push_times.back();
        times->tok_push_times.pop_back();
        delete[] elements;
    }
    delete times;

    return 0;
}