#include <iostream>
#include <cstring>
#include <thread>
#include <atomic>
#include <cstdio>

extern "C"
{
    #include <sys/time.h>
}

#include "ouroboros.hpp"
#include <vector>
#include <tuple>

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./analyze_stringhashing <total size in bytes> <term size in bytes> <number of buckets>"

#define NUM_THREADS 4

struct djb2_hash_t {
    size_t operator()(char* k) const
    {
        return djb2_hash((const unsigned char*) k);
    }
};

void work_hash(long num_terms,
               vector<char*> *terms,
               vector<size_t> *hashes,
               djb2_hash_t *termHash,
               int thread_id,
               long num_buckets)
{
    long N = num_terms / NUM_THREADS;

    for (long i = thread_id * N; i < (thread_id + 1) * N; i++) {
        (*hashes)[i] = (*termHash)(terms->at(i)) % num_buckets;
    }
}

int benchmark(long num_terms, vector<char*> *terms, vector<size_t> *hashes, long num_buckets)
{
    vector<djb2_hash_t*> termHashes;
    vector<thread*> threads;
    int rc = 0;
    
    for (auto i = 0; i < NUM_THREADS; i++) {
        termHashes.push_back(new djb2_hash_t());
        threads.push_back(new thread(work_hash, num_terms, terms, hashes, termHashes[i], i, num_buckets));
    }

    for (auto i = 0; i < NUM_THREADS; i++) {
        threads[i]->join();
    }

    for (auto i = 0; i < NUM_THREADS; i++) {
        delete termHashes[i];
    }

    return rc;
}

tuple<long, double, long> analyze(vector<size_t> *hashes, long num_terms, long num_buckets)
{
    long num_collisions(0);
    long avg_collision_size(0);
    long max_collision_size(0);
    long collisions;
    vector<long> *hash_vect;

    hash_vect = new vector<long>();

    for (long i = 0; i < num_buckets; i++) {
        hash_vect->push_back(0);
    }

    for (long i = 0; i < num_terms; i++) {
        (*hash_vect)[(*hashes)[i]] += 1;
    }

    for (long i = 0; i < num_buckets; i++) {
        collisions = (*hash_vect)[i] - 1;
        if (collisions > 0) {
            num_collisions++;
            avg_collision_size += collisions;
            max_collision_size = (collisions > max_collision_size) ? collisions : max_collision_size;
        }
    }

    delete hash_vect;

    if (num_collisions > 0) {
        return make_tuple(num_collisions, (double) avg_collision_size / (double) num_collisions, max_collision_size);
    } else {
        return make_tuple(num_collisions, 0.0, max_collision_size);
    }
}

int main(int argc, char **argv)
{
    long total_size;
    int term_size;
    long num_buckets;
    long num_terms;
    
    char *buffer;
    vector<char*> *terms;
    vector<size_t> *hashes;
    tuple<std::vector<char*>*, char*, vector<size_t>*> data;

    long num_collisions;
    double avg_collision_size;
    long max_collision_size;
    tuple<long, double, long> analysis;

    struct timeval start, end;
    double exec_time;
    int rc = 0;

    if (argc != 4) {
        cout << MSG << endl;
        return 1;
    }

    total_size = atol(argv[1]);
    term_size = atoi(argv[2]);
    num_buckets = atol(argv[3]);
    num_terms = total_size / (long) term_size;

    data = generate_synthdatahash(num_terms, term_size - 1);
    buffer = get<1>(data);
    terms = get<0>(data);
    hashes = get<2>(data);
    
    gettimeofday(&start, NULL);
    rc = benchmark(num_terms, terms, hashes, num_buckets);
    gettimeofday(&end, NULL);
    exec_time = (double) (end.tv_sec - start.tv_sec) + (double) (end.tv_usec - start.tv_usec) / 1000000;

    delete terms;
    delete[] buffer;

    analysis = analyze(hashes, num_terms, num_buckets);
    num_collisions = get<0>(analysis);
    avg_collision_size = get<1>(analysis);
    max_collision_size = get<2>(analysis);

    delete hashes;

    cout << "Finished hashing " << total_size << " bytes of data with " << NUM_THREADS \
        << " thread(s) and a term size of " << term_size << " bytes!" << endl;
    cout << "Total execution time: " << exec_time << " seconds" << endl;
    cout << "Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MB/sec" << endl;
    cout << "Throughput: " << ((double) num_terms / 1000) / exec_time << " kOPS" << endl;
    cout << "Number of collisions: " << num_collisions << endl;
    cout << "Average number of collisions per bucket: " << avg_collision_size << endl;
    cout << "Maximum number of collisions per bucket: " << max_collision_size << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}