#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>
#include <random>
#include <functional>

#include "ouroboros.hpp"
#include <vector>
#include <tuple>

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_reduce_ns <total number of words(int)> \
<block size in number of words(int)> <number of threads> <number of walks>"

struct worker_data
{
    long* val_buffer;
    long* bkt_buffer;
    long total_num_words;
    long thread_num_words;
    int num_walks;
    int num_threads;
};

void worker_work(worker_data& data, int thread_id)
{
    NUMAConfig config;
    config.set_task_numa_node(thread_id);

    {
        long* val_buffer = data.val_buffer;
        long* bkt_buffer;
        long total_num_words = data.total_num_words;
        long thread_num_words = data.thread_num_words;
        int num_walks = data.num_walks;
        int num_threads = data.num_threads;

        bkt_buffer = new long[thread_num_words];

        for (int k = 0; k < num_walks; k++) {
            long j = 0;
            for (long i = 0; i < total_num_words; i++) {
                long val = val_buffer[i];
                if (val % num_threads == thread_id) {
                    bkt_buffer[j] = val;
                    j = (j + 1) % thread_num_words;
                }
            }
        }

        data.bkt_buffer = bkt_buffer;
    }
}

void shuffle_buffer(long* buffer, long num_words, unsigned long seed)
{
    mt19937_64 random_number_generator(seed);
    uniform_int_distribution<long> distribution(0, num_words);

    for (long i = 0; i < num_words; i++) {
        long j = distribution(random_number_generator);
        exchange(buffer[i], buffer[j]);
    }
}

tuple<double, int, unsigned long> benchmark(long total_num_words, long block_num_words, int num_threads, int num_walks)
{
    vector<thread> init_threads;
    vector<thread> work_threads;
    vector<worker_data> data_threads;
    long* val_buffer;
    long thread_num_words = total_num_words / num_threads; 
    unsigned long seed = chrono::system_clock::now().time_since_epoch().count();

    chrono::time_point<chrono::high_resolution_clock> start, end;
    chrono::duration<double> diff;
    double exec_time;
    int rc = 0;
    
    val_buffer = new long[total_num_words];
    for (long i = 0; i < total_num_words; i++) {
        val_buffer[i] = i;
    }

    for (int i = 0; i < num_threads; i++) {
        init_threads.push_back(thread(shuffle_buffer, &val_buffer[i * thread_num_words], thread_num_words, seed));
    }

    for (int i = 0; i < num_threads; i++) {
        init_threads[i].join();
    }

    for (int i = 0; i < num_threads; i++) {
        worker_data data = worker_data{};
        data.val_buffer = val_buffer;
        data.bkt_buffer = nullptr;
        data.total_num_words = total_num_words;
        data.thread_num_words = block_num_words;
        data.num_walks = num_walks;
        data.num_threads = num_threads;
        data_threads.push_back(data);
    }

    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_threads; i++) {
        work_threads.push_back(thread(worker_work, ref(data_threads[i]), i));
    }

    for (int i = 0; i < num_threads; i++) {
        work_threads[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    exec_time = diff.count();

    for (auto i = 0; i < num_threads; i++) {
        delete[] data_threads[i].bkt_buffer;
    }

    delete[] val_buffer;

    return make_tuple(exec_time, rc, seed);
}

int main(int argc, char **argv)
{
    long total_num_words;
    long block_num_words;
    int num_threads;
    int num_walks;
    long total_bytes;
    
    unsigned long seed;
    double exec_time;
    int rc;
    tuple<double, int, unsigned long> benchmark_rc;

    if (argc != 5) {
        cout << MSG << endl;
        return 1;
    }

    total_num_words = atol(argv[1]);
    block_num_words = atol(argv[2]);
    num_threads = atoi(argv[3]);
    num_walks = atoi(argv[4]);
    total_bytes = total_num_words * sizeof(long);

    benchmark_rc = benchmark(total_num_words, block_num_words, num_threads, num_walks);
    exec_time = get<0>(benchmark_rc);
    rc = get<1>(benchmark_rc);
    seed = get<2>(benchmark_rc);

    cout << "Finished memorizing!" << endl;
    cout << "- Total number of bytes: " << (total_bytes) << endl;
    cout << "- Number of walks: " << num_walks  << endl;
    cout << "- Number of threads: " << num_threads  << endl;
    cout << "- Block size (in bytes): " << (block_num_words * sizeof(long)) << endl;
    cout << "- Total execution time: " << exec_time << " seconds" << endl;
    cout << "- Throughput: " << ((num_walks * total_bytes) / (1024 * 1024)) / exec_time << " MiB/sec" << endl;
    cout << "- Throughput: " << ((num_walks * total_num_words) / (1000 * 1000)) / exec_time << " mOPS" << endl;
    cout << "- Seed: " << seed << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}