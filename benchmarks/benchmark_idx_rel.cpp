#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>
#include <memory>

#include "ouroboros.hpp"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_idx_rel <source file> <total size in bytes> <number of NUMA nodes> \
<number of index threads> <block size in bytes> <page size in bytes> <index implementation>"

#define BLOCK_ADDON_SIZE 4096
#define DELIMITERS " \t\n"

void work_index(MemoryComponentManager* manager,
                atomic<long>* total_num_tokens,
                unsigned int numa_id,
                unsigned int index_id)
{
    NUMAConfig config;
    config.set_numa_node(numa_id);

    {
        TermFileIndexMemoryComponent* componentIndex;
        shared_ptr<BaseTermFileIndex> index;

        TokenStoreMemoryComponent* componentStore;
        deque<char*>* allTokens;

        // get the paged string store component identified by worker_id from the manager
        componentIndex = (TermFileIndexMemoryComponent*) 
                         manager->getMemoryComponent(MemoryComponentType::TERM_INDEX, index_id);

        // get the store from the store component
        index = componentIndex->getTermFileIndex();

        // get the token store component identified by worker_id from the manager
        componentStore = (TokenStoreMemoryComponent*) 
                         manager->getMemoryComponent(MemoryComponentType::TOKEN_STORE, index_id);

        // get the tokens and store from the store component
        allTokens = componentStore->getTokens();

        for (size_t i; i < allTokens->size(); i++) {
            index->insert(allTokens->at(i), index_id);
        }

        *total_num_tokens += index->getNumTerms();
    }
}

void work_read_tok_store(MemoryComponentManager* manager,
                         atomic<int>* file_counter,
                         vector<char*>* filenames,
                         unsigned int numa_id,
                         unsigned int index_id,
                         int block_size)
{
    // self configure to run in numa_id NUMA node
    NUMAConfig config;
    config.set_numa_node(numa_id);

    {
        WaveFileReaderDriver *reader;
        FileDataBlock *dataBlock;

        FileDualQueueMemoryComponent* componentQueue;
        DualQueue<FileDataBlock*> *queue;
        
        TokenStoreMemoryComponent* componentStore;
        BasePagedStringStore* store;
        deque<char*>* allTokens;
        
        BranchLessTokenizer *tokenizer;
        CTokBLock *tokBlock;
        char *buffer;
        char **tokens;
        
        char delims[32] = DELIMITERS;
        int length;
        int i;

        // allocate the buffers, the list of tokens for the tokenizer data block and create the 
        buffer = new char[(block_size + BLOCK_ADDON_SIZE) + 1];
        tokens = new char*[(block_size + BLOCK_ADDON_SIZE) / 2 + 1];
        tokBlock = new CTokBLock();
        tokBlock->buffer = buffer;
        tokBlock->tokens = tokens;
        tokenizer = new BranchLessTokenizer(delims);

        // create a dual queue memory component with one element;
        // the element will be used while reading, but not the queue;
        componentQueue = new FileDualQueueMemoryComponent(1, block_size + BLOCK_ADDON_SIZE);
        queue = componentQueue->getDualQueue();
        dataBlock = queue->pop_empty();

        // get the token store component identified by worker_id from the manager
        componentStore = (TokenStoreMemoryComponent*) 
                         manager->getMemoryComponent(MemoryComponentType::TOKEN_STORE, index_id);

        // get the tokens and store from the store component
        allTokens = componentStore->getTokens();
        store = componentStore->getPagedStringStore();

        // load balancing is performed through an atomic counter
        i = file_counter->fetch_add(1, std::memory_order_seq_cst);
        while (i < (int) filenames->size()) {
            // create a new reader driver for the current file
            reader = new WaveFileReaderDriver((*filenames)[i], block_size, BLOCK_ADDON_SIZE, delims);
            
            // try to open the file in the reader driver; if it errors out print message and skip the file
            try {
                reader->open();
            } catch (exception &e) {
                cout << "ERR: could not open file " << (*filenames)[i] << endl;
                delete reader;
                i = file_counter->fetch_add(1, std::memory_order_seq_cst);
                continue;
            }

            while (true) {
                // read a block of data from the file into data block buffer
                reader->readNextBlock(dataBlock);
                dataBlock->fileIdx = i;
                length = dataBlock->length;

                if (length > 0) {
                    tokenizer->getTokens(dataBlock, tokBlock);

                    for (long j = 0; j < tokBlock->numTokens; j++) {
                        allTokens->push_back(store->store(tokBlock->tokens[j]));
                    }
                }

                // if the reader driver reached the end of the file break from the while loop and read next file
                if (length == 0) {
                    break;
                }
            }

            // close the reader and free memory
            reader->close();
            delete reader;
            
            // load balancing is performed through an atomic counter
            i = file_counter->fetch_add(1, std::memory_order_seq_cst);
        }

        queue->push_empty(dataBlock);
        delete componentQueue;
        delete tokenizer;
        delete tokBlock;
        delete[] tokens;
        delete[] buffer;
    }
}

void work_init_indexes(MemoryComponentManager* manager,
                       unsigned int index_id,
                       unsigned int numa_id,
                       long page_size,
                       long initial_capacity,
                       TermFileIndexMemoryComponentType store_type)
{
    // self configure to run in numa_id NUMA node
    NUMAConfig config;
    config.set_numa_node(numa_id);

    {
        TermFileIndexMemoryComponent* component;
        unsigned long numBuckets;

        // using prime number for the initial size of the chained hashtable (the other hashmaps ignore this value)
        numBuckets = get_next_prime_number(initial_capacity / 65);

        // create a new page string store component; the component is responsible with storing the terms sequentially
        component = new TermFileIndexMemoryComponent(page_size, store_type, numBuckets);

        // add the string store component to the manager identified by the current worker_id
        manager->addMemoryComponent(MemoryComponentType::TERM_INDEX, index_id, component);
    }
}

void work_init_token_stores(MemoryComponentManager* manager,
                            unsigned int index_id,
                            unsigned int numa_id,
                            long page_size)
{
    // self configure to run in numa_id NUMA node
    NUMAConfig config;
    config.set_numa_node(numa_id);

    {
        TokenStoreMemoryComponent* component;

        // create a new token store component;
        // the component is responsible with storing the tokens sequentially in memory
        component = new TokenStoreMemoryComponent(page_size);

        // add the token store component to the manager identified by the current index_id
        manager->addMemoryComponent(MemoryComponentType::TOKEN_STORE, index_id, component);
    }
}

void read_filenames(char *file_path, vector<vector<char*>>& filenames, int num_numa_nodes)
{
    FILE *fp;
    char *buffer, *rc;
    int i;

    fp = fopen(file_path, "r");
    if (fp == NULL) {
        cout << "ERR: cannot open file " << file_path << endl;
        return;
    }

    i = 0;
    while (true) {
        buffer = new char[4096];
        
        rc = fgets(buffer, 4096, fp);
        if (rc == NULL) {
            delete buffer;
            break;
        }
        if (buffer[strlen(buffer) - 1] == '\n') {
            buffer[strlen(buffer) - 1] = '\0';
        }

        filenames[i % num_numa_nodes].push_back(buffer);
        i++;
    }

    fclose(fp);
}

tuple<double, double, double, int, int, long> benchmark(char *file_path,
                                                        int num_numa_nodes,
                                                        int num_indexers,
                                                        int block_size,
                                                        long page_size,
                                                        long total_size,
                                                        TermFileIndexMemoryComponentType store_type)
{
    // the manager is used to store and provide access to NUMA sensitive components (i.e. the queues)
    MemoryComponentManager* manager;
    
    // list of names of input files that will be read by the program
    vector<vector<char*>> filenames;
    char* filename;

    // list of threads that initialize the NUMA sensitive components
    vector<thread> threads_init;

    // list of threads that will read the contents of the input files
    vector<thread> threads_read;

    // list of threads that will tokenize the contents of the input files
    vector<thread> threads_tok;

    // atomic counter used by the reader threads to go over the input files
    vector<atomic<int>*> file_counters;

    // atomic int storing the total number of blocks read
    atomic<long> total_num_tokens(0);
    
    int current_num_numa_nodes;
    NUMAConfig config;

    chrono::time_point<chrono::high_resolution_clock> start, end;
    chrono::duration<double> diff;
    double exec_time = 0.0;
    double read_time = 0.0;
    double init_time = 0.0;
    long rc = -1;
    
    // find out how many NUMA nodes exist in the system
    current_num_numa_nodes = config.get_num_numa_nodes();
    current_num_numa_nodes = (current_num_numa_nodes < num_numa_nodes) ? current_num_numa_nodes : num_numa_nodes;
    current_num_numa_nodes = (num_indexers < current_num_numa_nodes) ? num_indexers : current_num_numa_nodes;

    // read the input file names from the file passed as argument to the program
    for (int i = 0; i < current_num_numa_nodes; i++) {
        filenames.push_back(vector<char*>());
        file_counters.push_back(new atomic<int>(0));
    }
    read_filenames(file_path, filenames, current_num_numa_nodes);

    // create each queue in a specific NUMA node and add the queues to the manager
    manager = new MemoryComponentManager();
    
    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_indexers; i++) {
        threads_init.push_back(thread(work_init_token_stores, 
                                      manager, 
                                      i, 
                                      i % current_num_numa_nodes, 
                                      page_size));
    }

    for (int i = 0; i < num_indexers; i++) {
        threads_init.push_back(thread(work_init_indexes,
                                      manager,
                                      i,
                                      i % current_num_numa_nodes,
                                      page_size,
                                      total_size / num_indexers,
                                      store_type));
    }

    for (int i = 0; i < num_indexers + num_indexers; i++) {
        threads_init[i].join();
    }

    end = chrono::high_resolution_clock::now();
    diff = end - start;
    init_time = diff.count();

    // start running the benchmark by creating the reader threads
    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_indexers; i++) {
        threads_read.push_back(thread(work_read_tok_store,
                                      manager,
                                      file_counters[i % current_num_numa_nodes],
                                      &filenames[i % current_num_numa_nodes],
                                      i % current_num_numa_nodes,
                                      i,
                                      block_size));
    }

    for (int i = 0; i < num_indexers; i++) {
        threads_read[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    read_time = diff.count();
    rc = total_num_tokens;

    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_indexers; i++) {
        threads_tok.push_back(thread(work_index,
                                     manager,
                                     &total_num_tokens,
                                     i % current_num_numa_nodes,
                                     i));
    }

    for (int i = 0; i < num_indexers; i++) {
        threads_tok[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    exec_time = diff.count();
    rc = total_num_tokens;

    // free all NUMA sensitive memory components
    delete manager;

    // free the buffers storing the input file names
    for (int i = 0; i < current_num_numa_nodes; i++) {
        while (filenames[i].size() > 0) {
            filename = filenames[i].back();
            filenames[i].pop_back();
            delete[] filename;
        }
        delete file_counters[i];
    }

    return make_tuple(exec_time, init_time, read_time, current_num_numa_nodes, num_indexers, rc);
}

int main(int argc, char **argv)
{
    char file_path[4096];
    long total_size;
    int num_numa_nodes;
    int num_indexers;
    int queue_size;
    int num_queues;
    int block_size;
    long page_size;
    char implementation[16];
    TermFileIndexMemoryComponentType store_type;

    tuple<double, double, double, int, int, long> benchmark_rc;
    double exec_time, init_time, read_time;
    long rc;

    if (argc != 8) {
        cout << MSG << endl;
        return 1;
    }

    strcpy(file_path, argv[1]);
    total_size = atol(argv[2]);
    num_numa_nodes = atoi(argv[3]);
    num_indexers = atoi(argv[4]);
    block_size = atoi(argv[5]);
    page_size = atol(argv[6]);
    strcpy(implementation, argv[7]);

    if (strcmp(implementation, "std") == 0) {
        store_type = TermFileIndexMemoryComponentType::STD;
    } else if (strcmp(implementation, "dense") == 0) {
        store_type = TermFileIndexMemoryComponentType::DENSE;
    } else if (strcmp(implementation, "swiss") == 0) {
        store_type = TermFileIndexMemoryComponentType::SWISS;
    } else if (strcmp(implementation, "chained") == 0) {
        store_type = TermFileIndexMemoryComponentType::CHAINED;
    } else if (strcmp(implementation, "pstd") == 0) {
        store_type = TermFileIndexMemoryComponentType::PSTD;
    } else if (strcmp(implementation, "pdense") == 0) {
        store_type = TermFileIndexMemoryComponentType::PDENSE;
    } else if (strcmp(implementation, "pswiss") == 0) {
        store_type = TermFileIndexMemoryComponentType::PSWISS;
    } else if (strcmp(implementation, "pchained") == 0) {
        store_type = TermFileIndexMemoryComponentType::PCHAINED;
    } else {
        cout << "ERR: unrecognized term index implementation!" << endl;
        return 2;
    }

    benchmark_rc = benchmark(file_path, num_numa_nodes, num_indexers, block_size, page_size, total_size, store_type);
    exec_time = get<0>(benchmark_rc);
    init_time = get<1>(benchmark_rc);
    read_time = get<2>(benchmark_rc);
    num_numa_nodes = get<3>(benchmark_rc);
    num_indexers = get<4>(benchmark_rc);
    rc = get<5>(benchmark_rc);

    cout << "Finished indexing and storing from memory " << total_size << " bytes of data:" << endl;
    cout << "- number of indexing threads: " << num_indexers << endl;
    cout << "- number of NUMA nodes: " << num_numa_nodes << endl;
    cout << "- block size: " << block_size << " bytes" << endl;
    cout << "- page size: " << page_size << " bytes" << endl;
    cout << "- total initialization time: " << init_time << " seconds" << endl;
    cout << "- total read time: " << read_time << " seconds" << endl;
    cout << "- total execution time: " << exec_time << " seconds" << endl;
    cout << "- throughput: " << (total_size / (1024 * 1024)) / exec_time << " MiB/sec" << endl;
    cout << "- throughput: " << (rc / exec_time) << " tokens/sec" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}