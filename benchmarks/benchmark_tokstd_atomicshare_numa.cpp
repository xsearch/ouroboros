#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>
#include <atomic>
#include <chrono>

#include "ouroboros.hpp"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_tokstd_atomicshare_numa <source file> <mapping file> <total size in bytes> \
<number of tokenizer threads> <number of reader threads> <block size in bytes>"

#define QUEUE_SIZE_RATIO 4
#define DELIMS " \t\n/_.,;:"

void work_tok(MemoryComponentManager *manager,
              atomic<long>* numTokens,
              int block_size,
              unsigned int cpu_id,
              unsigned int numa_id)
{
    NUMAConfig config;
    config.set_task_numa_node(numa_id);

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(cpu_id, &cpuset);
    pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);

    {
        auto queue = ((FileDualQueueMemoryComponent*) manager->getMemoryComponent(MemoryComponentType::DUALQUEUE, numa_id))->getDualQueue();

        FileDataBlock *dataBlock;
        StandardTokenizer *tokenizer;
        CTokBLock *tokBlock;
        char *buffer;
        char **tokens;
        char delims[32] = DELIMS;
        long toks(0);
        int length;

        buffer = new char[block_size + 1];
        tokens = new char*[block_size / 2 + 1];
        tokBlock = new CTokBLock();
        tokBlock->buffer = buffer;
        tokBlock->tokens = tokens;
        tokenizer = new StandardTokenizer(delims);

        while (true) {
            dataBlock = queue->pop_full();
            
            length = dataBlock->length;
            if (length > 0) {
                tokenizer->getTokens(dataBlock, tokBlock);
                toks += tokBlock->numTokens;
            }
            
            queue->push_empty(dataBlock);
            
            if (length == -1) {
                break;
            }
        }

        *numTokens += toks;
    }
}

void work_init(MemoryComponentManager *manager, int numa_id, int queue_size, int block_size) {
    NUMAConfig config;
    config.set_task_numa_node(numa_id);

    {
        FileDualQueueMemoryComponent *dualQueueComponent = new FileDualQueueMemoryComponent(queue_size, block_size);
        manager->addMemoryComponent(MemoryComponentType::DUALQUEUE, numa_id, dualQueueComponent);
    }
}

void work_preread(atomic<int>* position,
               vector<char*>* filenames,
               unsigned int rd_id,
               int block_size,
               char* buffer_ptr,
               long total_size,
               atomic<char*>* buffer_iterator)
{
    PFileReaderDriver *reader;
    int pos;
    char *thread_buffer;

    pos = position->fetch_add(1, std::memory_order_seq_cst);
    thread_buffer = buffer_iterator->fetch_add(block_size, std::memory_order_seq_cst);

    char local_buf[block_size];
    FileDataBlock componentDataBlock;
    componentDataBlock.buffer = local_buf;

    while (pos < (int) filenames->size() && thread_buffer < buffer_ptr + total_size) {
        reader = new PFileReaderDriver((*filenames)[pos], block_size);
        try {
            reader->open();
        } catch (exception &e) {
            cout << "ERR: could not open file " << (*filenames)[pos] << endl;
            delete reader;
            pos = position->fetch_add(1, std::memory_order_seq_cst);
            continue;    
        }

        while (thread_buffer < buffer_ptr + total_size) {
            reader->readNextBlock(&componentDataBlock);

            if (componentDataBlock.length == 0) {
                break;
            }

            memcpy(thread_buffer, componentDataBlock.buffer, block_size);
            thread_buffer = buffer_iterator->fetch_add(block_size, std::memory_order_seq_cst);
        }

        reader->close();
        delete reader;

        pos = position->fetch_add(1, std::memory_order_seq_cst);
    }
}

void work_read(MemoryComponentManager *manager,
               int block_size,
               char* buffer_ptr,
               long total_size,
               atomic<char*>* buffer_iterator,
               unsigned int cpu_id,
               unsigned int numa_id)
{
    NUMAConfig config;
    config.set_task_numa_node(numa_id);

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(cpu_id, &cpuset);
    pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);

    {
        auto queue = ((FileDualQueueMemoryComponent*) manager->getMemoryComponent(MemoryComponentType::DUALQUEUE, numa_id))->getDualQueue();

        char *thread_buffer;
        
        thread_buffer = buffer_iterator->fetch_add(block_size, std::memory_order_seq_cst);
        FileDataBlock *queueDataBlock;
        while (thread_buffer < buffer_ptr + total_size) {
            queueDataBlock = queue->pop_empty();

            memcpy(queueDataBlock->buffer, thread_buffer, block_size);
            queueDataBlock->blockSize = block_size;
            queueDataBlock->length = block_size;

            queue->push_full(queueDataBlock);
            thread_buffer = buffer_iterator->fetch_add(block_size, std::memory_order_seq_cst);
        }
    }
}

void read_filenames(char *file_path, vector<char*> &filenames)
{
    FILE *fp;
    char *buffer, *rc;

    fp = fopen(file_path, "r");
    if (fp == NULL) {
        cout << "ERR: cannot open file " << file_path << endl;
        return;
    }

    while (true) {
        buffer = new char[4096];
        
        rc = fgets(buffer, 4096, fp);
        if (rc == NULL) {
            delete buffer;
            break;
        }
        if (buffer[strlen(buffer) - 1] == '\n') {
            buffer[strlen(buffer) - 1] = '\0';
        }

        filenames.push_back(buffer);
    }

    fclose(fp);
}

tuple<vector<tuple<int,int>>*, vector<tuple<int,int>>*> read_numafile(char *file_path)
{
    std::ifstream numa_stream(file_path);
    std::stringstream numa_strstream;
    numa_strstream << numa_stream.rdbuf();
    const std::string& contents = numa_strstream.str();

    int tokenizer_break = contents.find("\n", 0);
    int producer_break = contents.find("\n", tokenizer_break + 1);
    const std::string& tokenizer_cores = contents.substr(0, tokenizer_break);
    const std::string& producer_cores = contents.substr(tokenizer_break + 1, producer_break);

    auto extract_core_information = [] (vector<tuple<int,int>>& core_vec, const std::string& core_str) -> void {
        size_t prevpos = 0;
        size_t pos = 0;
        while (pos < core_str.length() && (pos = core_str.find(",", pos)) != std::string::npos) {
            std::string cpu_entry = core_str.substr(prevpos, pos);
            int bar_delimiter = cpu_entry.find("|");
            core_vec.push_back(
                make_tuple(
                    std::stoi(cpu_entry.substr(0, bar_delimiter)),
                    std::stoi(cpu_entry.substr(bar_delimiter + 1, cpu_entry.length()))
                )
            );
            pos++;
            prevpos = pos;
        }
    };

    vector<tuple<int,int>> *tokenizer_core_list = new vector<tuple<int,int>>();
    vector<tuple<int,int>> *producer_core_list = new vector<tuple<int,int>>();
    extract_core_information(*tokenizer_core_list, tokenizer_cores);
    extract_core_information(*producer_core_list, producer_cores);

    return make_tuple(tokenizer_core_list, producer_core_list);
}

tuple<double, long> benchmark(char *file_path, char *mapping_path, int num_tokenizers, int num_readers, int block_size, long total_size)
{
    vector<char*> filenames;
    char* filename;
    vector<thread> threads_init;
    vector<thread> threads_preread;
    vector<thread> threads_read;
    vector<thread> threads_tok;
    atomic<long> numTokens(0);
    atomic<int> position(0);

    chrono::time_point<chrono::high_resolution_clock> start, end;
    chrono::duration<double> diff;
    double exec_time = 0.0;
    long rc = -1;

    char *data_buffers;
    int mem_rc = posix_memalign((void**) &data_buffers, 512, total_size);
    if (mem_rc) {
        std::cout << "ERR: Could not allocate memory for buffers" << std::endl;
        return make_tuple(0.0, 0);
    }

    auto mapping_information = read_numafile(mapping_path);
    read_filenames(file_path, filenames);

    vector<tuple<int, int>>* tokenizer_pinnings = std::get<0>(mapping_information);
    size_t tokenizer_pinning_entry_count = tokenizer_pinnings->size();
    vector<tuple<int, int>>* producer_pinnings = std::get<1>(mapping_information);
    size_t producer_pinning_entry_count = producer_pinnings->size();
    
    std::unordered_map<int, int> numa_reader_map;
    for (int producer = 0; producer < num_readers; producer++) {
        int numa_id = std::get<1>((*producer_pinnings)[producer]);
        auto map_entry = numa_reader_map.find(numa_id);
        if (map_entry != numa_reader_map.end()) {
            map_entry->second = map_entry->second + 1;
        } else {
            numa_reader_map.insert(std::pair<int, int>(numa_id, 0));
        }
    }

    int num_numa_nodes = numa_reader_map.size();
    int queue_size = QUEUE_SIZE_RATIO * num_readers / num_numa_nodes;

    MemoryComponentManager *manager = new MemoryComponentManager();

    for (int i = 0; i < num_numa_nodes; i++) {
        threads_init.push_back(thread(work_init, manager, i, queue_size, block_size));
    }

    for (auto i = 0; i < num_numa_nodes; i++) {
        threads_init[i].join();
    }

    atomic<char*> data_buffers_ptr(&data_buffers[0]);
    for (auto i = 0; i < num_readers; i++) {
        threads_preread.push_back(thread(work_preread, &position, &filenames, i, block_size, &data_buffers[0], total_size, &data_buffers_ptr));
    }

    for (auto i = 0; i < num_readers; i++) {
        threads_preread[i].join();
    }

    // rewind ptr
    data_buffers_ptr = &data_buffers[0];

    start = chrono::high_resolution_clock::now();

    size_t reader_idx = 0;
    for (auto i = 0; i < num_readers; i++) {
        auto cpu_entry = (*producer_pinnings)[reader_idx];
        unsigned int cpu_id = std::get<0>(cpu_entry);
        unsigned int numa_id = std::get<1>(cpu_entry);
        threads_read.push_back(thread(work_read, manager, block_size, &data_buffers[0], total_size, &data_buffers_ptr, cpu_id, numa_id));
        if (++reader_idx >= producer_pinning_entry_count) reader_idx = 0;
    }

    size_t tokenizer_idx = 0;
    for (auto i = 0; i < num_tokenizers; i++) {
        auto cpu_entry = (*tokenizer_pinnings)[tokenizer_idx];
        unsigned int cpu_id = std::get<0>(cpu_entry);
        unsigned int numa_id = std::get<1>(cpu_entry);
        threads_tok.push_back(thread(work_tok, manager, &numTokens, block_size, cpu_id, numa_id));
        if (++tokenizer_idx >= tokenizer_pinning_entry_count) tokenizer_idx = 0;
    }

    for (auto i = 0; i < num_readers; i++) {
        threads_read[i].join();
    }

    // Insert termination blocks to each localized queue, for each tokenizer in the NUMA node
    FileDataBlock* finalBlock;
    for (int numa_node = 0; numa_node < num_numa_nodes; numa_node++) {
        auto queue = ((FileDualQueueMemoryComponent*) manager->getMemoryComponent(MemoryComponentType::DUALQUEUE, numa_node))->getDualQueue();
        for (auto i = 0; i < ((num_tokenizers / num_numa_nodes) + 1); i++) {
            finalBlock = queue->pop_empty();
            finalBlock->length = -1;
            queue->push_full(finalBlock);
        }
    }

    for (auto i = 0; i < num_tokenizers; i++) {
        threads_tok[i].join();
    }

    end = chrono::high_resolution_clock::now();
    diff = end - start;
    exec_time = diff.count();
    rc = numTokens;

    while (filenames.size() > 0) {
        filename = filenames.back();
        filenames.pop_back();
        delete[] filename;
    }

    return make_tuple(exec_time, rc);
}

int main(int argc, char **argv)
{
    char file_path[4096];
    char mapping_path[4096];
    long total_size;
    int num_tokenizers;
    int num_readers;
    int block_size;

    tuple<double, long> benchmark_rc;
    double exec_time;
    long rc;

    if (argc != 7) {
        cout << MSG << endl;
        return 1;
    }

    strcpy(file_path, argv[1]);
    strcpy(mapping_path, argv[2]);
    total_size = atol(argv[3]);
    num_tokenizers = atoi(argv[4]);
    num_readers = atoi(argv[5]);
    block_size = atoi(argv[6]);

    benchmark_rc = benchmark(file_path, mapping_path, num_tokenizers, num_readers, block_size, total_size);
    exec_time = get<0>(benchmark_rc);
    rc = get<1>(benchmark_rc);

    cout << "Finished tokenizing " << total_size << " bytes of data:" << endl;
    cout << "- Number of tokenizer threads: " << num_tokenizers << endl;
    cout << "- Number of reader threads: " << num_readers << endl;
    cout << "- Block size: " << block_size << " bytes" << endl;
    cout << "- Total execution time: " << exec_time << " seconds" << endl;
    cout << "- Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MiB/sec" << endl;
    cout << "- Throughput: " << rc / exec_time << " tokens/sec" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}
