#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>
#include <functional>

#include "ouroboros.hpp"
#include <vector>
#include <tuple>

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_rdtokidxcht_directtfidf_ns <source file> <total size in bytes> \
<number of index threads> <page size> <search term>"

#define DELIMS " \t\n/_.,;:"

void work_index(vector<char*>& files,
                atomic<unsigned int>& filePos,
                long fileIndexNumBuckets,
                long termsIndexNumBuckets,
                long page_size,
                NSCHTFileIndex*& fileIndex,
                NSCHTDirectTFIDFIndex*& termsIndex,
                int thread_id)
{
    NUMAConfig config;
    config.set_task_numa_node(thread_id);

    {
        FileReaderDriver *reader;
        FileDataBlock *dataBlock;
        BranchLessTokenizer *tokenizer;
        CTokBLock *tokBlock;
        char delims[32] = DELIMS;
        unsigned int pos;

        fileIndex = new NSCHTFileIndex(page_size, fileIndexNumBuckets);
        termsIndex = new NSCHTDirectTFIDFIndex(page_size, termsIndexNumBuckets);
        dataBlock = new FileDataBlock();
        dataBlock->buffer = new char[page_size];
        tokBlock = new CTokBLock();
        tokBlock->buffer = new char[page_size + 1];
        tokBlock->tokens = new char*[page_size / 2 + 1];
        tokenizer = new BranchLessTokenizer(delims);

        pos = filePos.fetch_add(1, std::memory_order_seq_cst);
        while (pos < files.size()) {
            reader = new FileReaderDriver(files.at(pos), page_size);
            try {
                reader->open();
            } catch (exception &e) {
                cout << "ERROR: could not open file " << files.at(pos) << endl;
                delete reader;
                pos = filePos.fetch_add(1, std::memory_order_seq_cst);
                continue;
            }

            dataBlock->blockSize = page_size;
            dataBlock->fileIdx = fileIndex->insert(files.at(pos));
            dataBlock->filepath = (char*) fileIndex->reverseLookup(dataBlock->fileIdx);

            while (true) {
                reader->readNextBlock(dataBlock);
                if (dataBlock->length == 0) {
                    break;
                }

                tokenizer->getTokens(dataBlock, tokBlock);
                for (long i = 0; i < tokBlock->numTokens; i++) {
                    termsIndex->insert(tokBlock->tokens[i], dataBlock->fileIdx);
                }
            }

            reader->close();
            delete reader;
            pos = filePos.fetch_add(1, std::memory_order_seq_cst);
        }

        delete[] dataBlock->buffer;
        delete dataBlock;
        delete[] tokBlock->buffer;
        delete[] tokBlock->tokens;
        delete tokBlock;
        delete tokenizer;
    }
}

void read_filenames(char *file_path, vector<char*> &filenames)
{
    FILE *fp;
    char *buffer, *rc;

    fp = fopen(file_path, "r");
    if (fp == NULL) {
        cout << "ERR: cannot open file " << file_path << endl;
        return;
    }

    while (true) {
        buffer = new char[4096];
        
        rc = fgets(buffer, 4096, fp);
        if (rc == NULL) {
            delete buffer;
            break;
        }
        if (buffer[strlen(buffer) - 1] == '\n') {
            buffer[strlen(buffer) - 1] = '\0';
        }

        filenames.push_back(buffer);
    }

    fclose(fp);
}

tuple<double, int> benchmark(char* source_file,
                             long total_bytes,
                             int num_index_threads,
                             long page_size,
                             char* search_term)
{
    vector<NSCHTFileIndex*> fileIndexes;
    vector<NSCHTDirectTFIDFIndex*> termsIndexes;
    vector<thread> threads;
    vector<char*> files;
    atomic<unsigned int> filePos(0);
    long fileIndexNumBuckets;
    long termsIndexNumBuckets;

    long searchTermIdx;
    std::tuple<char*, long, long> lookupResult;

    chrono::time_point<chrono::high_resolution_clock> start, end;
    chrono::duration<double> diff;
    double exec_time;
    int rc = 0;

    read_filenames(source_file, files);
    fileIndexNumBuckets = get_next_prime_number(files.size());
    termsIndexNumBuckets =  get_next_prime_number(((total_bytes / num_index_threads) / files.size()) / 8);
    
    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_index_threads; i++) {
        fileIndexes.push_back(nullptr);
        termsIndexes.push_back(nullptr);
    }

    for (int i = 0; i < num_index_threads; i++) {
        threads.push_back(thread(work_index,
                                 std::ref(files),
                                 std::ref(filePos),
                                 fileIndexNumBuckets,
                                 termsIndexNumBuckets,
                                 page_size,
                                 std::ref(fileIndexes[i]),
                                 std::ref(termsIndexes[i]),
                                 i));
    }

    for (int i = 0; i < num_index_threads; i++) {
        threads[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    exec_time = diff.count();
    
    for (int i = 0; i < num_index_threads; i++) {
        searchTermIdx = termsIndexes[i]->lookup(search_term);
        if (searchTermIdx != -1) {
            lookupResult = termsIndexes[i]->reverseLookup(searchTermIdx);
            rc++;
        }
    }

    for (int i = 0; i < num_index_threads; i++) {
        delete fileIndexes[i];
        delete termsIndexes[i];
    }
    
    for (unsigned int i = 0; i < files.size(); i++) {
        delete files[i];
    }

    return make_tuple(exec_time, rc);
}

int main(int argc, char **argv)
{
    char source_file[4096] = "";
    long total_bytes;
    int num_index_threads;
    long page_size;
    char search_term[256] = "";
    
    tuple<double, int> benchmark_rc;
    double exec_time;
    int rc;
    

    if (argc != 6) {
        cout << MSG << endl;
        return 1;
    }

    if (strlen(argv[1]) >= 4096 or strlen(argv[5]) >= 256) {
        cout << "FATAL: Arguments too long!" << endl;
        return 2;
    }

    strcpy(source_file, argv[1]);
    total_bytes = atol(argv[2]);
    num_index_threads = atoi(argv[3]);
    page_size = atol(argv[4]);
    strcpy(search_term, argv[5]);
    
    benchmark_rc = benchmark(source_file, total_bytes, num_index_threads, page_size, search_term);
    exec_time = get<0>(benchmark_rc);
    rc = get<1>(benchmark_rc);

    cout << "Finished indexing!" << endl;
    cout << "- Total number of bytes: " << total_bytes << endl;
    cout << "- Number of index threads: " << num_index_threads << endl;
    cout << "- Page size (in bytes): " << page_size << endl;
    cout << "- Total execution time: " << exec_time << " seconds" << endl;
    cout << "- Throughput: " << ((total_bytes / (1024 * 1024)) / exec_time) << " MiB/sec" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}