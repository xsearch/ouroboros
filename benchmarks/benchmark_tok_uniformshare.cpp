#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>

#include "ouroboros.hpp"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_tok_uniformshare <source file> <total size in bytes> \
<number of tokenizer threads> <number of reader threads> <block size in bytes>"

#define QUEUE_SIZE_RATIO 4
#define DELIMS " \t\n/_.,;:"

void work_tok(DualQueue<FileDataBlock*>* queue,
              atomic<long>* numTokens,
              unsigned int rd_id,
              unsigned int tok_id,
              int block_size)
{
    FileDataBlock *dataBlock;
    BranchLessTokenizer *tokenizer;
    CTokBLock *tokBlock;
    char *buffer;
    char **tokens;
    char delims[32] = DELIMS;
    long toks(0);
    int length;

    buffer = new char[block_size + 1];
    tokens = new char*[block_size / 2 + 1];
    tokBlock = new CTokBLock();
    tokBlock->buffer = buffer;
    tokBlock->tokens = tokens;
    tokenizer = new BranchLessTokenizer(delims);

    while (true) {
        dataBlock = queue->pop_full();
        
        length = dataBlock->length;
        if (length > 0) {
            tokenizer->getTokens(dataBlock, tokBlock);
            toks += tokBlock->numTokens;
        }
        
        queue->push_empty(dataBlock);
        
        if (length == -1) {
            break;
        }
    }

    *numTokens += toks;
}

void work_preread(atomic<int>* position,
               vector<char*>* filenames,
               unsigned int rd_id,
               int block_size,
               char* buffer_ptr,
               long total_size,
               atomic<char*>* buffer_iterator)
{
    PFileReaderDriver *reader;
    int pos;
    char *thread_buffer;

    pos = position->fetch_add(1, std::memory_order_seq_cst);
    thread_buffer = buffer_iterator->fetch_add(block_size, std::memory_order_seq_cst);

    char local_buf[block_size];
    FileDataBlock componentDataBlock;
    componentDataBlock.buffer = local_buf;

    while (pos < (int) filenames->size() && thread_buffer < buffer_ptr + total_size) {
        reader = new PFileReaderDriver((*filenames)[pos], block_size);
        try {
            reader->open();
        } catch (exception &e) {
            cout << "ERR: could not open file " << (*filenames)[pos] << endl;
            delete reader;
            pos = position->fetch_add(1, std::memory_order_seq_cst);
            continue;    
        }

        while (true) {
            thread_buffer = buffer_iterator->fetch_add(block_size, std::memory_order_seq_cst);
            reader->readNextBlock(&componentDataBlock);

            if (componentDataBlock.length == 0) break;

            memcpy(thread_buffer, componentDataBlock.buffer, block_size);
        }

        reader->close();
        delete reader;

        pos = position->fetch_add(1, std::memory_order_seq_cst);
    }
}

void work_read(DualQueue<FileDataBlock*> *queue,
               unsigned int rd_id,
               int block_size,
               char *base_buffer_ptr,
               int partition_count,
               long stride)
{
    FileDataBlock *queueDataBlock;
    for (int x = 0; x < partition_count; x++) {
        queueDataBlock = queue->pop_empty();

        memcpy(queueDataBlock->buffer, base_buffer_ptr, block_size);
        queueDataBlock->blockSize = block_size;
        queueDataBlock->length = block_size;

        queue->push_full(queueDataBlock);
        base_buffer_ptr += stride;
    }
}

void read_filenames(char *file_path, vector<char*> &filenames)
{
    FILE *fp;
    char *buffer, *rc;

    fp = fopen(file_path, "r");
    if (fp == NULL) {
        cout << "ERR: cannot open file " << file_path << endl;
        return;
    }

    while (true) {
        buffer = new char[4096];
        
        rc = fgets(buffer, 4096, fp);
        if (rc == NULL) {
            delete buffer;
            break;
        }
        if (buffer[strlen(buffer) - 1] == '\n') {
            buffer[strlen(buffer) - 1] = '\0';
        }

        filenames.push_back(buffer);
    }

    fclose(fp);
}

tuple<double, long> benchmark(char *file_path, int num_tokenizers, int num_readers, int block_size, long total_size)
{
    int queue_size = QUEUE_SIZE_RATIO * num_readers;
    DualQueue<FileDataBlock*> queue(queue_size);
    vector<char*> filenames;
    char* filename;
    vector<thread> threads_preread;
    vector<thread> threads_read;
    vector<thread> threads_tok;
    atomic<long> numTokens(0);
    atomic<int> position(0);

    chrono::time_point<chrono::high_resolution_clock> start, end;
    chrono::duration<double> diff;
    double exec_time = 0.0;
    long rc = -1;

    FileDataBlock *dataBlocks;
    char *buffers;
    dataBlocks = new FileDataBlock[queue_size]();
    buffers = new char[queue_size * (long) block_size];

    char *data_buffers;
    int mem_rc = posix_memalign((void**) &data_buffers, 512, total_size);
    if (mem_rc) {
        std::cout << "ERR: Could not allocate memory for buffers" << std::endl;
        return make_tuple(0.0, 0);
    }

    for (auto i = 0; i < queue_size; i++) {
        dataBlocks[i].buffer = &buffers[i * block_size];
        queue.push_empty(&dataBlocks[i]);
    }

    read_filenames(file_path, filenames);

    atomic<char*> data_buffers_ptr(&data_buffers[0]);

    int partitions_per_reader = (total_size / block_size) / num_readers;
    int leftover_partitions = (total_size / block_size) % num_readers;
    std::vector<int> partition_counts(num_readers, partitions_per_reader);

    for (int x = 0; x < leftover_partitions; x++) {
        partition_counts[x]++;
    }

    char *buffer_base = data_buffers;
    unsigned int stride = block_size * num_readers;

    for (auto i = 0; i < num_readers; i++) {
        threads_preread.push_back(thread(work_preread, &position, &filenames, i, block_size, &data_buffers[0], total_size, &data_buffers_ptr));
    }

    for (auto i = 0; i < num_readers; i++) {
        threads_preread[i].join();
    }

    // rewind ptr
    data_buffers_ptr = &data_buffers_ptr[0];

    start = chrono::high_resolution_clock::now();
    for (auto i = 0; i < num_readers; i++) {
        threads_read.push_back(thread(work_read, &queue, i, block_size, buffer_base + (i * block_size), partition_counts[i], stride));
    }

    for (auto i = 0; i < num_tokenizers; i++) {
        threads_tok.push_back(thread(work_tok, &queue, &numTokens, i % num_readers, i, block_size));
    }

    for (auto i = 0; i < num_readers; i++) {
        threads_read[i].join();
    }

    // Drop in some -1 blocks after readers are done
    FileDataBlock* finalBlock;
    for (auto i = 0; i < num_tokenizers; i++) {
        finalBlock = queue.pop_empty();
        finalBlock->length = -1;
        queue.push_full(finalBlock);
    }

    for (auto i = 0; i < num_tokenizers; i++) {
        threads_tok[i].join();
    }

    end = chrono::high_resolution_clock::now();
    diff = end - start;
    exec_time = diff.count();
    rc = numTokens;


    while (filenames.size() > 0) {
        filename = filenames.back();
        filenames.pop_back();
        delete[] filename;
    }

    return make_tuple(exec_time, rc);
}

int main(int argc, char **argv)
{
    char file_path[4096];
    long total_size;
    int num_tokenizers;
    int num_readers;
    int block_size;

    tuple<double, long> benchmark_rc;
    double exec_time;
    long rc;

    if (argc != 6) {
        cout << MSG << endl;
        return 1;
    }

    strcpy(file_path, argv[1]);
    total_size = atol(argv[2]);
    num_tokenizers = atoi(argv[3]);
    num_readers = atoi(argv[4]);
    block_size = atoi(argv[5]);

    benchmark_rc = benchmark(file_path, num_tokenizers, num_readers, block_size, total_size);
    exec_time = get<0>(benchmark_rc);
    rc = get<1>(benchmark_rc);

    cout << "Finished tokenizing " << total_size << " bytes of data:" << endl;
    cout << "- Number of tokenizer threads: " << num_tokenizers << endl;
    cout << "- Number of reader threads: " << num_readers << endl;
    cout << "- Block size: " << block_size << " bytes" << endl;
    cout << "- Total execution time: " << exec_time << " seconds" << endl;
    cout << "- Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MiB/sec" << endl;
    cout << "- Throughput: " << rc / exec_time << " tokens/sec" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}
