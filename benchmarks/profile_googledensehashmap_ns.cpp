#include <iostream>
#include <cstring>
#include <thread>
#include <atomic>
#include <cstdio>
#include <chrono>

#include "ouroboros.hpp"
#include <vector>
#include <tuple>
#include "sparsehash/dense_hash_map"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./profile_googledensehashmap_ns <total size in bytes> <term size in bytes> <number of threads>"

void work_index(long num_terms,
                vector<char*> *terms,
                vector<double> *termsLatency,
                double& allocLatency,
                google::dense_hash_map<char*, long, std::hash<char*>, std::equal_to<char*>>*& termIndex,
                int thread_id,
                int num_threads)
{
    NUMAConfig config;
    config.set_task_numa_node(thread_id);

    {
        chrono::time_point<chrono::high_resolution_clock> start, end;
        chrono::duration<double> diff;
        double latency;
        long N = num_terms / num_threads;

        start = chrono::high_resolution_clock::now();
        termIndex = new google::dense_hash_map<char*, long, std::hash<char*>, std::equal_to<char*>>();
        termIndex->set_empty_key(NULL);
        end = chrono::high_resolution_clock::now();

        diff = end - start;
        allocLatency = diff.count();

        for (long i = thread_id * N; i < (thread_id + 1) * N; i++) {
            start = chrono::high_resolution_clock::now();
            (*termIndex)[terms->at(i)] = i;
            end = chrono::high_resolution_clock::now();
            diff = end - start;
            latency = diff.count();
            (*termsLatency)[i] = latency;
        }
    }
}

tuple<double, double, double, double, double, double, int>
benchmark(long num_terms, vector<char*> *terms, int num_threads)
{
    vector<google::dense_hash_map<char*, long, std::hash<char*>, std::equal_to<char*>>*> termIndexes;
    vector<thread> threads;
    vector<double> termsLatency(num_terms, 0.0);
    vector<double> allocLatency(num_threads, 0.0);
    
    std::tuple<long, bool> query_res;
    char query_term[10] = "000000000";
    bool found_term;

    chrono::time_point<chrono::high_resolution_clock> start, end;
    double latency;
    double min_alloc_latency, avg_alloc_latency, max_alloc_latency;
    double min_index_latency, avg_index_latency, max_index_latency;
    int rc = 0;

    for (int i = 0; i < num_threads; i++) {
        termIndexes.push_back(NULL);
    }

    for (int i = 0; i < num_threads; i++) {
        threads.push_back(thread(work_index,
                                 num_terms,
                                 terms,
                                 &termsLatency,
                                 std::ref(allocLatency[i]),
                                 std::ref(termIndexes[i]),
                                 i,
                                 num_threads));
    }

    for (int i = 0; i < num_threads; i++) {
        threads[i].join();
    }
    
    found_term = false;
    for (int i = 0; i < num_threads; i++) {
        auto search = termIndexes[i]->find((*terms)[num_threads]);
        if (search != termIndexes[i]->end()) {
            found_term = true;
            break;
        }
    }
    if (!found_term) {
        cout << "WARN: term <" << (*terms)[num_threads] << "> was not found in the hashmap!" << endl;
    } else {
        rc = get<0>(query_res);
    }
    
    found_term = false;
    for (int i = 0; i < num_threads; i++) {
        auto search = termIndexes[i]->find(query_term);
        if (search != termIndexes[i]->end()) {
            found_term = true;
            break;
        }
    }
    if (found_term) {
        cout << "WARN: term <" << query_term << "> was found in the hashmap!" << endl;
    }

    min_alloc_latency = allocLatency[0];
    max_alloc_latency = allocLatency[0];
    avg_alloc_latency = allocLatency[0];
    for (int i = 1; i < num_threads; i++) {
        latency = allocLatency[i];
        min_alloc_latency = (min_alloc_latency > latency) ? latency : min_alloc_latency;
        max_alloc_latency = (max_alloc_latency < latency) ? latency : max_alloc_latency;
        avg_alloc_latency += latency;
    }
    avg_alloc_latency = avg_alloc_latency / num_threads;

    min_index_latency = termsLatency[0];
    avg_index_latency = termsLatency[0];
    max_index_latency = termsLatency[0];
    for (long i = 1; i <  num_terms; i++) {
        latency = termsLatency[i];
        min_index_latency = (min_index_latency > latency) ? latency : min_index_latency;
        max_index_latency = (max_index_latency < latency) ? latency : max_index_latency;
        avg_index_latency += latency;
    }
    avg_index_latency = avg_index_latency / num_terms;

    for (auto i = 0; i < num_threads; i++) {
        delete termIndexes[i];
    }

    return make_tuple(min_alloc_latency, avg_alloc_latency, max_alloc_latency,
                      min_index_latency, avg_index_latency, max_index_latency,
                      rc);
}

int main(int argc, char **argv)
{
    long total_size;
    int term_size;
    int num_threads;
    long num_terms;
    
    tuple<std::vector<char*>*, char*> data;
    char *buffer;
    vector<char*> *terms;

    tuple<double, double, double, double, double, double, int> benchmark_rc;
    double min_alloc_latency, avg_alloc_latency, max_alloc_latency;
    double min_index_latency, avg_index_latency, max_index_latency;
    int rc;

    if (argc != 4) {
        cout << MSG << endl;
        return 1;
    }

    total_size = atol(argv[1]);
    term_size = atoi(argv[2]);
    num_threads = atoi(argv[3]);
    num_terms = total_size / (long) term_size;

    data = generate_synthdata(num_terms, term_size - 1);
    buffer = get<1>(data);
    terms = get<0>(data);
    
    benchmark_rc = benchmark(num_terms, terms, num_threads);
    min_alloc_latency = get<0>(benchmark_rc) * 1000 * 1000;
    avg_alloc_latency = get<1>(benchmark_rc) * 1000 * 1000;
    max_alloc_latency = get<2>(benchmark_rc) * 1000 * 1000;
    min_index_latency = get<3>(benchmark_rc) * 1000 * 1000 * 1000;
    avg_index_latency = get<4>(benchmark_rc) * 1000 * 1000 * 1000;
    max_index_latency = get<5>(benchmark_rc) * 1000 * 1000 * 1000;
    rc = get<6>(benchmark_rc);

    delete terms;
    delete[] buffer;

    cout << "Finished hashing/inserting " << total_size << " bytes of data with " << num_threads \
         << " thread(s) and a term size of " << term_size << " bytes!" << endl;
    cout << "Min memory allocation time: " << min_alloc_latency << " microseconds" << endl;
    cout << "Average memory allocation time: " << avg_alloc_latency << " microseconds" << endl;
    cout << "Max memory allocation time: " << max_alloc_latency << " microseconds" << endl;
    cout << "Min term index time: " << min_index_latency << " nanoseconds" << endl;
    cout << "Average term index time: " << avg_index_latency << " nanoseconds" << endl;
    cout << "Max term index time: " << max_index_latency << " nanoseconds" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}