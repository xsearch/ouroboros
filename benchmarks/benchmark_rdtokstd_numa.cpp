#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>

#include "ouroboros.hpp"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_rdtok_numa <source file> <total size in bytes> \
<number of reader threads> <number of tokenizer threads> <block size in bytes>"

#define QUEUE_SIZE_RATIO 4
#define BLOCK_ADDON_SIZE 4096
#define DELIMITERS " \t\n"

void work_tok(MemoryComponentManager* manager,
              atomic<long>* total_num_tokens,
              unsigned int numa_id,
              int block_size)
{
    NUMAConfig config;
    config.set_task_numa_node(numa_id);

    {
        FileDualQueueMemoryComponent* component;
        DualQueue<FileDataBlock*> *queue;
        FileDataBlock *dataBlock;
        StandardTokenizer *tokenizer;
        CTokBLock *tokBlock;
        char *buffer;
        char **tokens;
        char delims[32] = DELIMITERS;
        long num_tokens(0);
        int length;

        // allocate the buffers, the list of tokens for the tokenizer data block and create the 
        buffer = new char[block_size + 1];
        tokens = new char*[block_size / 2 + 1];
        tokBlock = new CTokBLock();
        tokBlock->buffer = buffer;
        tokBlock->tokens = tokens;
        tokenizer = new StandardTokenizer(delims);

        // get the queue component identified by numa_id from the manager
        component = (FileDualQueueMemoryComponent*) manager->getMemoryComponent(MemoryComponentType::DUALQUEUE,
                                                                                numa_id);
        // get the queue from the queue component
        queue = component->getDualQueue();

        // load balancing is achieved through the queue
        while (true) {
            // pop full data block from the queue
            dataBlock = queue->pop_full();
            
            // if the data in the block has a length greater than 0 then tokenzie, otherwise exit the while loop
            length = dataBlock->length;
            if (length > 0) {
                tokenizer->getTokens(dataBlock, tokBlock);
                num_tokens += tokBlock->numTokens;
            }
            
            queue->push_empty(dataBlock);

            if (length == -1) {
                break;
            }
        }

        delete tokenizer;
        delete tokBlock;
        delete[] tokens;
        delete[] buffer;

        *total_num_tokens += num_tokens;
    }
}

void work_read(MemoryComponentManager* manager,
               atomic<int>* file_counter,
               vector<char*>* filenames,
               unsigned int numa_id,
               int block_size)
{
    // self configure to run in numa_id NUMA node
    NUMAConfig config;
    config.set_task_numa_node(numa_id);

    {
        FileDualQueueMemoryComponent* component;
        DualQueue<FileDataBlock*> *queue;
        WaveFileReaderDriver *reader;
        FileDataBlock *dataBlock;
        char delims[32] = DELIMITERS;
        int i;

        // get the queue component identified by numa_id from the manager
        component = (FileDualQueueMemoryComponent*) manager->getMemoryComponent(MemoryComponentType::DUALQUEUE,
                                                                                numa_id);
        // get the queue from the queue component
        queue = component->getDualQueue();

        // load balancing is performed through an atomic counter
        i = file_counter->fetch_add(1, std::memory_order_seq_cst);
        while (i < (int) filenames->size()) {
            // create a new reader driver for the current file
            reader = new WaveFileReaderDriver((*filenames)[i], block_size, BLOCK_ADDON_SIZE, delims);
            
            // try to open the file in the reader driver; if it errors out print message and skip the file
            try {
                reader->open();
            } catch (exception &e) {
                cout << "ERR: could not open file " << (*filenames)[i] << endl;
                delete reader;
                i = file_counter->fetch_add(1, std::memory_order_seq_cst);
                continue;
            }

            while (true) {
                // pop empty data block from the queue
                dataBlock = queue->pop_empty();
                
                // read a block of data from the file into data block buffer
                reader->readNextBlock(dataBlock);
                
                // push full data block to queue (in this case it pushed to the empty queue since there is no consumer)
                queue->push_full(dataBlock);

                // if the reader driver reached the end of the file break from the while loop and read next file
                if (dataBlock->length == 0) {
                    break;
                }
            }

            // close the reader and free memory
            reader->close();
            delete reader;
            
            // load balancing is performed through an atomic counter
            i = file_counter->fetch_add(1, std::memory_order_seq_cst);
        }
    }
}

void work_init(MemoryComponentManager* manager, unsigned int numa_id, int queue_size, int block_size)
{
    // self configure to run in numa_id NUMA node
    NUMAConfig config;
    config.set_task_numa_node(numa_id);

    {
        FileDualQueueMemoryComponent* component;

        // create a new queue component; the component is responsible with intializing the queue and the queue elements
        component = new FileDualQueueMemoryComponent(queue_size,  block_size + BLOCK_ADDON_SIZE);

        // add the queue component to the manager identified by the current numa_id
        manager->addMemoryComponent(MemoryComponentType::DUALQUEUE, numa_id, component);
    }
}

void read_filenames(char *file_path, vector<vector<char*>> &filenames, int num_numa_nodes)
{
    FILE *fp;
    char *buffer, *rc;
    int i;

    fp = fopen(file_path, "r");
    if (fp == NULL) {
        cout << "ERR: cannot open file " << file_path << endl;
        return;
    }

    i = 0;
    while (true) {
        buffer = new char[4096];
        
        rc = fgets(buffer, 4096, fp);
        if (rc == NULL) {
            delete buffer;
            break;
        }
        if (buffer[strlen(buffer) - 1] == '\n') {
            buffer[strlen(buffer) - 1] = '\0';
        }

        filenames[i % num_numa_nodes].push_back(buffer);
        i++;
    }

    fclose(fp);
}

tuple<double, long, int, int> benchmark(char *source_file_path, int num_readers, int num_tokenizers, int block_size)
{
    // the manager is used to store and provide access to NUMA sensitive components (i.e. the queues)
    MemoryComponentManager* manager;
    FileDualQueueMemoryComponent* component;
    DualQueue<FileDataBlock*> *queue;
    FileDataBlock *finalBlock;
    
    // list of names of input files that will be read by the program
    vector<vector<char*>> filenames;
    char* filename;

    // list of threads that initialize the NUMA sensitive components
    vector<thread> threads_init;

    // list of threads that will read the contents of the input files
    vector<thread> threads_read;

    // list of threads that will tokenize the contents of the input files
    vector<thread> threads_tok;

    // atomic counter used by the reader threads to go over the input files
    vector<atomic<int>*> file_counters;

    // atomic int storing the total number of blocks read
    atomic<long> total_num_tokens(0);
    
    int num_numa_nodes;
    int queue_size;
    NUMAConfig config;

    chrono::time_point<chrono::high_resolution_clock> start, end;
    chrono::duration<double> diff;
    double exec_time = 0.0;
    long rc = -1;
    
    // find out how many NUMA nodes exist in the system
    num_numa_nodes = config.get_num_numa_nodes();
    num_numa_nodes = (num_readers < num_numa_nodes) ? num_readers : num_numa_nodes;
    num_numa_nodes = (num_tokenizers < num_numa_nodes) ? num_tokenizers : num_numa_nodes;

    // read the input file names from the file passed as argument to the program
    for (int i = 0; i < num_numa_nodes; i++) {
        filenames.push_back(vector<char*>());
        file_counters.push_back(new atomic<int>(0));
    }
    read_filenames(source_file_path, filenames, num_numa_nodes);

    // create each queue in a specific NUMA node and add the queues to the manager
    manager = new MemoryComponentManager();
    queue_size = QUEUE_SIZE_RATIO * num_tokenizers / num_numa_nodes;
    
    for (int i = 0; i < num_numa_nodes; i++) {
        threads_init.push_back(thread(work_init, manager, i, queue_size, block_size));
    }

    for (int i = 0; i < num_numa_nodes; i++) {
        threads_init[i].join();
    }

    // check to see if the queues were allocated successfully
    for (int i = 0; i < num_numa_nodes; i++) {
        component = (FileDualQueueMemoryComponent*) manager->getMemoryComponent(MemoryComponentType::DUALQUEUE, i);
        if (!component->isAllocated()) {
            cout << "ERR: Could not allocate a buffer for the a queue!" << endl;
            
            // free all NUMA sensitive memory
            delete manager;

            // free the buffers storing the input file names
            for (int i = 0; i < num_numa_nodes; i++) {
                while (filenames[i].size() > 0) {
                    filename = filenames[i].back();
                    filenames[i].pop_back();
                    delete[] filename;
                }
                delete file_counters[i];
            }

            // exit the benchmark on error
            return make_tuple(exec_time, rc, queue_size, num_numa_nodes);
        }
    }

    // start running the benchmark by creating the reader threads
    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_readers; i++) {
        threads_read.push_back(thread(work_read,
                                      manager,
                                      file_counters[i % num_numa_nodes],
                                      &filenames[i % num_numa_nodes],
                                      i % num_numa_nodes,
                                      block_size));
    }

    for (int i = 0; i < num_tokenizers; i++) {
        threads_tok.push_back(thread(work_tok,
                                     manager,
                                     &total_num_tokens,
                                     i % num_numa_nodes,
                                     block_size + BLOCK_ADDON_SIZE));
    }
    
    for (int i = 0; i < num_readers; i++) {
        threads_read[i].join();
    }

    for (int i = 0; i < num_numa_nodes; i++) {
        for (int j = 0; j < num_tokenizers / num_numa_nodes + 1; j++) {
            component = (FileDualQueueMemoryComponent*) manager->getMemoryComponent(MemoryComponentType::DUALQUEUE, i);
            queue = component->getDualQueue();
            finalBlock = queue->pop_empty();
            finalBlock->length = -1;
            queue->push_full(finalBlock);
        }
    }

    for (int i = 0; i < num_tokenizers; i++) {
        threads_tok[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    exec_time = diff.count();
    rc = total_num_tokens;

    // free all NUMA sensitive memory components
    delete manager;

    // free the buffers storing the input file names
    for (int i = 0; i < num_numa_nodes; i++) {
        while (filenames[i].size() > 0) {
            filename = filenames[i].back();
            filenames[i].pop_back();
            delete[] filename;
        }
        delete file_counters[i];
    }

    return make_tuple(exec_time, rc, queue_size, num_numa_nodes);
}

int main(int argc, char **argv)
{
    char source_file_path[4096];
    long total_size;
    int num_readers;
    int num_tokenizers;
    int block_size;
    int queue_size;
    int num_queues;

    tuple<double, long, int, int> benchmark_rc;
    double exec_time;
    long rc;

    if (argc != 6) {
        cout << MSG << endl;
        return 1;
    }
    
    strcpy(source_file_path, argv[1]);
    total_size = atol(argv[2]);
    num_readers = atoi(argv[3]);
    num_tokenizers = atoi(argv[4]);
    block_size = atoi(argv[5]);

    benchmark_rc = benchmark(source_file_path, num_readers, num_tokenizers, block_size);
    exec_time = get<0>(benchmark_rc);
    rc = get<1>(benchmark_rc);
    queue_size = get<2>(benchmark_rc);
    num_queues = get<3>(benchmark_rc);

    cout << "Finished tokenizing " << total_size << " bytes of data:" << endl;
    cout << "- number of reader threads: " << num_readers << endl;
    cout << "- number of tokenizer threads: " << num_tokenizers << endl;
    cout << "- number of queues: " << num_queues << endl;
    cout << "- queue size: " << queue_size << endl;
    cout << "- block size: " << block_size << " bytes" << endl;
    cout << "- total execution time: " << exec_time << " seconds" << endl;
    cout << "- throughput: " << (total_size / (1024 * 1024)) / exec_time << " MiB/sec" << endl;
    cout << "- throughput: " << (rc / exec_time) << " tokens/sec" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}