#include <iostream>
#include <cstring>
#include <thread>
#include <atomic>
#include <cstdio>

extern "C"
{
    #include <sys/time.h>
}

#include "ouroboros.hpp"
#include <vector>
#include <tuple>

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_stringrecordhashmap_ns <total size in bytes> <term size in bytes> \
<number of threads> <number of buckets> <buffer capacity step> <record capacity step>"

void work_index(long num_terms,
                vector<char*> *terms,
                StringRecordHashMap *termIndex,
                CharPtrCapBuffer *buffer,
                int thread_id,
                int num_threads)
{
    char *token;
    tuple<long, bool> rc;

    for (long i = thread_id; i < num_terms; i += num_threads) {
        token = buffer->copyToPos(terms->at(i));
        rc = termIndex->insert(token);
        if (!get<1>(rc)) {
            buffer->movePosNext();
        }
    }
}

int benchmark(long num_terms,
              vector<char*> *terms,
              int num_threads,
              long num_buckets,
              long buff_cap_step,
              long recc_cap_step)
{
    vector<StringRecordHashMap*> termIndexes;
    vector<RecordManager<char*>*> recordManagers;
    vector<thread*> threads;
    vector<CharPtrCapBuffer*> buffers;
    int rc = 0;
    std::tuple<long, bool> query_res;
    char query_term[10] = "000000000";
    bool found_term;
    
    for (auto i = 0; i < num_threads; i++) {
        recordManagers.push_back(new RecordManager<char*>(recc_cap_step));
        termIndexes.push_back(new StringRecordHashMap(recordManagers[i], num_buckets / num_threads));
        buffers.push_back(new CharPtrCapBuffer(buff_cap_step));
        threads.push_back(new thread(work_index, num_terms, terms, termIndexes[i], buffers[i], i, num_threads));
    }

    for (auto i = 0; i < num_threads; i++) {
        threads[i]->join();
    }
    
    found_term = false;
    for (auto i = 0; i < num_threads; i++) {
        query_res = termIndexes[i]->lookup((*terms)[num_threads]);
        if (get<1>(query_res)) {
            found_term = true;
            break;
        }
    }
    if (!found_term) {
        cout << "WARN: term <" << (*terms)[num_threads] << "> was not found in the hashmap!" << endl;
    } else {
        rc = get<0>(query_res);
    }
    
    found_term = false;
    for (auto i = 0; i < num_threads; i++) {
        query_res = termIndexes[i]->lookup(query_term);
        if (get<1>(query_res)) {
            found_term = true;
            break;
        }
    }
    if (found_term) {
        cout << "WARN: term <" << query_term << "> was found in the hashmap!" << endl;
    }

    for (auto i = 0; i < num_threads; i++) {
        delete termIndexes[i];
        delete recordManagers[i];
        delete buffers[i];
    }

    return rc;
}

int main(int argc, char **argv)
{
    long total_size;
    int term_size;
    int num_threads;
    long num_buckets;
    long buff_cap_step;
    long recc_cap_step;
    long num_terms;
    
    char *buffer;
    vector<char*> *terms;
    tuple<std::vector<char*>*, char*> data;

    struct timeval start, end;
    double exec_time;
    int rc = 0;

    if (argc != 7) {
        cout << MSG << endl;
        return 1;
    }

    total_size = atol(argv[1]);
    term_size = atoi(argv[2]);
    num_threads = atoi(argv[3]);
    num_buckets = atol(argv[4]);
    buff_cap_step = atol(argv[5]);
    recc_cap_step = atol(argv[6]);
    num_terms = total_size / (long) term_size;

    data = generate_synthdata(num_terms, term_size - 1);
    buffer = get<1>(data);
    terms = get<0>(data);
    
    gettimeofday(&start, NULL);
    rc = benchmark(num_terms, terms, num_threads, num_buckets, buff_cap_step, recc_cap_step);
    gettimeofday(&end, NULL);
    exec_time = (double) (end.tv_sec - start.tv_sec) + (double) (end.tv_usec - start.tv_usec) / 1000000;

    delete terms;
    delete[] buffer;

    cout << "Finished hashing " << total_size << " bytes of data with " << num_threads \
        << " thread(s) and a term size of " << term_size << " bytes!" << endl;
    cout << "Total execution time: " << exec_time << " seconds" << endl;
    cout << "Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MB/sec" << endl;
    cout << "Throughput: " << ((double) num_terms / 1000) / exec_time << " kOPS" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}