#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>
#include <memory>

#include "ouroboros.hpp"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_rdtokidx_inf <source file> <total size in bytes> <number of numa nodes> \
<number of reader threads> <number of indexer threads> <block size in bytes> <page size in bytes> \
<index implementation> <queries file>"

#define QUEUE_SIZE_RATIO 2
#define BLOCK_ADDON_SIZE 4096
#define DELIMITERS " \t\n"
#define MAX_RESULTS 2

void work_index(MemoryComponentManager* manager,
                atomic<long>* total_num_tokens,
                unsigned int numa_id,
                unsigned int queue_id,
                unsigned int index_id,
                int block_size)
{
    NUMAConfig config;
    config.set_numa_node(numa_id);

    {
        TFIDFIndexMemoryComponent* componentIndex;
        shared_ptr<BaseTFIDFIndex> index;
        FileDualQueueMemoryComponent* componentQueue;
        DualQueue<FileDataBlock*> *queue;
        FileDataBlock *dataBlock;
        BranchLessTokenizer *tokenizer;
        CTokBLock *tokBlock;
        char *buffer;
        char **tokens;
        char delims[32] = DELIMITERS;
        int length;

        // allocate the buffers, the list of tokens for the tokenizer data block and create the 
        buffer = new char[block_size + 1];
        tokens = new char*[block_size / 2 + 1];
        tokBlock = new CTokBLock();
        tokBlock->buffer = buffer;
        tokBlock->tokens = tokens;
        tokenizer = new BranchLessTokenizer(delims);

        // get the paged string store component identified by worker_id from the manager
        componentIndex = (TFIDFIndexMemoryComponent*) 
                         manager->getMemoryComponent(MemoryComponentType::TFIDF_INDEX, index_id);

        // get the store from the store component
        index = componentIndex->getTFIDFIndex();

        // get the queue component identified by numa_id from the manager
        componentQueue = (FileDualQueueMemoryComponent*)
                         manager->getMemoryComponent(MemoryComponentType::DUALQUEUE, queue_id);
        
        // get the queue from the queue component
        queue = componentQueue->getDualQueue();

        // load balancing is achieved through the queue
        while (true) {
            // pop full data block from the queue
            dataBlock = queue->pop_full();
            
            // if the data in the block has a length greater than 0 then tokenzie, otherwise exit the while loop
            length = dataBlock->length;
            if (length > 0) {
                tokenizer->getTokens(dataBlock, tokBlock);

                for (long i = 0; i < tokBlock->numTokens; i++) {
                    index->insert(tokBlock->tokens[i], dataBlock->fileIdx);
                }
            }
            
            queue->push_empty(dataBlock);

            if (length == -1) {
                break;
            }
        }

        delete tokenizer;
        delete tokBlock;
        delete[] tokens;
        delete[] buffer;

        *total_num_tokens += index->getNumTerms();
    }
}

void work_read(MemoryComponentManager* manager,
               atomic<int>* file_counter,
               vector<char*>* filenames,
               unsigned int numa_id,
               unsigned int queue_id,
               int block_size)
{
    // self configure to run in numa_id NUMA node
    NUMAConfig config;
    config.set_numa_node(numa_id);

    {
        FileDualQueueMemoryComponent* componentQueue;
        FileIndexMemoryComponent* componentFileIndex;
        DualQueue<FileDataBlock*> *queue;
        shared_ptr<BaseFileIndex> index;
        WaveFileReaderDriver *reader;
        FileDataBlock *dataBlock;
        char delims[32] = DELIMITERS;
        int i, length;
        long fileIdx;

        // get the queue component identified by queue_id
        componentQueue = (FileDualQueueMemoryComponent*)
                         manager->getMemoryComponent(MemoryComponentType::DUALQUEUE, queue_id);
        
        // get the queue from the queue component
        queue = componentQueue->getDualQueue();

        // get the file index component identified by queue_id
        componentFileIndex = (FileIndexMemoryComponent*)
                             manager->getMemoryComponent(MemoryComponentType::FILE_INDEX, queue_id);
        
        // get the file index from the file index component
        index = componentFileIndex->getFileIndex();

        // load balancing is performed through an atomic counter
        i = file_counter->fetch_add(1, std::memory_order_seq_cst);
        while (i < (int) filenames->size()) {
            // create a new reader driver for the current file
            reader = new WaveFileReaderDriver((*filenames)[i], block_size, BLOCK_ADDON_SIZE, delims);
            
            // try to open the file in the reader driver; if it errors out print message and skip the file
            try {
                reader->open();
                fileIdx = index->insert((*filenames)[i]);
            } catch (exception &e) {
                cout << "ERR: could not open file " << (*filenames)[i] << endl;
                delete reader;
                i = file_counter->fetch_add(1, std::memory_order_seq_cst);
                continue;
            }

            while (true) {
                // pop empty data block from the queue
                dataBlock = queue->pop_empty();
                
                // read a block of data from the file into data block buffer
                reader->readNextBlock(dataBlock);
                dataBlock->fileIdx = fileIdx;
                length = dataBlock->length;
                
                // push full data block to queue (in this case it pushed to the empty queue since there is no consumer)
                queue->push_full(dataBlock);

                // if the reader driver reached the end of the file break from the while loop and read next file
                if (length == 0) {
                    break;
                }
            }

            // close the reader and free memory
            reader->close();
            delete reader;
            
            // load balancing is performed through an atomic counter
            i = file_counter->fetch_add(1, std::memory_order_seq_cst);
        }
    }
}

void work_init_indexes(MemoryComponentManager* manager,
                       unsigned int numa_id,
                       unsigned int index_id,
                       long page_size,
                       long initial_capacity,
                       TFIDFIndexMemoryComponentType store_type)
{
    // self configure to run in numa_id NUMA node
    NUMAConfig config;
    config.set_numa_node(numa_id);

    {
        TFIDFIndexMemoryComponent* component;
        unsigned long numBuckets;
        size_t bucketSize = ChainedHashTable<const char*,
                                             PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>*,
                                             cstr_hash,
                                             cstr_equal>::getBucketSizeInBytes();
        
        numBuckets = get_next_prime_number(initial_capacity / bucketSize);

        // create a new page string store component; the component is responsible with storing the terms sequentially
        component = new TFIDFIndexMemoryComponent(page_size, store_type, numBuckets);

        // add the string store component to the manager identified by the current worker_id
        manager->addMemoryComponent(MemoryComponentType::TFIDF_INDEX, index_id, component);
    }
}

void work_init_queues(MemoryComponentManager* manager,
                      unsigned int numa_id,
                      unsigned int queue_id,
                      int queue_size,
                      int block_size)
{
    // self configure to run in numa_id NUMA node
    NUMAConfig config;
    config.set_numa_node(numa_id);

    {
        FileDualQueueMemoryComponent* componentQueue;
        FileIndexMemoryComponent* componentFileIndex;

        // create a new queue component; the component is responsible with intializing the queue and the queue elements
        componentQueue = new FileDualQueueMemoryComponent(queue_size, block_size + BLOCK_ADDON_SIZE);

        // create a new file index component; the component is responsible for maintaining the indexes of file paths
        componentFileIndex = new FileIndexMemoryComponent(FileIndexMemoryComponentType::STD);

        // add the queue component to the manager, identified by the current queue_id
        manager->addMemoryComponent(MemoryComponentType::DUALQUEUE, queue_id, componentQueue);

        // add the file index component to the manager, identified by the current queue_id
        manager->addMemoryComponent(MemoryComponentType::FILE_INDEX, queue_id, componentFileIndex);
    }
}

void read_search_terms(char *file_path, vector<char*> &search_terms)
{
    FILE *fp;
    char *buffer, *rc;

    fp = fopen(file_path, "r");
    if (fp == NULL) {
        cout << "ERR: cannot open file " << file_path << endl;
        return;
    }

    while (true) {
        buffer = new char[4096];
        
        rc = fgets(buffer, 4096, fp);
        if (rc == NULL) {
            delete buffer;
            break;
        }
        if (buffer[strlen(buffer) - 1] == '\n') {
            buffer[strlen(buffer) - 1] = '\0';
        }

        search_terms.push_back(buffer);
    }

    fclose(fp);
}

void read_filenames(char *file_path, vector<vector<char*>> &filenames, int num_numa_nodes)
{
    FILE *fp;
    char *buffer, *rc;
    int i;

    fp = fopen(file_path, "r");
    if (fp == NULL) {
        cout << "ERR: cannot open file " << file_path << endl;
        return;
    }

    i = 0;
    while (true) {
        buffer = new char[4096];
        
        rc = fgets(buffer, 4096, fp);
        if (rc == NULL) {
            delete buffer;
            break;
        }
        if (buffer[strlen(buffer) - 1] == '\n') {
            buffer[strlen(buffer) - 1] = '\0';
        }

        filenames[i % num_numa_nodes].push_back(buffer);
        i++;
    }

    fclose(fp);
}

tuple<double, double, double, long, int, int, int> benchmark(char *source_file_path,
                                                             char *search_file_path,
                                                             int set_num_numa_nodes,
                                                             int set_num_readers,
                                                             int num_indexers,
                                                             int block_size,
                                                             long page_size,
                                                             long total_size,
                                                             TFIDFIndexMemoryComponentType store_type)
{
    // the manager is used to store and provide access to NUMA sensitive components (i.e. the queues)
    MemoryComponentManager* manager;
    FileDualQueueMemoryComponent* component;
    DualQueue<FileDataBlock*> *queue;
    FileDataBlock *finalBlock;
    
    // list of names of input files that will be read by the program
    vector<vector<char*>> filenames;
    char* filename;

    // list of search terms that will be used to query the index
    vector<char*> search_terms;
    char* search_term;

    // list of threads that initialize the NUMA sensitive components
    vector<thread> threads_init;

    // list of threads that will read the contents of the input files
    vector<thread> threads_read;

    // list of threads that will tokenize the contents of the input files
    vector<thread> threads_tok;

    // atomic counter used by the reader threads to go over the input files
    vector<atomic<int>*> file_counters;

    // atomic int storing the total number of blocks read
    atomic<long> total_num_tokens(0);
    
    int num_numa_nodes;
    int num_readers;
    int queue_size;
    NUMAConfig config;

    chrono::time_point<chrono::high_resolution_clock> start, end;
    chrono::duration<double> diff;
    double exec_time = 0.0;
    double init_time = 0.0;
    double search_time = 0.0;
    long rc = 0;
    
    // find out how many NUMA nodes exist in the system
    num_numa_nodes = config.get_num_numa_nodes();
    num_numa_nodes = (set_num_numa_nodes < num_numa_nodes) ? set_num_numa_nodes : num_numa_nodes;
    num_readers = (set_num_readers < num_indexers) ? set_num_readers : num_indexers;
    num_numa_nodes = (num_readers < num_numa_nodes) ? num_readers : num_numa_nodes;

    // read the input file names from the file passed as argument to the program
    for (int i = 0; i < num_numa_nodes; i++) {
        filenames.push_back(vector<char*>());
        file_counters.push_back(new atomic<int>(0));
    }
    read_filenames(source_file_path, filenames, num_numa_nodes);

    // read the search terms from the file passed as argument to the program
    read_search_terms(search_file_path, search_terms);

    // create each queue in a specific NUMA node and add the queues to the manager
    manager = new MemoryComponentManager();
    queue_size = QUEUE_SIZE_RATIO * num_indexers / num_readers;
    
    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_readers; i++) {
        threads_init.push_back(thread(work_init_queues,
                                      manager,
                                      i % num_numa_nodes,
                                      i,
                                      queue_size,
                                      block_size));
    }

    for (int i = 0; i < num_indexers; i++) {
        threads_init.push_back(thread(work_init_indexes,
                                      manager,
                                      i % num_numa_nodes,
                                      i,
                                      page_size,
                                      total_size / num_indexers,
                                      store_type));
    }

    for (int i = 0; i < num_readers + num_indexers; i++) {
        threads_init[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    init_time = diff.count();

    // start running the benchmark by creating the reader threads
    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_readers; i++) {
        threads_read.push_back(thread(work_read,
                                      manager,
                                      file_counters[i % num_numa_nodes],
                                      &filenames[i % num_numa_nodes],
                                      i % num_numa_nodes,
                                      i,
                                      block_size));
    }

    for (int i = 0; i < num_indexers; i++) {
        threads_tok.push_back(thread(work_index,
                                     manager,
                                     &total_num_tokens,
                                     i % num_numa_nodes,
                                     i % num_readers,
                                     i,
                                     block_size + BLOCK_ADDON_SIZE));
    }
    
    for (int i = 0; i < num_readers; i++) {
        threads_read[i].join();
    }

    for (int i = 0; i < num_readers; i++) {
        for (int j = 0; j < num_indexers / num_readers + 1; j++) {
            component = (FileDualQueueMemoryComponent*) manager->getMemoryComponent(MemoryComponentType::DUALQUEUE, i);
            queue = component->getDualQueue();
            finalBlock = queue->pop_empty();
            finalBlock->length = -1;
            queue->push_full(finalBlock);
        }
    }

    for (int i = 0; i < num_indexers; i++) {
        threads_tok[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    exec_time = diff.count();

    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < search_terms.size(); i++) {
        int frequency = 0;
        long numFiles = MAX_RESULTS;
        
        cout << "- " << search_terms[i] << " : [ ";
        
        for (int index_id = 0; index_id < num_indexers; index_id++) {
            TFIDFIndexMemoryComponent* componentIndex;
            FileIndexMemoryComponent* componentFileIndex;
            shared_ptr<BaseTFIDFIndex> index;
            shared_ptr<BaseFileIndex> fileIndex;
            shared_ptr<TFIndexResult> result;
            long idx;
            

            // get the TFIDF index component identified by index_id
            componentIndex = (TFIDFIndexMemoryComponent*) 
                             manager->getMemoryComponent(MemoryComponentType::TFIDF_INDEX, index_id);

            // get the TFIDF index from the component
            index = componentIndex->getTFIDFIndex();

            // get the file index component identified by index_id modulo num_readers
            componentFileIndex = (FileIndexMemoryComponent*)
                                 manager->getMemoryComponent(MemoryComponentType::FILE_INDEX, index_id % num_readers);
            
            // get the file index from the component
            fileIndex = componentFileIndex->getFileIndex();

            // search the index for the query term and return the term and inverse document frequencies
            result = index->lookup(search_terms[i]);
            for (int j = 0; (j < result->files.size()) and (numFiles > 0); j++, numFiles--) {
                const char* file_path = fileIndex->reverseLookup(result->files[j].fileIdx);
                cout << file_path << ":" << result->files[j].fileFrequency << " ";
                
            }
            frequency += result->termFrequency;
        }
        
        if (numFiles == 0) {
            cout << "... ] " << frequency << endl;
        } else {
            cout << "] " << frequency << endl;
        }
        
        rc += frequency;
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    search_time = diff.count();

    // free all NUMA sensitive memory components
    delete manager;

    // free the buffers storing the input file names
    for (int i = 0; i < num_numa_nodes; i++) {
        while (filenames[i].size() > 0) {
            filename = filenames[i].back();
            filenames[i].pop_back();
            delete[] filename;
        }
        delete file_counters[i];
    }
    for (int i = 0; i < search_terms.size(); i++) {
        search_term = search_terms.back();
        search_terms.pop_back();
        delete[] search_term;
    }

    return make_tuple(exec_time, init_time, search_time, rc, num_numa_nodes, num_readers, queue_size);
}

int main(int argc, char **argv)
{
    char source_file_path[4096];
    long total_size;
    int num_numa_nodes;
    int num_readers;
    int num_indexers;
    int queue_size;
    int block_size;
    long page_size;
    char implementation[16];
    char search_file_path[4096];
    TFIDFIndexMemoryComponentType store_type;

    tuple<double, double, double, long, int, int, int> benchmark_rc;
    double exec_time, init_time, search_time;
    long rc;

    if (argc != 10) {
        cout << MSG << endl;
        return 1;
    }

    strcpy(source_file_path, argv[1]);
    total_size = atol(argv[2]);
    num_numa_nodes = atoi(argv[3]);
    num_readers = atoi(argv[4]);
    num_indexers = atoi(argv[5]);
    block_size = atoi(argv[6]);
    page_size = atol(argv[7]);
    strcpy(implementation, argv[8]);
    strcpy(search_file_path, argv[9]);

    if (strcmp(implementation, "std") == 0) {
        store_type = TFIDFIndexMemoryComponentType::STD;
    } else if (strcmp(implementation, "dense") == 0) {
        store_type = TFIDFIndexMemoryComponentType::DENSE;
    } else if (strcmp(implementation, "swiss") == 0) {
        store_type = TFIDFIndexMemoryComponentType::SWISS;
    } else if (strcmp(implementation, "chained") == 0) {
        store_type = TFIDFIndexMemoryComponentType::CHAINED;
    } else if (strcmp(implementation, "pstd") == 0) {
        store_type = TFIDFIndexMemoryComponentType::PSTD;
    } else if (strcmp(implementation, "pdense") == 0) {
        store_type = TFIDFIndexMemoryComponentType::PDENSE;
    } else if (strcmp(implementation, "pswiss") == 0) {
        store_type = TFIDFIndexMemoryComponentType::PSWISS;
    } else if (strcmp(implementation, "pchained") == 0) {
        store_type = TFIDFIndexMemoryComponentType::PCHAINED;
    } else {
        cout << "ERR: unrecognized term index implementation!" << endl;
        return 2;
    }

    cout << "Started indexing, storing and searching " << total_size << " bytes of data:" << endl;
    benchmark_rc = benchmark(source_file_path,
                             search_file_path,
                             num_numa_nodes,
                             num_readers,
                             num_indexers,
                             block_size,
                             page_size,
                             total_size,
                             store_type);
    exec_time = get<0>(benchmark_rc);
    init_time = get<1>(benchmark_rc);
    search_time = get<2>(benchmark_rc);
    rc = get<3>(benchmark_rc);
    num_numa_nodes = get<4>(benchmark_rc);
    num_readers = get<5>(benchmark_rc);
    queue_size = get<6>(benchmark_rc);

    cout << "Finished indexing, storing and searching " << total_size << " bytes of data:" << endl;
    cout << "- number of reader threads: " << num_readers << endl;
    cout << "- number of indexing threads: " << num_indexers << endl;
    cout << "- number of NUMA nodes: " << num_numa_nodes << endl;
    cout << "- number of queues: " << num_readers << endl;
    cout << "- queue size: " << queue_size << endl;
    cout << "- block size: " << block_size << " bytes" << endl;
    cout << "- page size: " << page_size << " bytes" << endl;
    cout << "- total initialization time: " << init_time << " seconds" << endl;
    cout << "- total execution time: " << exec_time << " seconds" << endl;
    cout << "- total search time: " << search_time << " seconds" << endl;
    cout << "- throughput: " << (total_size / (1024 * 1024)) / exec_time << " MiB/sec" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}