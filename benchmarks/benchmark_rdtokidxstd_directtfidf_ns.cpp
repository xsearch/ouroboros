#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>
#include <functional>
#include <vector>
#include <tuple>
#include <mutex>
#include <condition_variable>

#include "ouroboros.hpp"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_rdtokidxstd_directtfidf_ns <source file> \
<total size in bytes> <number of index threads> <search term>"

#define QUEUE_SIZE_RATIO 4
#define QUEUE_BLOCK_SIZE 262144
#define DELIMS " \t\n/_.,;:"

struct numa_group_sync_t {
    std::mutex mtx;
    std::condition_variable cv;
    bool ready;

    numa_group_sync_t() {
        ready = false;
    }
};

void work_index(DualQueue<FileDataBlock*>*& queue,
                StdDirectTFIDFIndex*& termsIndex,
                numa_group_sync_t& synd_data,
                int numa_location)
{
    NUMAConfig config;
    config.set_numa_node(numa_location);

    {
        FileDataBlock *dataBlock;
        BranchLessTokenizer *tokenizer;
        CTokBLock *tokBlock;
        char delims[32] = DELIMS;
        int length;

        termsIndex = new StdDirectTFIDFIndex();
        tokBlock = new CTokBLock();
        tokBlock->buffer = new char[QUEUE_BLOCK_SIZE + 1];
        tokBlock->tokens = new char*[QUEUE_BLOCK_SIZE / 2 + 1];
        tokenizer = new BranchLessTokenizer(delims);
        
        while (true) {
            dataBlock = queue->pop_full();
        
            length = dataBlock->length;
            if (length > 0) {
                tokenizer->getTokens(dataBlock, tokBlock);
                for (long i = 0; i < tokBlock->numTokens; i++) {
                    termsIndex->insert(tokBlock->tokens[i], dataBlock->fileIdx);
                }
            }
        
            queue->push_empty(dataBlock);

            if (length == -1) {
                break;
            }
        }

        delete[] tokBlock->buffer;
        delete[] tokBlock->tokens;
        delete tokBlock;
        delete tokenizer;
    }
}

void work_read(vector<char*>& files,
               atomic<unsigned int>& position,
               StdAppendFileIndex*& fileIndex,
               DualQueue<FileDataBlock*>*& queue,
               numa_group_sync_t& synd_data,
               int num_threads_per_numa,
               int numa_location)
{
    NUMAConfig config;
    config.set_numa_node(numa_location);

    {
        PFileReaderDriver *reader;
        FileDataBlock *dataBlock;
        FileDataBlock *dataBlocks;
        char *buffers;
        long fileIdx;
        char *filePath;
        int pos;
        int rc;

        queue = new DualQueue<FileDataBlock*>(QUEUE_SIZE_RATIO * num_threads_per_numa);
        dataBlocks = new FileDataBlock[QUEUE_SIZE_RATIO * num_threads_per_numa]();
        buffers = NULL;
        rc = posix_memalign((void**) &buffers, 512, QUEUE_BLOCK_SIZE * QUEUE_SIZE_RATIO * (long) num_threads_per_numa);
        if (rc != 0) {
            cout << "ERROR: Could not allocate buffers for the benchmark!" << endl;
            delete[] dataBlocks;
            delete queue;
            return;
        }

        for (int i = 0; i < QUEUE_SIZE_RATIO * num_threads_per_numa; i++) {
            dataBlocks[i].buffer = &buffers[i * QUEUE_BLOCK_SIZE];
            queue->push_empty(&dataBlocks[i]);
        }
        fileIndex = new StdAppendFileIndex();

        pos = position.fetch_add(1, std::memory_order_seq_cst);
        while (pos < (int) files.size()) {
            reader = new PFileReaderDriver(files[pos], QUEUE_BLOCK_SIZE);
            try {
                reader->open();
            } catch (exception &e) {
                cout << "ERROR: could not open file " << files[pos] << endl;
                delete reader;
                pos = position.fetch_add(1, std::memory_order_seq_cst);
                continue;    
            }

            fileIdx = fileIndex->insert(files.at(pos));
            filePath = (char*) fileIndex->reverseLookup(dataBlock->fileIdx);

            while (true) {
                dataBlock = queue->pop_empty();

                reader->readNextBlock(dataBlock);
                dataBlock->fileIdx = fileIdx;
                dataBlock->filepath = filePath;

                queue->push_full(dataBlock);

                if (dataBlock->length == 0) {
                    break;
                }
            }

            reader->close();
            delete reader;

            pos = position.fetch_add(1, std::memory_order_seq_cst);
        }
    }
}

void read_filenames(char *file_path, vector<char*> &filenames)
{
    FILE *fp;
    char *buffer, *rc;

    fp = fopen(file_path, "r");
    if (fp == NULL) {
        cout << "ERR: cannot open file " << file_path << endl;
        return;
    }

    while (true) {
        buffer = new char[4096];
        
        rc = fgets(buffer, 4096, fp);
        if (rc == NULL) {
            delete buffer;
            break;
        }
        if (buffer[strlen(buffer) - 1] == '\n') {
            buffer[strlen(buffer) - 1] = '\0';
        }

        filenames.push_back(buffer);
    }

    fclose(fp);
}

tuple<double, int> benchmark(char* source_file, long total_bytes, int num_index_threads, char* search_term)
{
    vector<StdAppendFileIndex*> fileIndexes;
    vector<StdDirectTFIDFIndex*> termsIndexes;
    vector<thread> index_threads;
    vector<DualQueue<FileDataBlock*>*>* queues;
    vector<thread> reader_threads;
    vector<numa_group_sync_t> sync_data; 
    vector<char*> files;
    atomic<unsigned int> file_pos(0);
    NUMAConfig config;
    int num_numa_nodes;
    int num_reader_threads;

    long searchTermIdx;
    std::tuple<const char*, long, long> lookupResult;

    chrono::time_point<chrono::high_resolution_clock> start, end;
    chrono::duration<double> diff;
    double exec_time;
    int rc = 0;

    read_filenames(source_file, files);
    num_numa_nodes = config.get_num_numa_nodes();
    num_reader_threads = (num_numa_nodes > num_index_threads) ? num_index_threads : num_numa_nodes;

    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_reader_threads; i++) {
        fileIndexes.push_back(nullptr);
        queues->push_back(nullptr);
        sync_data.push_back(numa_group_sync_t());
    }

    for (int i = 0; i < num_index_threads; i++) {
        termsIndexes.push_back(nullptr);
    }

    for (int i = 0; i < num_reader_threads; i++) {
        reader_threads.push_back(thread(work_read,
                                        std::ref(files),
                                        std::ref(file_pos),
                                        std::ref(fileIndexes[i]),
                                        std::ref(queues[i]),
                                        std::ref(sync_data[i]),
                                        num_index_threads / num_reader_threads,
                                        i));
    }

    for (int i = 0; i < num_index_threads; i++) {
        int j = i % num_reader_threads;
        index_threads.push_back(thread(work_index,
                                       std::ref(queues[j]),
                                       std::ref(termsIndexes[i]),
                                       std::ref(sync_data[j]),
                                       j));
    }

    for (int i = 0; i < num_reader_threads; i++) {
        reader_threads[i].join();
    }

    for (int i = 0; i < num_index_threads; i++) {
        index_threads[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    exec_time = diff.count();
    
    for (int i = 0; i < num_index_threads; i++) {
        searchTermIdx = termsIndexes[i]->lookup(search_term);
        if (searchTermIdx != -1) {
            lookupResult = termsIndexes[i]->reverseLookup(searchTermIdx);
            rc++;
        }
    }

    for (int i = 0; i < num_index_threads; i++) {
        delete fileIndexes[i];
        delete termsIndexes[i];
    }
    
    for (unsigned int i = 0; i < files.size(); i++) {
        delete files[i];
    }

    return make_tuple(exec_time, rc);
}

int main(int argc, char **argv)
{
    char source_file[4096] = "";
    long total_bytes;
    int num_index_threads;
    char search_term[256] = "";
    
    tuple<double, int> benchmark_rc;
    double exec_time;
    int rc;
    

    if (argc != 5) {
        cout << MSG << endl;
        return 1;
    }

    if (strlen(argv[1]) >= 4096 or strlen(argv[5]) >= 256) {
        cout << "FATAL: Arguments too long!" << endl;
        return 2;
    }

    strcpy(source_file, argv[1]);
    total_bytes = atol(argv[2]);
    num_index_threads = atoi(argv[3]);
    strcpy(search_term, argv[4]);
    
    benchmark_rc = benchmark(source_file, total_bytes, num_index_threads, search_term);
    exec_time = get<0>(benchmark_rc);
    rc = get<1>(benchmark_rc);

    cout << "Finished indexing!" << endl;
    cout << "- Total number of bytes: " << total_bytes << endl;
    cout << "- Number of index threads: " << num_index_threads << endl;
    cout << "- Total execution time: " << exec_time << " seconds" << endl;
    cout << "- Throughput: " << ((total_bytes / (1024 * 1024)) / exec_time) << " MiB/sec" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}