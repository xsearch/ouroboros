#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>

#include "ouroboros.hpp"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_rd_nonuma <source file> <total size in bytes> <number of threads> <block size in bytes>"

#define QUEUE_SIZE_RATIO 2

void work_read(DualQueue<FileDataBlock*>* queue,
               atomic<int>* file_counter,
               atomic<long>* total_num_blocks_read,
               vector<char*>* filenames,
               int block_size)
{
    PFileReaderDriver *reader;
    FileDataBlock *dataBlock;
    long num_blocks_read(0);
    int i;

    i = file_counter->fetch_add(1, std::memory_order_seq_cst);
    while (i < filenames->size()) {
        reader = new PFileReaderDriver(filenames->at(i), block_size);
        try {
            reader->open();
        } catch (exception &e) {
            cout << "ERROR: could not open file " << filenames->at(i) << endl;
            delete reader;
            i = file_counter->fetch_add(1, std::memory_order_seq_cst);
            continue;    
        }

        while (true) {
            dataBlock = queue->pop_empty();
            
            reader->readNextBlock(dataBlock);
            
            queue->push_empty(dataBlock);

            if (dataBlock->length == 0) {
                break;
            } else {
                num_blocks_read++;
            }
        }

        reader->close();
        delete reader;

        i = file_counter->fetch_add(1, std::memory_order_seq_cst);
    }

    (*total_num_blocks_read) += num_blocks_read;
}

void read_filenames(char *file_path, vector<char*> &filenames)
{
    FILE *fp;
    char *buffer, *rc;

    fp = fopen(file_path, "r");
    if (fp == NULL) {
        cout << "ERROR: cannot open file " << file_path << endl;
        return;
    }

    while (true) {
        buffer = new char[4096];
        
        rc = fgets(buffer, 4096, fp);
        if (rc == NULL) {
            delete buffer;
            break;
        }
        if (buffer[strlen(buffer) - 1] == '\n') {
            buffer[strlen(buffer) - 1] = '\0';
        }

        filenames.push_back(buffer);
    }

    fclose(fp);
}

tuple<double, long, int, int> benchmark(char *file_path, int num_readers, int block_size)
{
    // the recylable queue that will be used to store the blocks read from disk
    DualQueue<FileDataBlock*> queue(num_readers * QUEUE_SIZE_RATIO);
    FileDataBlock *dataBlocks;
    FileDataBlock *finalBlock;
    char *buffers;
    
    // list of names of input files that will be read by the program
    vector<char*> filenames;
    char* filename;

    // list of threads that will read the contents of the input files
    vector<thread> threads_read;

    // atomic counter used by the reader threads to go over the input files
    atomic<int> file_counter(0);

    // atomic int storing the total number of blocks read
    atomic<long> num_blocks_read(0);

    chrono::time_point<chrono::high_resolution_clock> start, end;
    chrono::duration<double> diff;
    double exec_time = 0.0;
    long rc = -1;
    
    // read the input file names from the file passed as argument to the program
    read_filenames(file_path, filenames);

    dataBlocks = new FileDataBlock[num_readers * QUEUE_SIZE_RATIO]();
    rc = posix_memalign((void**) &buffers, 512, num_readers * QUEUE_SIZE_RATIO * (long) block_size);
    if (rc != 0) {
        cout << "Could not allocate buffers in the benchmark!" << endl;
        delete[] dataBlocks;
        return make_tuple(exec_time, rc, num_readers * QUEUE_SIZE_RATIO, 1);
    }

    for (auto i = 0; i < num_readers * QUEUE_SIZE_RATIO; i++) {
        dataBlocks[i].buffer = &buffers[i * block_size];
        queue.push_empty(&dataBlocks[i]);
    }

    // start running the benchmark by creating the reader threads
    start = chrono::high_resolution_clock::now();    
    for (auto i = 0; i < num_readers; i++) {
        threads_read.push_back(thread(work_read,
                                      &queue,
                                      &file_counter,
                                      &num_blocks_read,
                                      &filenames,
                                      block_size));
    }

    for (auto i = 0; i < num_readers; i++) {
        threads_read[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    exec_time = diff.count();
    rc = num_blocks_read;

    free(buffers);
    delete[] dataBlocks;

    while (filenames.size() > 0) {
        filename = filenames.back();
        filenames.pop_back();
        delete[] filename;
    }

    return make_tuple(exec_time, rc, num_readers * QUEUE_SIZE_RATIO, 1);
}

int main(int argc, char **argv)
{
    char source_file_path[4096];
    long total_size;
    int num_readers;
    int block_size;
    int queue_size;
    int num_queues;

    tuple<double, long, int, int> benchmark_rc;
    double exec_time;
    long rc;

    if (argc != 5) {
        cout << MSG << endl;
        return 1;
    }

    strcpy(source_file_path, argv[1]);
    total_size = atol(argv[2]);
    num_readers = atoi(argv[3]);
    block_size = atoi(argv[4]);

    benchmark_rc = benchmark(source_file_path, num_readers, block_size);
    exec_time = get<0>(benchmark_rc);
    rc = get<1>(benchmark_rc);
    queue_size = get<2>(benchmark_rc);
    num_queues = get<3>(benchmark_rc);

    cout << "Finished reading " << total_size << " bytes of data:" << endl;
    cout << "- number of reader threads: " << num_readers << endl;
    cout << "- number of queues: " << num_queues << endl;
    cout << "- queue size: " << queue_size << endl;
    cout << "- block size: " << block_size << " bytes" << endl;
    cout << "- total execution time: " << exec_time << " seconds" << endl;
    cout << "- throughput: " << (total_size / (1024 * 1024)) / exec_time << " MiB/sec" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}