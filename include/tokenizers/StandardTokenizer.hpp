#ifndef OUROBOROS_STANDARDTOKENIZER_H
#define OUROBOROS_STANDARDTOKENIZER_H

#include "tokenizers/BaseTokenizer.hpp"
#include "tokenizers/CTokBlock.hpp"
#include "readerdrivers/FileDataBlock.hpp"

namespace ouroboros
{
    class StandardTokenizer: public BaseTokenizer
    {
        private:
            char *delim;
        public:
            StandardTokenizer(char *delim);
            virtual void getTokens(BaseDataBlock *dataBlock, BaseTokBlock *tokBlock);
            virtual void getTokens(FileDataBlock *dataBlock, CTokBLock *tokBlock);
            ~StandardTokenizer() = default;
    };
}

#endif