#ifndef OUROBOROS_INDEXING_H
#define OUROBOROS_INDEXING_H

#include <string>
#include <cstring>
#include <cstddef>

#define INDEX_NEXISTS -1
#define TOMBSTONE -1

namespace ouroboros
{
    struct strptr_hash {
        std::size_t operator()(const std::string* k) const
        {
            return std::hash<std::string>()(*k);
        }
    };

    struct strptr_equal {
        bool operator()(const std::string *lhs, const std::string *rhs) const
        {
            return *lhs == *rhs;
        }
    };

    struct cstr_hash {
        std::size_t operator()(const char* k) const
        {
            return std::hash<std::string_view>()(std::string_view(k, std::strlen(k)));
        }
    };

    struct cstr_equal {
        bool operator()(const char* lhs, const char* rhs) const
        {
            return (lhs == rhs) or (lhs and rhs and (std::strcmp(lhs, rhs) == 0));
        }
    };

    struct charptr_hash {
        std::size_t operator()(const char* k) const
        {
            return std::hash<std::string>()(std::string(k));
        }
    };

    struct charptr_equal {
        bool operator()(const char *lhs, const char *rhs) const
        {
            return (std::strcmp(lhs, rhs) == 0);
        }
    };
}

#endif