#ifndef OUROBOROS_STDHASHMAPTERMINDEX_H
#define OUROBOROS_STDHASHMAPTERMINDEX_H

#include <unordered_map>
#include <vector>
#include <string>
#include <mutex>

#include "indexing/data/BaseTermIndex.hpp"
#include "indexing/indexing.hpp"

namespace ouroboros
{
    class StdHashMapTermIndex: public BaseTermIndex
    {
        protected:
            std::unordered_map<std::string*, long, strptr_hash, strptr_equal> *map;
            std::vector<std::string*> *index;
            std::mutex mtx;
            long numTerms;
        public:
            StdHashMapTermIndex();
            virtual ~StdHashMapTermIndex();

            virtual long insert(const char *term);
            virtual void remove(const char *term);
            virtual long lookup(const char *term);
            virtual const char* reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif