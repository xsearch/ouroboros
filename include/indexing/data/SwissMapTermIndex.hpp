#ifndef OUROBOROS_SWISS_MAP_TERM_INDEX_H
#define OUROBOROS_SWISS_MAP_TERM_INDEX_H

#ifdef GOOGLE_SWISS_LIB

#include "indexing/data/BaseTermIndex.hpp"
#include "indexing/indexing.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include <deque>
#include <memory>

#include <absl/container/flat_hash_map.h>

namespace ouroboros
{
    class SwissMapTermIndex: public BaseTermIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> store;
            absl::flat_hash_map<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<const char*> terms;
            long numTerms;
        public:
            explicit SwissMapTermIndex(std::shared_ptr<BasePagedStringStore> store) : 
                store(store), map(), terms(), numTerms(0) { }
            virtual ~SwissMapTermIndex() = default;

            virtual long insert(const char *term);
            virtual void remove(const char *term);
            virtual long lookup(const char *term);
            virtual const char* reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif

#endif