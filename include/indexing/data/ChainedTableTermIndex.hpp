#ifndef OUROBOROS_CHAINED_TABLE_TERM_INDEX_H
#define OUROBOROS_CHAINED_TABLE_TERM_INDEX_H

#include "indexing/data/BaseTermIndex.hpp"
#include "indexing/indexing.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include <deque>
#include <memory>

#include "corestructures/ChainedHashTable.hpp"

namespace ouroboros
{
    class ChainedTableTermIndex: public BaseTermIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> store;
            ChainedHashTable<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<const char*> terms;
            long numTerms;
        public:
            explicit ChainedTableTermIndex(std::shared_ptr<BasePagedStringStore> store, unsigned long numBuckets) : 
                store(store), map(numBuckets), terms(), numTerms(0) { }
            virtual ~ChainedTableTermIndex() = default;

            virtual long insert(const char *term);
            virtual void remove(const char *term);
            virtual long lookup(const char *term);
            virtual const char* reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif