#ifndef OUROBOROS_STD_MAP_TERM_INDEX_H
#define OUROBOROS_STD_MAP_TERM_INDEX_H

#include "indexing/data/BaseTermIndex.hpp"
#include "indexing/indexing.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include <unordered_map>
#include <deque>
#include <memory>

namespace ouroboros
{
    class StdMapTermIndex: public BaseTermIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> store;
            std::unordered_map<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<const char*> terms;
            long numTerms;
        public:
            explicit StdMapTermIndex(std::shared_ptr<BasePagedStringStore> store) : 
                store(store), map(), terms(), numTerms(0) { }
            virtual ~StdMapTermIndex() = default;

            virtual long insert(const char *term);
            virtual void remove(const char *term);
            virtual long lookup(const char *term);
            virtual const char* reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif