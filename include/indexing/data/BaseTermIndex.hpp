#ifndef OUROBOROS_BASETERMINDEX_H
#define OUROBOROS_BASETERMINDEX_H

#include "indexing/data/BaseDataIndex.hpp"

namespace ouroboros
{
    class BaseTermIndex: public BaseDataIndex
    {
        public:
            BaseTermIndex() : BaseDataIndex(DataIndexType::TERM) { }
            virtual ~BaseTermIndex() = default;

            virtual long insert(const char *term) = 0;
            virtual void remove(const char *term) = 0;
            virtual long lookup(const char *term) = 0;
            virtual const char* reverseLookup(long idx) = 0;
            virtual long getNumTerms() = 0;
    };
}

#endif