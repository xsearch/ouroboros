#ifndef OUROBOROS_DENSE_MAP_TERM_INDEX_H
#define OUROBOROS_DENSE_MAP_TERM_INDEX_H

#ifdef GOOGLE_DENSE_LIB

#include "indexing/data/BaseTermIndex.hpp"
#include "indexing/indexing.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include <deque>
#include <memory>

#include <sparsehash/dense_hash_map>

namespace ouroboros
{
    class DenseMapTermIndex: public BaseTermIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> store;
            google::dense_hash_map<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<const char*> terms;
            long numTerms;
        public:
            explicit DenseMapTermIndex(std::shared_ptr<BasePagedStringStore> store) : 
                store(store), map(), terms(), numTerms(0) { map.set_empty_key(NULL); }
            virtual ~DenseMapTermIndex() = default;

            virtual long insert(const char *term);
            virtual void remove(const char *term);
            virtual long lookup(const char *term);
            virtual const char* reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif

#endif