#ifndef OUROBOROS_BASE_TFIDF_INDEX_H
#define OUROBOROS_BASE_TFIDF_INDEX_H

#include "indexing/information/BaseInformationIndex.hpp"
#include "indexing/information/TFIndexResult.hpp"
#include <memory>

namespace ouroboros
{
    class BaseTFIDFIndex: BaseInformationIndex
    {
        public:
            explicit BaseTFIDFIndex() : BaseInformationIndex(InformationIndexType::TFIDF) { }
            virtual ~BaseTFIDFIndex() = default;

            virtual void insert(const char* term, long fileIdx) = 0;
            virtual std::shared_ptr<TFIndexResult> lookup(const char* term) = 0;
            virtual long getNumTerms() = 0;
    };
}

#endif