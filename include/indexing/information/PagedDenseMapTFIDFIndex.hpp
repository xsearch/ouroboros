#ifndef OUROBOROS_PAGED_DENSE_MAP_TFIDF_INDEX_H
#define OUROBOROS_PAGED_DENSE_MAP_TFIDF_INDEX_H

#ifdef GOOGLE_DENSE_LIB

#include "indexing/information/BaseTFIDFIndex.hpp"
#include "indexing/information/TFIndexEntry.hpp"
#include "corestructures/HugePagedBytesStore.hpp"
#include "corestructures/PagedVersatileIndex.hpp"
#include "indexing/indexing.hpp"

#include <sparsehash/dense_hash_map>

namespace ouroboros
{
    class PagedDenseMapTFIDFIndex: public BaseTFIDFIndex
    {
        private:
            std::shared_ptr<HugePagedBytesStore> bytesStore;
            google::dense_hash_map<const char*, PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>*, cstr_hash, cstr_equal> map;
            long numTerms;
        public:
            explicit PagedDenseMapTFIDFIndex(std::shared_ptr<HugePagedBytesStore> bytesStore) :
                bytesStore(bytesStore), map(), numTerms(0)
                { map.set_empty_key(NULL); }
            virtual ~PagedDenseMapTFIDFIndex() = default;

            virtual void insert(const char *term, long fileIdx);
            virtual std::shared_ptr<TFIndexResult> lookup(const char *term);
            virtual long getNumTerms();
    };
}

#endif

#endif