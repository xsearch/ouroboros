#ifndef OUROBOROS_PAGED_SWISS_MAP_TFIDF_INDEX_H
#define OUROBOROS_PAGED_SWISS_MAP_TFIDF_INDEX_H

#ifdef GOOGLE_SWISS_LIB

#include "indexing/information/BaseTFIDFIndex.hpp"
#include "indexing/information/TFIndexEntry.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include "corestructures/PagedVersatileStore.hpp"
#include "corestructures/PagedVersatileIndex.hpp"
#include "corestructures/PagedLinkedElement.hpp"
#include "indexing/indexing.hpp"
#include <deque>

#include <absl/container/flat_hash_map.h>

namespace ouroboros
{
    class PagedSwissMapTFIDFIndex: public BaseTFIDFIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> termStore;
            std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>>> indexStore;
            std::shared_ptr<PagedVersatileStore<PagedLinkedElement<IDFIndexEntry>>> listStore;
            absl::flat_hash_map<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>*> index;
            long numTerms;
        public:
            explicit PagedSwissMapTFIDFIndex(std::shared_ptr<BasePagedStringStore> termStore,
                std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>>> idxStor,
                std::shared_ptr<PagedVersatileStore<PagedLinkedElement<IDFIndexEntry>>> lstStor) : 
                termStore(termStore), indexStore(idxStor), listStore(lstStor), map(), index(), numTerms(0) { }
            virtual ~PagedSwissMapTFIDFIndex() = default;

            virtual long insert(const char *term, long fileIdx);
            virtual long lookup(const char *term);
            virtual std::shared_ptr<TFIndexResult> reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif

#endif