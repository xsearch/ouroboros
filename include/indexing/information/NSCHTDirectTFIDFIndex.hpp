#ifndef OUROBOROS_NS_CHT_DIRECTTFIDF_INDEX_H
#define OUROBOROS_NS_CHT_DIRECTTFIDF_INDEX_H

#include "indexing/information/BaseInformationIndex.hpp"
#include "corestructures/NSMemBytesStore.hpp"
#include "corestructures/ChainedHashTable.hpp"
#include "indexing/indexing.hpp"

#include <vector>
#include <tuple>

namespace ouroboros
{
    class NSCHTDirectTFIDFIndex: BaseInformationIndex
    {
        struct tfidf_data_t {
            char *term;
            long fileIdx;
            long frequency;
        };

        private:
            NSMemBytesStore* store;
            ChainedHashTable<char*, long, charptr_hash, charptr_equal>* map;
            std::vector<tfidf_data_t>* terms;
        public:
            NSCHTDirectTFIDFIndex(long pageSize, long numBuckets);
            virtual ~NSCHTDirectTFIDFIndex();

            virtual long insert(const char *term, long fileIdx);
            virtual long lookup(const char *term);
            virtual const std::tuple<char*, long, long> reverseLookup(long idx);
    };
}

#endif