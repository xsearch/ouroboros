#ifndef OUROBOROS_PAGED_SWISS_MAP_TFIDF_INDEX_H
#define OUROBOROS_PAGED_SWISS_MAP_TFIDF_INDEX_H

#ifdef GOOGLE_SWISS_LIB

#include "indexing/information/BaseTFIDFIndex.hpp"
#include "indexing/information/TFIndexEntry.hpp"
#include "corestructures/HugePagedBytesStore.hpp"
#include "corestructures/PagedVersatileIndex.hpp"
#include "indexing/indexing.hpp"

#include <absl/container/flat_hash_map.h>

namespace ouroboros
{
    class PagedSwissMapTFIDFIndex: public BaseTFIDFIndex
    {
        private:
            std::shared_ptr<HugePagedBytesStore> bytesStore;
            absl::flat_hash_map<const char*, PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>*, cstr_hash, cstr_equal> map;
            long numTerms;
        public:
            explicit PagedSwissMapTFIDFIndex(std::shared_ptr<HugePagedBytesStore> bytesStore) : 
                bytesStore(bytesStore), map(), numTerms(0) { }
            virtual ~PagedSwissMapTFIDFIndex() = default;

            virtual void insert(const char *term, long fileIdx);
            virtual std::shared_ptr<TFIndexResult> lookup(const char *term);
            virtual long getNumTerms();
    };
}

#endif

#endif