#ifndef OUROBOROS_STD_MAP_TFIDF_INDEX_H
#define OUROBOROS_STD_MAP_TFIDF_INDEX_H

#include "indexing/information/BaseTFIDFIndex.hpp"
#include "indexing/information/TFIndexEntry.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include "corestructures/PagedVersatileIndex.hpp"
#include "indexing/indexing.hpp"
#include <unordered_map>
#include <deque>

namespace ouroboros
{
    class StdMapTFIDFIndex: public BaseTFIDFIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> termStore;
            std::unordered_map<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>*> index;
            long numTerms;
        public:
            explicit StdMapTFIDFIndex(std::shared_ptr<BasePagedStringStore> termStore) : 
                termStore(termStore), map(), index(), numTerms(0) { }
            virtual ~StdMapTFIDFIndex();

            virtual long insert(const char *term, long fileIdx);
            virtual long lookup(const char *term);
            virtual std::shared_ptr<TFIndexResult> reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif