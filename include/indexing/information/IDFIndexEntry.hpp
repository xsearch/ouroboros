#ifndef OUROBOROS_IDF_INDEX_ENTRY_H
#define OUROBOROS_IDF_INDEX_ENTRY_H

namespace ouroboros
{
    struct IDFIndexEntry
    {
        long fileIdx;
        long fileFrequency;

        IDFIndexEntry() : fileIdx(-1), fileFrequency(-1) { }
        IDFIndexEntry(long fileIdx, long fileFrequency) : fileIdx(fileIdx), fileFrequency(fileFrequency) { }
        ~IDFIndexEntry() = default;
    };
}

#endif