#ifndef OUROBOROS_PAGED_CHAINED_TABLE_TFIDF_INDEX_H
#define OUROBOROS_PAGED_CHAINED_TABLE_TFIDF_INDEX_H

#include "indexing/information/BaseTFIDFIndex.hpp"
#include "indexing/information/TFIndexEntry.hpp"
#include "corestructures/HugePagedBytesStore.hpp"
#include "corestructures/PagedVersatileIndex.hpp"
#include "corestructures/ChainedHashTable.hpp"
#include "indexing/indexing.hpp"

namespace ouroboros
{
    class PagedChainedTableTFIDFIndex: public BaseTFIDFIndex
    {
        private:
            std::shared_ptr<HugePagedBytesStore> bytesStore;
            ChainedHashTable<const char*, PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>*, cstr_hash, cstr_equal> map;
            long numTerms;
        public:
            explicit PagedChainedTableTFIDFIndex(std::shared_ptr<HugePagedBytesStore> bytesStore, 
                unsigned long numBuckets) : bytesStore(bytesStore), map(numBuckets), numTerms(0) { }
            virtual ~PagedChainedTableTFIDFIndex() = default;

            virtual void insert(const char *term, long fileIdx);
            virtual std::shared_ptr<TFIndexResult> lookup(const char *term);
            virtual long getNumTerms();
    };
}

#endif