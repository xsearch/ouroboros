#ifndef OUROBOROS_BASEFREQUENCYINDEX_H
#define OUROBOROS_BASEFREQUENCYINDEX_H

#include "indexing/information/BaseInformationIndex.hpp"

namespace ouroboros
{
    class BaseFrequencyIndex: public BaseInformationIndex
    {
        public:
            BaseFrequencyIndex() : BaseInformationIndex(InformationIndexType::FREQUENCY) { }
            virtual ~BaseFrequencyIndex() = default;

            virtual void insert(long idx) = 0;
            virtual void remove(long idx) = 0;
            virtual long lookup(long idx) = 0;
            virtual void initialize(long idx, long value) = 0;
            virtual void increment(long idx, long value) = 0;
            virtual void decrement(long idx, long value) = 0;
            virtual long getSize() = 0;
    };
}

#endif