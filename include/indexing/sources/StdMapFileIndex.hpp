#ifndef OUROBOROS_STD_MAP_FILE_INDEX_H
#define OUROBOROS_STD_MAP_FILE_INDEX_H

#include "indexing/sources/BaseFileIndex.hpp"
#include "indexing/indexing.hpp"
#include <unordered_map>
#include <deque>

namespace ouroboros
{
    class StdMapFileIndex: public BaseFileIndex
    {
        private:
            std::unordered_map<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<const char*> paths;
            long numFiles;
        public:
            explicit StdMapFileIndex() : map(), paths(), numFiles(0) { }
            virtual ~StdMapFileIndex() = default;

            virtual long insert(const char *path);
            virtual long lookup(const char *path);
            virtual const char* reverseLookup(long idx);
    };
}

#endif