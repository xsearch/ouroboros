#ifndef OUROBOROS_BASEFILEINDEX_H
#define OUROBOROS_BASEFILEINDEX_H

#include "indexing/sources/BaseSourceIndex.hpp"

namespace ouroboros
{
    class BaseFileIndex: public BaseSourceIndex
    {
        public:
            BaseFileIndex() : BaseSourceIndex(SourceIndexType::FILE) { }
            virtual ~BaseFileIndex() = default;

            virtual long insert(const char *path) = 0;
            virtual long lookup(const char *path) = 0;
            virtual const char* reverseLookup(long idx) = 0;
    };
}

#endif