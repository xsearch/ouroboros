#ifndef OUROBOROS_BASESOURCEINDEX_H
#define OUROBOROS_BASESOURCEINDEX_H

namespace ouroboros
{
    enum class SourceIndexType {FILE};

    class BaseSourceIndex
    {
        protected:
            SourceIndexType type;
        public:
            BaseSourceIndex(SourceIndexType type) : type(type) { }
            virtual ~BaseSourceIndex() = default;
            SourceIndexType getType() { return type; }
    };
}

#endif