#ifndef OUROBOROS_NS_CHT_HASHTABLE_FILEINDEX_H
#define OUROBOROS_NS_CHT_HASHTABLE_FILEINDEX_H

#include "indexing/sources/BaseFileIndex.hpp"
#include "corestructures/NSMemBytesStore.hpp"
#include "corestructures/ChainedHashTable.hpp"

#include <vector>

namespace ouroboros {
    class NSCHTFileIndex: BaseFileIndex
    {
        private:
            NSMemBytesStore* store;
            ChainedHashTable<char*, long>* map;
            std::vector<char*>* files;
        public:
            NSCHTFileIndex(long pageSize, long numBuckets);
            virtual ~NSCHTFileIndex();

            virtual long insert(const char *path);
            virtual long lookup(const char *path);
            virtual const char* reverseLookup(long idx);
    };
}

#endif