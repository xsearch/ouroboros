#ifndef OUROBOROS_STDAPPEND_FILEINDEX_H
#define OUROBOROS_STDAPPEND_FILEINDEX_H

#include "indexing/sources/BaseFileIndex.hpp"

#include <vector>
#include <string>

namespace ouroboros
{
    class StdAppendFileIndex: public BaseFileIndex
    {
        private:
            std::vector<std::string*>* files;
        public:
            StdAppendFileIndex();
            virtual ~StdAppendFileIndex();

            virtual long insert(const char *path);
            virtual long lookup(const char *path);
            virtual const char* reverseLookup(long idx);
    };
}

#endif