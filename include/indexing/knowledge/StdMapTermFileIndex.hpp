#ifndef OUROBOROS_STD_MAP_TERM_FILE_INDEX_H
#define OUROBOROS_STD_MAP_TERM_FILE_INDEX_H

#include "indexing/knowledge/BaseTermFileIndex.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include "indexing/indexing.hpp"
#include <unordered_map>
#include <deque>

namespace ouroboros
{
    class StdMapTermFileIndex: public BaseTermFileIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> store;
            std::unordered_map<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<std::shared_ptr<TermFileIndexEntry>> index;
            long numTerms;
        public:
            explicit StdMapTermFileIndex(std::shared_ptr<BasePagedStringStore> store) : 
                store(store), map(), index(), numTerms(0) { }
            virtual ~StdMapTermFileIndex() = default;

            virtual long insert(const char *term, long fileIdx);
            virtual void remove(const char *term, long fileIdx);
            virtual long lookup(const char *term);
            virtual std::shared_ptr<TermFileIndexEntry> reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif