#ifndef OUROBOROS_BASETERMFILERELATION_H
#define OUROBOROS_BASETERMFILERELATION_H

#include <vector>
#include <tuple>

#include "indexing/knowledge/BaseKnowledgeIndex.hpp"

namespace ouroboros
{
    class BaseTermFileRelation: public BaseKnowledgeIndex
    {
        public:
            BaseTermFileRelation() : BaseKnowledgeIndex(KnowledgeIndexType::TERM_FILE_RELATION) { }
            virtual ~BaseTermFileRelation() = default;

            virtual long insert(long termIdx, long fileIdx) = 0;
            virtual void remove(long termIdx, long fileIdx) = 0;
            virtual std::vector<std::tuple<long, long>>* lookup(long termIdx) = 0;
            virtual void reverseRemove(long idx) = 0;
            virtual std::tuple<long, long> reverseLookup(long idx) = 0;
            virtual long getSize() = 0;
    };
}

#endif