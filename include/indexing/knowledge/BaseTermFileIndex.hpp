#ifndef OUROBOROS_BASE_TERM_FILE_INDEX_H
#define OUROBOROS_BASE_TERM_FILE_INDEX_H

#include "indexing/knowledge/BaseKnowledgeIndex.hpp"
#include "indexing/knowledge/TermFileIndexEntry.hpp"
#include <memory>

namespace ouroboros
{
    class BaseTermFileIndex: public BaseKnowledgeIndex
    {
        public:
            explicit BaseTermFileIndex() : BaseKnowledgeIndex(KnowledgeIndexType::TERM_FILE_INDEX) { }
            virtual ~BaseTermFileIndex() = default;

            virtual long insert(const char *term, long fileIdx) = 0;
            virtual void remove(const char *term, long fileIdx) = 0;
            virtual long lookup(const char *term) = 0;
            virtual std::shared_ptr<TermFileIndexEntry> reverseLookup(long idx) = 0;
            virtual long getNumTerms() = 0;
    };
}

#endif