#ifndef OUROBOROS_TERM_FILE_INDEX_ENTRY_H
#define OUROBOROS_TERM_FILE_INDEX_ENTRY_H

#include <list>

namespace ouroboros
{
    struct TermFileIndexEntry
    {
        const char* term;
        std::list<long> files;

        TermFileIndexEntry(const char* term) : term(term), files() { }
        ~TermFileIndexEntry() = default;

        TermFileIndexEntry(const TermFileIndexEntry& other) = default;
        TermFileIndexEntry& operator=(const TermFileIndexEntry& other) = default;
        TermFileIndexEntry(TermFileIndexEntry&& other) = default;
        TermFileIndexEntry& operator=(TermFileIndexEntry&& other) = default;
    };
}

#endif