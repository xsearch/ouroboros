#ifndef OUROBOROS_DENSE_MAP_TERM_FILE_INDEX_H
#define OUROBOROS_DENSE_MAP_TERM_FILE_INDEX_H

#ifdef GOOGLE_DENSE_LIB

#include "indexing/knowledge/BaseTermFileIndex.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include "indexing/indexing.hpp"
#include <deque>

#include <sparsehash/dense_hash_map>

namespace ouroboros
{
    class DenseMapTermFileIndex: public BaseTermFileIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> store;
            google::dense_hash_map<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<std::shared_ptr<TermFileIndexEntry>> index;
            long numTerms;
        public:
            explicit DenseMapTermFileIndex(std::shared_ptr<BasePagedStringStore> store) : 
                store(store), map(), index(), numTerms(0) { map.set_empty_key(NULL); }
            virtual ~DenseMapTermFileIndex() = default;

            virtual long insert(const char *term, long fileIdx);
            virtual void remove(const char *term, long fileIdx);
            virtual long lookup(const char *term);
            virtual std::shared_ptr<TermFileIndexEntry> reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif

#endif