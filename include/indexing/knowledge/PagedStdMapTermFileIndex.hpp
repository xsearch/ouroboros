#ifndef OUROBOROS_PAGED_STD_MAP_TERM_FILE_INDEX_H
#define OUROBOROS_PAGED_STD_MAP_TERM_FILE_INDEX_H

#include "indexing/knowledge/BaseTermFileIndex.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include "corestructures/PagedVersatileStore.hpp"
#include "corestructures/PagedVersatileIndex.hpp"
#include "corestructures/PagedLinkedElement.hpp"
#include "indexing/indexing.hpp"
#include <unordered_map>
#include <deque>

namespace ouroboros
{
    class PagedStdMapTermFileIndex: public BaseTermFileIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> termStore;
            std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<char*, long>>> indexStore;
            std::shared_ptr<PagedVersatileStore<PagedLinkedElement<long>>> listStore;
            std::unordered_map<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<PagedVersatileIndex<char*, long>*> index;
            long numTerms;
        public:
            explicit PagedStdMapTermFileIndex(std::shared_ptr<BasePagedStringStore> termStore,
                std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<char*, long>>> idxStor,
                std::shared_ptr<PagedVersatileStore<PagedLinkedElement<long>>> lstStor) : 
                termStore(termStore), indexStore(idxStor), listStore(lstStor), map(), index(), numTerms(0) { }
            virtual ~PagedStdMapTermFileIndex() = default;

            virtual long insert(const char *term, long fileIdx);
            virtual void remove(const char *term, long fileIdx);
            virtual long lookup(const char *term);
            virtual std::shared_ptr<TermFileIndexEntry> reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif