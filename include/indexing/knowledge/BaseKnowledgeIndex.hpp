#ifndef OUROBOROS_BASE_KNOWLEDGE_INDEX_H
#define OUROBOROS_BASE_KNOWLEDGE_INDEX_H

namespace ouroboros
{
    enum class KnowledgeIndexType {BASE, TERM_FILE_RELATION, TERM_FILE_INDEX};

    class BaseKnowledgeIndex
    {
        protected:
            KnowledgeIndexType type;
        public:
            explicit BaseKnowledgeIndex(KnowledgeIndexType type) : type(type) { }
            virtual ~BaseKnowledgeIndex() = default;

            virtual KnowledgeIndexType getType() { return type; }
    };
}

#endif