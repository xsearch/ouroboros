#ifndef OUROBOROS_WFILE_READER_DRIVER_H
#define OUROBOROS_WFILE_READER_DRIVER_H

#include "readerdrivers/BaseReaderDriver.hpp"
#include "readerdrivers/FileDataBlock.hpp"

#include <string>
#include <atomic>
#include <exception>

namespace ouroboros
{
    class WaveFileReaderException: public std::exception
    {
        private:
            std::string msg;
        public:
            WaveFileReaderException(const std::string s) : msg(s) { }
            virtual ~WaveFileReaderException() = default;

            virtual const char* what() const throw() { return msg.c_str(); }
    };

    class WaveFileReaderDriver: public BaseReaderDriver
    {
        private:
            static const int NONE = -1;
            static const long DISK_ALIGNMENT = 512;

            char* filepath;
            char delimDict[256];
            char delim;
            int fd;
            int blockPos;
            int blockSize;
            int blockAddOn;
        public:
            explicit WaveFileReaderDriver(char *filepath, int blockSize, int blockAddOn, char* delims);
            virtual ~WaveFileReaderDriver();

            WaveFileReaderDriver(const WaveFileReaderDriver& other);
            WaveFileReaderDriver& operator=(const WaveFileReaderDriver& other);
            WaveFileReaderDriver(WaveFileReaderDriver&& other);
            WaveFileReaderDriver& operator=(WaveFileReaderDriver&& other);

            virtual void open();
            virtual void close();
            virtual void readNextBlock(BaseDataBlock *dataBlock);
            virtual void readNextBlock(FileDataBlock *dataBlock);
            virtual void readBlockAt(FileDataBlock *dataBlock, int blockNumber);
    };
}

#endif