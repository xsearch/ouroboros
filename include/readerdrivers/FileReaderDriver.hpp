#ifndef OUROBOROS_FILE_STORAGE_DRIVER_H
#define OUROBOROS_FILE_STORAGE_DRIVER_H

#include "readerdrivers/BaseReaderDriver.hpp"
#include "readerdrivers/FileDataBlock.hpp"

#include <exception>
#include <string>
#include <cstdio>

namespace ouroboros
{
    class FileReaderException: public std::exception
    {
        private:
            std::string msg;
        public:
            FileReaderException(const std::string s) : msg(s) { }
            virtual ~FileReaderException() = default;

            virtual const char* what() const throw() { return msg.c_str(); }
    };

    class FileReaderDriver: public BaseReaderDriver
    {
        private:
            char *filepath;
            FILE *filep;
            int position;
            int blockSize;
        public:
            explicit FileReaderDriver(char *filepath, int blockSize);
            virtual ~FileReaderDriver() = default;

            // TO-DO implement copy and move constructors and assignment operators
            
            virtual void open();
            virtual void close();
            virtual void readNextBlock(BaseDataBlock *dataBlock);
            virtual void readNextBlock(FileDataBlock *dataBlock);
            virtual void readBlockAt(FileDataBlock *dataBlock, int position);
            
    };
}

#endif