#ifndef OUROBOROS_PFILEREADERDRIVER_H
#define OUROBOROS_PFILEREADERDRIVER_H

#include "readerdrivers/BaseReaderDriver.hpp"
#include "readerdrivers/FileDataBlock.hpp"

#include <string>
#include <atomic>
#include <exception>

namespace ouroboros
{
    class PFileReaderException: public std::exception
    {
        private:
            std::string msg;
        public:
            PFileReaderException(const std::string s) : msg(s) { }
            virtual ~PFileReaderException() = default;

            virtual const char* what() const throw() { return msg.c_str(); }
    };

    class PFileReaderDriver: public BaseReaderDriver
    {
        private:
            char *filepath;
            int fd;
            std::atomic<int> position;
            int blockSize;
        public:
            explicit PFileReaderDriver(char *filepath, int blockSize);
            virtual ~PFileReaderDriver() = default;

            // TO-DO implement copy and move constructors and assignment operators

            virtual void open();
            virtual void close();
            virtual void readNextBlock(BaseDataBlock *dataBlock);
            virtual void readNextBlock(FileDataBlock *dataBlock);
            virtual void readBlockAt(FileDataBlock *dataBlock, int position);
            
    };
}

#endif