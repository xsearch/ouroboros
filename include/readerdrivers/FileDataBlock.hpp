#ifndef OUROBOROS_FILEDATABLOCK_H
#define OUROBOROS_FILEDATABLOCK_H

#include "readerdrivers/BaseDataBlock.hpp"

namespace ouroboros
{
    class FileDataBlock: public BaseDataBlock
    {
        public:
            char *buffer;
            int position;
            int blockSize;
            int length;
            char *filepath;
            long fileIdx;
            
            FileDataBlock();
            ~FileDataBlock() = default;
    };
}

#endif