#ifndef OUROBOROS_STRINGHASHMAP_H
#define OUROBOROS_STRINGHASHMAP_H

#include "corestructures/CapacityArray.hpp"

#include <mutex>
#include <atomic>
#include <tuple>
#include <cstring>

namespace ouroboros
{
    class StringHashMap
    {
        protected:
            CapacityArray<char*> **buckets;
            std::mutex *mtxs;
            unsigned long numBuckets;
            long minCollisions;
            long maxCollisions;
            std::atomic<long> numElements;
        public:
            StringHashMap(unsigned long numBuckets, long minCollisions, long maxCollisions);
            virtual ~StringHashMap();

            virtual unsigned long hash(const char *str);

            virtual std::tuple<long, bool> insert(char *str);
            virtual std::tuple<long, bool> lookup(char *str);
            virtual long getNumElements();
    };
}

#endif