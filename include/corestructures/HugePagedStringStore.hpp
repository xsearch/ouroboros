#ifndef OUROBOROS_HUGE_PAGED_STRING_STORE_H
#define OUROBOROS_HUGE_PAGED_STRING_STORE_H

#include "corestructures/BasePagedStringStore.hpp"

#include <vector>

namespace ouroboros
{
    class HugePagedStringStore: public BasePagedStringStore
    {
        private:
            static const long SMALL_PAGE_ALIGNMENT = 4096;
            static const long LARGE_PAGE_ALIGNMENT = 2097152;
            static const long HUGE_PAGE_ALIGNMENT = 1073741824;

            std::vector<char*> pages;
            long pageSize;
            long pagePosition;
            long pageAlignment;
            char* pageTail;

            virtual void expand();
        public:
            explicit HugePagedStringStore(long pageSize);
            virtual ~HugePagedStringStore();

            // copy-constructor
            HugePagedStringStore(const HugePagedStringStore& other);
            // copy-assignment
            HugePagedStringStore& operator=(const HugePagedStringStore& other);
            // move-constructor
            HugePagedStringStore(HugePagedStringStore&& other);
            // move-assignment
            HugePagedStringStore& operator=(HugePagedStringStore&& other);
            
            /* method used to store a string in the paged string store;
             * returns the address to where the string was stored */
            virtual char* store(const char* str);
    };
}

#endif