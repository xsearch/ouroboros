#ifndef OUROBOROS_CAPACITYARRAY_H
#define OUROBOROS_CAPACITYARRAY_H

#include <mutex>
#include <atomic>
#include <type_traits>

#define MAX_CAPACITY_REACHED -2

namespace ouroboros
{
    template <class T>
    class CapacityArray
    {
        protected:
            T **vect;
            std::atomic<long> pos;
            std::mutex mtx;
            long capacity;
            long capacityStep;
            long capacityMax;
        public:
            CapacityArray(long capacityStep, long capacityMax);
            virtual ~CapacityArray();

            virtual void expand();
            virtual long push_back(T elem);
            virtual T& operator[](long idx);
            virtual long getCapacity();
            virtual long getLength();
    };

    template <class T>
    CapacityArray<T>::CapacityArray(long capacityStep, long capacityMax)
    {
        this->capacityStep = capacityStep;
        this->capacityMax = capacityMax;
        vect = new T*[capacityMax / capacityStep]{NULL};
        capacity = 0;
        expand();
        pos.store(0);
    }

    template <class T>
    CapacityArray<T>::~CapacityArray()
    {
        for (auto i = 0; i < (capacity / capacityStep); i++) {
            delete[] vect[i];
        }
        delete[] vect;
    }

    template <class T>
    void CapacityArray<T>::expand()
    {
        long i = capacity / capacityStep;
        if (std::is_pointer<T>::value) {
            vect[i] = new T[capacityStep]{NULL};    
        } else {
            vect[i] = new T[capacityStep]{0};
        }
        capacity += capacityStep;
    }

    template <class T>
    long CapacityArray<T>::push_back(T elem)
    {
        long idx = pos.fetch_add(1, std::memory_order_seq_cst);
        long i = idx / capacityStep;
        long j = idx % capacityStep;

        if (idx >= capacity) {
            std::lock_guard<std::mutex> lock(mtx);
            if (idx >= capacity) {
                if ((capacity + capacityStep) <= capacityMax) {
                    expand();
                }
            }
        }

        if (idx >= capacityMax) {
            idx = MAX_CAPACITY_REACHED;
            pos--;
        } else {
            vect[i][j] = elem;
        }

        return idx;
    }

    template <class T>
    T& CapacityArray<T>::operator[](long idx)
    {
        long i = idx / capacityStep;
        long j = idx % capacityStep;

        return vect[i][j];
    }

    template <class T>
    inline long CapacityArray<T>::getCapacity()
    {
        return capacity;
    }

    template <class T>
    inline long CapacityArray<T>::getLength()
    {
        long length = pos.load();
        return (length < capacityMax) ? length : capacityMax;
    }
}

#endif