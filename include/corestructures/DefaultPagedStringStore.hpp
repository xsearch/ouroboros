#ifndef OUROBOROS_DEFAULT_PAGED_STRING_STORE_H
#define OUROBOROS_DEFAULT_PAGED_STRING_STORE_H

#include "corestructures/BasePagedStringStore.hpp"

#include <vector>

namespace ouroboros
{
    class DefaultPagedStringStore: public BasePagedStringStore
    {
        private:
            std::vector<char*> pages;
            long pageSize;
            long pagePosition;
            char* pageTail;

            virtual void expand();
        public:
            // constructor and destructor
            explicit DefaultPagedStringStore(long pageSize);
            virtual ~DefaultPagedStringStore();

            // copy-constructor
            DefaultPagedStringStore(const DefaultPagedStringStore& other);
            // copy-assignment
            DefaultPagedStringStore& operator=(const DefaultPagedStringStore& other);
            // move-constructor
            DefaultPagedStringStore(DefaultPagedStringStore&& other);
            // move-assignment
            DefaultPagedStringStore& operator=(DefaultPagedStringStore&& other);

            /* method used to store a string in the paged string store;
             * returns the address to where the string was stored */
            virtual char* store(const char* str);
    };
}

#endif