#ifndef OUROBOROS_SYNQUEUE_H
#define OUROBOROS_SYNQUEUE_H

#include <vector>
#include <mutex>
#include <condition_variable>

namespace ouroboros
{
    template<class T>
    class SyncQueue
    {
        protected:
            int maxSize;
            int size;
            int head;
            int tail;
            T *stor;
            std::mutex mtx;
            std::condition_variable cvPush;
            std::condition_variable cvPop;
        public:
            SyncQueue(int maxSize);
            ~SyncQueue();
            void push(T elem);
            T pop();
    };

    template<class T>
    SyncQueue<T>::SyncQueue(int maxSize) {
        this->maxSize = maxSize;
        size = 0;
        head = 0;
        tail = 0;
        
        stor = new T[maxSize];
        for (auto i = 0; i < maxSize; i++) {
            stor[i] = nullptr;
        }
    }

    template<class T>
    SyncQueue<T>::~SyncQueue() {
        delete[] stor;
    }

    template<class T>
    void SyncQueue<T>::push(T elem) {
        std::unique_lock<std::mutex> lck(mtx);
        int lSize = size;

        while (size >= maxSize) {
            cvPop.wait(lck);
        }
        
        stor[tail] = elem;
        tail = (tail + 1) % maxSize;
        size++;

        lck.unlock();
        cvPush.notify_all();
    }

    template<class T>
    T SyncQueue<T>::pop() {
        std::unique_lock<std::mutex> lck(mtx);
        int lSize = size;
        T rc;
        
        while (size <= 0) {
            cvPush.wait(lck);
        }

        rc = stor[head];
        head = (head + 1) % maxSize;
        size--;
        
        lck.unlock();
        cvPop.notify_all();

        return rc;
    }
}

#endif