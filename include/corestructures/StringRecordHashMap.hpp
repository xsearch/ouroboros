#ifndef OUROBOROS_STRINGRECORDHASHMAP_H
#define OUROBOROS_STRINGRECORDHASHMAP_H

#include "corestructures/RecordManager.hpp"

#include <tuple>
#include <cstring>

#include "corestructures/RecordManager.hpp"

#define OUROBOROS_DEFAULT_STRING_RECORD_HASHMAP_NUM_BUCKETS 65536

namespace ouroboros
{
    class StringRecordHashMap
    {
        protected:
            RecordManager<char*>* recordManager;
            ouroboros_record<char*>** buckets;
            unsigned long numBuckets;
            long numElements;

            virtual std::tuple<long, bool> appendToBucket(ouroboros_record<char*>*& bucket, char* str);
            virtual std::tuple<long, bool> lookupInBucket(ouroboros_record<char*>* bucket, char* str);
        public:
            StringRecordHashMap(RecordManager<char*>* recordManager);
            StringRecordHashMap(RecordManager<char*>* recordManager, unsigned long numBuckets);
            virtual ~StringRecordHashMap();

            virtual unsigned long hash(const char* str);

            virtual std::tuple<long, bool> insert(char* str);
            virtual std::tuple<long, bool> lookup(char* str);
            virtual long getNumElements();
    };
}

#endif