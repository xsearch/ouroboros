#ifndef OUROBOROS_CHAINED_HASHTABLE_H
#define OUROBOROS_CHAINED_HASHTABLE_H

#include <functional>

namespace ouroboros
{
    template<class K, class V, class Hash = std::hash<K>, class KeyEqual = std::equal_to<K>>
    class ChainedHashTable
    {
        private:
            struct ChainedHashTableEntry
            {
                unsigned long hKey;
                ChainedHashTableEntry* next = NULL;
                K key;
                V value;
            };

            struct ChainedHashTableBucket
            {
                unsigned long hKey;
                ChainedHashTableEntry* next = NULL;
                K key;
                V value;
                bool used = false;
            };

            ChainedHashTableBucket* buckets;
            unsigned long numBuckets;
            Hash hashFn;
            KeyEqual equalFn;
        public:
            explicit ChainedHashTable(unsigned long numBuckets = 4026031);
            virtual ~ChainedHashTable();

            ChainedHashTable(const ChainedHashTable& other);
            ChainedHashTable& operator=(const ChainedHashTable& other);
            ChainedHashTable(ChainedHashTable&& other);
            ChainedHashTable& operator=(ChainedHashTable&& other);

            virtual void insert(K key, V value);
            virtual bool lookup(K key, V& value);

            static size_t getBucketSizeInBytes();
    };

    template<class K, class V, class Hash, class KeyEqual>
    ChainedHashTable<K, V, Hash, KeyEqual>::ChainedHashTable(unsigned long numBuckets)
    {
        this->numBuckets = numBuckets;
        buckets = new ChainedHashTableBucket[this->numBuckets]{};
    }

    template<class K, class V, class Hash, class KeyEqual>
    ChainedHashTable<K, V, Hash, KeyEqual>::~ChainedHashTable()
    {
        ChainedHashTableEntry *bkt, *prevBkt;
        
        for (unsigned long i = 0; i < numBuckets; i++) {
            if (buckets[i].next != NULL) {
                bkt = buckets[i].next;
                while (bkt != NULL) {
                    prevBkt = bkt;
                    bkt = bkt->next;
                    delete prevBkt;
                }
            }
        }
        delete[] buckets;
    }

    template<class K, class V, class Hash, class KeyEqual>
    ChainedHashTable<K, V, Hash, KeyEqual>::ChainedHashTable(const ChainedHashTable<K, V, Hash, KeyEqual>& other)
    {
        ChainedHashTableEntry *bkt, *otherBkt;

        numBuckets = other.numBuckets;
        buckets = new ChainedHashTableBucket[numBuckets]{};

        for (unsigned long i = 0; i < numBuckets; i++) {
            if (other.buckets[i].used) {
                buckets[i].hKey = other.buckets[i].hKey;
                buckets[i].key = other.buckets[i].key;
                buckets[i].value = other.buckets[i].value;
                buckets[i].used = other.buckets[i].used;

                otherBkt = other.buckets[i].next;
                if (otherBkt != NULL) {
                    buckets[i].next = new ChainedHashTableEntry{};
                    bkt = buckets[i].next;
                    bkt->hKey = otherBkt->hKey;
                    bkt->key = otherBkt->key;
                    bkt->value = otherBkt->value;
                    otherBkt = otherBkt->next;
                    while (otherBkt != NULL) {
                        bkt->next = new ChainedHashTableEntry{};
                        bkt = bkt->next;
                        bkt->hKey = otherBkt->hKey;
                        bkt->key = otherBkt->key;
                        bkt->value = otherBkt->value;
                        otherBkt = otherBkt->next;
                    }
                    bkt->next = NULL;
                }
            }
        }
    }

    template<class K, class V, class Hash, class KeyEqual>
    ChainedHashTable<K, V, Hash, KeyEqual>& 
    ChainedHashTable<K, V, Hash, KeyEqual>::operator=(const ChainedHashTable<K, V, Hash, KeyEqual>& other)
    {
        ChainedHashTableEntry *bkt, *prevBkt, *otherBkt;

        if (this == &other) {
            return *this;
        }

        for (unsigned long i = 0; i < numBuckets; i++) {
            if (buckets[i].next != NULL) {
                bkt = buckets[i].next;
                while (bkt != NULL) {
                    prevBkt = bkt;
                    bkt = bkt->next;
                    delete prevBkt;
                }
            }
        }
        delete[] buckets;

        numBuckets = other.numBuckets;
        buckets = new ChainedHashTableBucket[numBuckets]{};

        for (unsigned long i = 0; i < numBuckets; i++) {
            if (other.buckets[i].used) {
                buckets[i].hKey = other.buckets[i].hKey;
                buckets[i].key = other.buckets[i].key;
                buckets[i].value = other.buckets[i].value;
                buckets[i].used = other.buckets[i].used;

                otherBkt = other.buckets[i].next;
                if (otherBkt != NULL) {
                    buckets[i].next = new ChainedHashTableEntry{};
                    bkt = buckets[i].next;
                    bkt->hKey = otherBkt->hKey;
                    bkt->key = otherBkt->key;
                    bkt->value = otherBkt->value;
                    otherBkt = otherBkt->next;
                    while (otherBkt != NULL) {
                        bkt->next = new ChainedHashTableEntry{};
                        bkt = bkt->next;
                        bkt->hKey = otherBkt->hKey;
                        bkt->key = otherBkt->key;
                        bkt->value = otherBkt->value;
                        otherBkt = otherBkt->next;
                    }
                    bkt->next = NULL;
                }
            }
        }

        return *this;
    }

    template<class K, class V, class Hash, class KeyEqual>
    ChainedHashTable<K, V, Hash, KeyEqual>::ChainedHashTable(ChainedHashTable<K, V, Hash, KeyEqual>&& other)
    {
        numBuckets = other.numBuckets;
        buckets = other.buckets;

        other.numBuckets = 0;
        other.buckets = NULL;
    }

    template<class K, class V, class Hash, class KeyEqual>
    ChainedHashTable<K, V, Hash, KeyEqual>& 
    ChainedHashTable<K, V, Hash, KeyEqual>::operator=(ChainedHashTable<K, V, Hash, KeyEqual>&& other)
    {
        ChainedHashTableEntry *bkt, *prevBkt;

        if (this == &other) {
            return *this;
        }

        for (unsigned long i = 0; i < numBuckets; i++) {
            if (buckets[i].next != NULL) {
                bkt = buckets[i].next;
                while (bkt != NULL) {
                    prevBkt = bkt;
                    bkt = bkt->next;
                    delete prevBkt;
                }
            }
        }
        delete[] buckets;

        numBuckets = other.numBuckets;
        buckets = other.buckets;

        other.numBuckets = 0;
        other.buckets = NULL;

        return *this;
    }

    template<class K, class V, class Hash, class KeyEqual>
    void ChainedHashTable<K, V, Hash, KeyEqual>::insert(K key, V value)
    {
        unsigned long hKey;
        unsigned long bktid;
        ChainedHashTableEntry* bkt;

        hKey = hashFn(key);
        bktid = hKey % numBuckets;

        if (!buckets[bktid].used) {
            buckets[bktid].hKey = hKey;
            buckets[bktid].key = key;
            buckets[bktid].value = value;
            buckets[bktid].used = true;
        } else {
            if (buckets[bktid].next == NULL) {
                buckets[bktid].next = new ChainedHashTableEntry{};
                bkt = buckets[bktid].next;
            } else {
                bkt = buckets[bktid].next;
                while (bkt->next != NULL) {
                    bkt = bkt->next;
                }
                bkt->next = new ChainedHashTableEntry{};
                bkt = bkt->next;
            }
            bkt->hKey = hKey;
            bkt->key = key;
            bkt->value = value;
        }
    }

    template<class K, class V, class Hash, class KeyEqual>
    bool ChainedHashTable<K, V, Hash, KeyEqual>::lookup(K key, V& value)
    {
        unsigned long hKey;
        unsigned long bktid;
        ChainedHashTableEntry* bkt;

        hKey = hashFn(key);
        bktid = hKey % numBuckets;

        if (buckets[bktid].used) {
            if (buckets[bktid].hKey == hKey and equalFn(buckets[bktid].key, key)) {
                value = buckets[bktid].value;
                return true;
            }
            bkt = buckets[bktid].next;
            while (bkt != NULL) {
                if (bkt->hKey == hKey and equalFn(bkt->key, key)) {
                    value = bkt->value;
                    return true;
                }
                bkt = bkt->next;
            }
        }

        return false;
    }

    template<class K, class V, class Hash, class KeyEqual>
    size_t ChainedHashTable<K, V, Hash, KeyEqual>::getBucketSizeInBytes()
    {
        return sizeof(ChainedHashTableBucket);
    }
}

#endif