#ifndef OUROBOROS_PAGED_VERSATILE_INDEX_H
#define OUROBOROS_PAGED_VERSATILE_INDEX_H

#include "corestructures/PagedLinkedElement.hpp"

namespace ouroboros
{
    template<class T, class V>
    struct PagedVersatileIndex
    {
        T element;
        PagedLinkedElement<V>* head;
        PagedLinkedElement<V>* tail;

        PagedVersatileIndex(T element) : element(element), head(NULL), tail(NULL) { }
        ~PagedVersatileIndex() = default;

        PagedVersatileIndex(const PagedVersatileIndex& other) = default;
        PagedVersatileIndex& operator=(const PagedVersatileIndex& other) = default;
        PagedVersatileIndex(PagedVersatileIndex&& other) = default;
        PagedVersatileIndex& operator=(PagedVersatileIndex&& other) = default;
    };
}

#endif