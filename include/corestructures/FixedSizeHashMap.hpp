#ifndef OUROBOROS_FIXEDSIZE_HASHMAP_H
#define OUROBOROS_FIXEDSIZE_HASHMAP_H

#include <functional>

namespace ouroboros
{
    template<class K, class V>
    class FixedSizeHashMap
    {
        private:
            V* buckets;
            unsigned long numBuckets;
            std::hash<K> hashFn;
        public:
            FixedSizeHashMap(unsigned long numBuckets);
            ~FixedSizeHashMap();

            void insert(K key, V value);
            V lookup(K key);
    };

    template<class K, class V>
    FixedSizeHashMap<K, V>::FixedSizeHashMap(unsigned long numBuckets)
    {
        this->numBuckets = numBuckets;
        buckets = new V[numBuckets]{};
    }

    template<class K, class V>
    FixedSizeHashMap<K, V>::~FixedSizeHashMap()
    {
        delete[] buckets;
    }

    template<class K, class V>
    void FixedSizeHashMap<K, V>::insert(K key, V value)
    {
        unsigned long hval;
        unsigned long bktid;

        hval = hashFn(key);
        bktid = hval % numBuckets;
        buckets[bktid] = value;
    }

    template<class K, class V>
    V FixedSizeHashMap<K, V>::lookup(K key)
    {
        unsigned long hval;
        unsigned long bktid;

        hval = hashFn(key);
        bktid = hval % numBuckets;
        
        return buckets[bktid];
    }
}

#endif