#ifndef OUROBOROS_RECORDMANAGER_H
#define OUROBOROS_RECORDMANAGER_H

#include <vector>
#include <type_traits>
#include <cstddef>

#define OUROBOROS_DEFAULT_RECORD_MANAGER_CAPACITY_STEP 4096

namespace ouroboros
{
    template <class T>
    struct ouroboros_record
    {
        T value;
        ouroboros_record *next;
    };

    template <class T>
    class RecordManager
    {
        protected:
            std::vector<ouroboros_record<T>*> *records;
            long pos;
            long capacity;
            long capacityStep;
            long numRecords;
        public:
            RecordManager();
            RecordManager(long capacityStep);
            virtual ~RecordManager();

            virtual void increaseCapacity();
            virtual ouroboros_record<T>* getNewRecord();
            virtual long getCapacity();
            virtual long getNumRecords();
    };

    template <class T>
    RecordManager<T>::RecordManager()
    {
        capacityStep = OUROBOROS_DEFAULT_RECORD_MANAGER_CAPACITY_STEP;
        records = new std::vector<ouroboros_record<T>*>();
        capacity = 0;
        increaseCapacity();
        pos = 0;
        numRecords = 0;
    }

    template <class T>
    RecordManager<T>::RecordManager(long capacityStep)
    {
        this->capacityStep = capacityStep;
        records = new std::vector<ouroboros_record<T>*>();
        capacity = 0;
        increaseCapacity();
        pos = 0;
        numRecords = 0;
    }

    template <class T>
    RecordManager<T>::~RecordManager()
    {
        for (auto recordArray : *records) {
            delete[] recordArray;
        }
        delete records;
    }

    template <class T>
    inline void RecordManager<T>::increaseCapacity()
    {
        if (std::is_pointer<T>::value) {
            records->push_back(new ouroboros_record<T>[capacityStep]{{NULL, NULL}});    
        } else {
            records->push_back(new ouroboros_record<T>[capacityStep]{{0, NULL}});
        }
        capacity += capacityStep;
    }
    
    template <class T>
    ouroboros_record<T>* RecordManager<T>::getNewRecord()
    {
        long idx = pos++;
        long i = idx / capacityStep;
        long j = idx % capacityStep;
        ouroboros_record<T> *rec = NULL;
        
        if (idx >= capacity) {
            increaseCapacity();
        }

        rec = &((*records)[i][j]);
        numRecords++;
        return rec;
    }
    
    template <class T>
    long RecordManager<T>::getCapacity()
    {
        return capacity;
    }
    
    template <class T>
    long RecordManager<T>::getNumRecords()
    {
        return numRecords;
    }
}

#endif
