#ifndef OUROBOROS_PAGED_VERSATILE_STORE_H
#define OUROBOROS_PAGED_VERSATILE_STORE_H

extern "C"
{
    #include <sys/mman.h>
}

#include <cstdlib>
#include <deque>
#include <new>

namespace ouroboros
{
    template<class T>
    class PagedVersatileStore
    {
        private:
            static const long SMALL_PAGE_ALIGNMENT = 4096;
            static const long LARGE_PAGE_ALIGNMENT = 2097152;
            static const long HUGE_PAGE_ALIGNMENT = 1073741824;

            std::deque<T*> pages;
            long pageLength;
            long pagePosition;
            long pageAlignment;
            T* pageTail;

            virtual void expand();
        public:
            explicit PagedVersatileStore(long pageSize);
            virtual ~PagedVersatileStore();
            
            PagedVersatileStore(const PagedVersatileStore<T>& other) = delete;
            PagedVersatileStore<T>& operator=(const PagedVersatileStore<T>& other) = delete;
            PagedVersatileStore(PagedVersatileStore<T>&& other);
            PagedVersatileStore<T>& operator=(PagedVersatileStore<T>&& other);

            virtual T* getNew();
    };

    template<class T>
    PagedVersatileStore<T>::PagedVersatileStore(long pageSize) :
        pages(), pageLength(pageSize / sizeof(T)), pagePosition(0)
    {
        int rc;

        if ((pageLength * sizeof(T)) >= HUGE_PAGE_ALIGNMENT) {
            pageAlignment = HUGE_PAGE_ALIGNMENT;
        } else if ((pageLength * sizeof(T)) >= LARGE_PAGE_ALIGNMENT) {
            pageAlignment = LARGE_PAGE_ALIGNMENT;
        } else {
            pageAlignment = SMALL_PAGE_ALIGNMENT;
        }
        
        rc = posix_memalign((void**) &pageTail, pageAlignment, pageLength * sizeof(T));
        if (rc != 0) {
            pagePosition = pageLength + 1;
            throw std::bad_alloc{};
        }

        rc = madvise(pageTail, pageLength * sizeof(T), MADV_HUGEPAGE);
        if (rc != 0) {
            pagePosition = pageLength + 1;
            free(pageTail);
            throw std::bad_alloc{};
        }

        pages.push_back(pageTail);
    }

    template<class T>
    PagedVersatileStore<T>::~PagedVersatileStore()
    {
        while (pages.size() > 0) {
            pageTail = pages.back();
            pages.pop_back();
            free(pageTail);
        }
    }

    template<class T>
    PagedVersatileStore<T>::PagedVersatileStore(PagedVersatileStore<T>&& other) : pages(std::move(other.pages)),
        pageLength(other.pageLength), pagePosition(other.pagePosition), pageAlignment(other.pageAlignment)
    {
        pageTail = other.pageTail;

        other.pages = std::deque<T*>();
        other.pagePosition = 0;
        other.pageTail = NULL;
    }

    template<class T>
    PagedVersatileStore<T>& PagedVersatileStore<T>::operator=(PagedVersatileStore<T>&& other)
    {
        if (this == &other) {
            return *this;
        }

        while (pages.size() > 0) {
            pageTail = pages.back();
            pages.pop_back();
            free(pageTail);
        }

        pages = std::move(other.pages);
        pageLength = other.pageLength;
        pagePosition = other.pagePosition;
        pageAlignment = other.pageAlignment;
        pageTail = other.pageTail;

        other.pages = std::deque<T*>();
        other.pagePosition = 0;
        other.pageTail = NULL;

        return *this;
    }

    template<class T>
    void PagedVersatileStore<T>::expand()
    {
        int rc;
        
        rc = posix_memalign((void**) &pageTail, pageAlignment, pageLength * sizeof(T));
        if (rc != 0) {
            pagePosition = pageLength + 1;
            throw std::bad_alloc{};
        }

        rc = madvise(pageTail, pageLength * sizeof(T), MADV_HUGEPAGE);
        if (rc != 0) {
            pagePosition = pageLength + 1;
            free(pageTail);
            throw std::bad_alloc{};
        }

        pages.push_back(pageTail);
        pagePosition = 0;
    }

    template<class T>
    T* PagedVersatileStore<T>::getNew()
    {
        if (pagePosition >= pageLength) {
            expand();
        }

        return &pageTail[pagePosition++];
    }
}

#endif