#ifndef OUROBOROS_NS_MEM_BYTESSTORE_H
#define OUROBOROS_NS_MEM_BYTESSTORE_H

#include <vector>

namespace ouroboros
{
    class NSMemBytesStore
    {
        private:
            std::vector<char*> *pages;
            long pageSize;
            long position;
            long lastLength;
            char* pageTail;

            virtual void createNewPage();
        public:
            NSMemBytesStore(long pageSize);
            virtual ~NSMemBytesStore();

            virtual char* copy(const char* str);
            virtual void next();
    };
}

#endif