#ifndef OUROBOROS_CAPACITYVECTOR_H
#define OUROBOROS_CAPACITYVECTOR_H

#include <vector>
#include <mutex>
#include <atomic>
#include <type_traits>

namespace ouroboros
{
    template <class T>
    class CapacityVector
    {
        protected:
            std::vector<T*> *vect;
            std::atomic<long> pos;
            std::mutex mtx;
            long capacity;
            long capacityStep;
        public:
            CapacityVector(long capacityStep);
            virtual ~CapacityVector();

            virtual void expand();
            virtual long push_back(T elem);
            virtual T& operator[](long idx);
            virtual long getCapacity();
            virtual long getLength();
    };

    template <class T>
    CapacityVector<T>::CapacityVector(long capacityStep)
    {
        this->capacityStep = capacityStep;
        vect = new std::vector<T*>();
        capacity = 0;
        expand();
        pos.store(0);
    }

    template <class T>
    CapacityVector<T>::~CapacityVector()
    {
        for (auto i = 0; i < (capacity / capacityStep); i++) {
            delete[] (*vect)[i];
        }
        delete vect;
    }

    template <class T>
    void CapacityVector<T>::expand()
    {
        if (std::is_pointer<T>::value) {
            vect->push_back(new T[capacityStep]{NULL});    
        } else {
            vect->push_back(new T[capacityStep]{0});
        }
        capacity += capacityStep;
    }

    template <class T>
    long CapacityVector<T>::push_back(T elem)
    {
        long idx = pos.fetch_add(1, std::memory_order_seq_cst);
        long i = idx / capacityStep;
        long j = idx % capacityStep;

        if (idx >= capacity) {
            std::lock_guard<std::mutex> lock(mtx);
            if (idx >= capacity) {
                expand();
            }
        }
        
        (*vect)[i][j] = elem;

        return idx;
    }

    template <class T>
    T& CapacityVector<T>::operator[](long idx)
    {
        long i = idx / capacityStep;
        long j = idx % capacityStep;

        return (*vect)[i][j];
    }

    template <class T>
    inline long CapacityVector<T>::getCapacity()
    {
        return capacity;
    }

    template <class T>
    inline long CapacityVector<T>::getLength()
    {
        return pos.load();
    }
}

#endif