#ifndef OUROBOROS_RANDOM_PAGED_STRING_STORE_H
#define OUROBOROS_RANDOM_PAGED_STRING_STORE_H

#include "corestructures/BasePagedStringStore.hpp"

#include <vector>

namespace ouroboros
{
    class RandomPagedStringStore: public BasePagedStringStore
    {
        private:
            static const long HUGE_PAGE_ALIGNMENT = 1073741824;
            static const long STRING_DEFAULT_LENGTH = 29;
            static const long STRING_MAX_LENGTH = 4096;
            std::vector<char*>* pages;
            long pageSize;
            long pagePosition;
            char* pageTail;
            bool allocated;
            long* shufflePattern;
            long shufflePatternSize;
            long shufflePosition;
        public:
            RandomPagedStringStore(long pageSize);
            virtual ~RandomPagedStringStore();

            virtual bool isAllocated();
            virtual void expand();
            virtual char* store(const char* str);
    };
}

#endif