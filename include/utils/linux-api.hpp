#ifndef OUROBOROS_LINUX_API_H
#define OUROBOROS_LINUX_API_H

namespace linuxIO
{
    extern "C" {
        #include <unistd.h>
        #include <fcntl.h>
    }
}

namespace linuxMEM
{
    extern "C"
    {
        #include <sys/mman.h>
    }
}

#endif