#ifndef OUROBOROS_TERM_FILE_INDEX_MEMORY_COMPONENT_H
#define OUROBOROS_TERM_FILE_INDEX_MEMORY_COMPONENT_H

#include "memorymanagers/BaseMemoryComponent.hpp"
#include "indexing/knowledge/BaseTermFileIndex.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include "corestructures/PagedVersatileStore.hpp"
#include "corestructures/PagedVersatileIndex.hpp"
#include "corestructures/PagedLinkedElement.hpp"
#include <memory>

namespace ouroboros
{
    enum class TermFileIndexMemoryComponentType {STD, DENSE, SWISS, CHAINED, PSTD, PDENSE, PSWISS, PCHAINED};

    class TermFileIndexMemoryComponent: public BaseMemoryComponent
    {
        private:
            std::shared_ptr<BasePagedStringStore> termStore;
            std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<char*, long>>> indexStore;
            std::shared_ptr<PagedVersatileStore<PagedLinkedElement<long>>> listStore;
            std::shared_ptr<BaseTermFileIndex> index;
        public:
            explicit TermFileIndexMemoryComponent(long pageSize,
                                              TermFileIndexMemoryComponentType type,
                                              unsigned long numBuckets = 134217728);
            virtual ~TermFileIndexMemoryComponent() = default;

            virtual std::shared_ptr<BasePagedStringStore> getPagedStringStore();
            virtual std::shared_ptr<BaseTermFileIndex> getTermFileIndex();
    };
}

#endif