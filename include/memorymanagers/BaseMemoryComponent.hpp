#ifndef OUROBOROS_BASE_MEMORY_COMPONENT_H
#define OUROBOROS_BASE_MEMORY_COMPONENT_H

#include <string>

namespace ouroboros
{
    enum class MemoryComponentType {BASE, DUALQUEUE, PAGEDSTRINGSTORE, INPUT_BUFFER, TERM_INDEX, TERM_FILE_INDEX,
                                    TFIDF_INDEX, CHAOS_BUCKETS, CHAOS_HASH_TABLE, TOKEN_STORE, FILE_INDEX};

    class BaseMemoryComponent
    {
        private:
            MemoryComponentType type;
        public:
            BaseMemoryComponent(MemoryComponentType type) : type(type) { }
            virtual ~BaseMemoryComponent() = default;

            virtual MemoryComponentType getType() { return type; }
    };
}

#endif
