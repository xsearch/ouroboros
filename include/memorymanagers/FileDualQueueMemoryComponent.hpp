#ifndef OUROBOROS_FILEDUALQUEUE_MEMORY_COMPONENT_H
#define OUROBOROS_FILEDUALQUEUE_MEMORY_COMPONENT_H

#include "memorymanagers/BaseMemoryComponent.hpp"
#include "readerdrivers/FileDataBlock.hpp"
#include "corestructures/DualQueue.hpp"

namespace ouroboros
{
    class FileDualQueueMemoryComponent: public BaseMemoryComponent
    {
        private:
            static const int DISK_ALIGNMENT = 512;
            FileDataBlock* dataBlocks;
            DualQueue<FileDataBlock*>* dualQueue;
            char* buffer;
            bool allocated;
            int queueSize;
        public:
            FileDualQueueMemoryComponent(int queueSize, int blockSize);
            virtual ~FileDualQueueMemoryComponent();

            virtual DualQueue<FileDataBlock*>* getDualQueue();
            virtual bool isAllocated();
    };
}

#endif