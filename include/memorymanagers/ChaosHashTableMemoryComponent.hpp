#ifndef OUROBOROS_CHAOS_HASH_TABLE_MEMORY_COMPONENT_H
#define OUROBOROS_CHAOS_HASH_TABLE_MEMORY_COMPONENT_H

#include "memorymanagers/BaseMemoryComponent.hpp"
#include "corestructures/ChaosHashTable.hpp"
#include "indexing/indexing.hpp"

namespace ouroboros
{
    class ChaosHashTableMemoryComponent: public BaseMemoryComponent
    {
        private:
            ChaosHashTable<char*, long, cstr_hash, cstr_equal>* table;
            ChaosBucket<char*, long>* buckets;
            long numBuckets;
        public:
            ChaosHashTableMemoryComponent(ChaosBucket<char*, long>* buckets, long numBuckets);
            virtual ~ChaosHashTableMemoryComponent();

            virtual ChaosHashTable<char*, long, cstr_hash, cstr_equal>* getChaosHashTable();
    };
}

#endif