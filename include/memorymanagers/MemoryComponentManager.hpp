#ifndef OUROBOROS_MEMORY_COMPONENT_MANAGER_H
#define OUROBOROS_MEMORY_COMPONENT_MANAGER_H

#include <string>
#include <vector>
#include <unordered_map>
#include <mutex>

#include "memorymanagers/BaseMemoryComponent.hpp"

namespace ouroboros
{
    class MemoryComponentManager
    {
        private:
            std::unordered_map<MemoryComponentType, std::vector<BaseMemoryComponent*>*>* dict;
            std::mutex* mtx;
        public:
            MemoryComponentManager();
            virtual ~MemoryComponentManager();

            virtual void addMemoryComponent(MemoryComponentType type, unsigned int id, BaseMemoryComponent *component);
            virtual BaseMemoryComponent* getMemoryComponent(MemoryComponentType type, unsigned int id);
    };
}

#endif