#ifndef OUROBOROS_TERM_INDEX_MEMORY_COMPONENT_H
#define OUROBOROS_TERM_INDEX_MEMORY_COMPONENT_H

#include "memorymanagers/BaseMemoryComponent.hpp"
#include "indexing/data/BaseTermIndex.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include <memory>

namespace ouroboros
{
    enum class TermIndexMemoryComponentType {STD, DENSE, SWISS, CHAINED};

    class TermIndexMemoryComponent: public BaseMemoryComponent
    {
        private:
            std::shared_ptr<BasePagedStringStore> store;
            std::shared_ptr<BaseTermIndex> index;
        public:
            explicit TermIndexMemoryComponent(long pageSize,
                                              TermIndexMemoryComponentType type,
                                              unsigned long numBuckets = 134217728);
            virtual ~TermIndexMemoryComponent() = default;

            virtual std::shared_ptr<BasePagedStringStore> getPagedStringStore();
            virtual std::shared_ptr<BaseTermIndex> getTermIndex();
    };
}

#endif