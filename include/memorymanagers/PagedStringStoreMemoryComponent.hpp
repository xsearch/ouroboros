#ifndef OUROBOROS_PAGED_STRING_STORE_MEMORY_COMPONENT_H
#define OUROBOROS_PAGED_STRING_STORE_MEMORY_COMPONENT_H

#include "memorymanagers/BaseMemoryComponent.hpp"
#include "corestructures/BasePagedStringStore.hpp"

namespace ouroboros
{
    class PagedStringStoreMemoryComponent: public BaseMemoryComponent
    {
        private:
            BasePagedStringStore* store;
        public:
            PagedStringStoreMemoryComponent(long pageSize, PagedStringStoreType type);
            virtual ~PagedStringStoreMemoryComponent();

            virtual BasePagedStringStore* getPagedStringStore();
    };
}

#endif